# -*- coding: utf-8 -*-
"""
Spyder Editor

This temporary script file is located here:
/home/michal/.spyder2/.temp.py
"""
import numpy as np
import pyqchem as pyq
import copy
from multiprocessing import Pool
import matplotlib.pyplot as plt

def f(inList):
    index = inList[0]
    inObj = inList[1]
    ppn = inList[2]
    time = inList[3]
    delta = inList[4]
    
#        for i in range(3*self.inputObjects[0].size + 9):
    paramPos = np.hstack((inObj.cellParams.flatten(), inObj.positions.flatten()))
    paramPos[index] += delta

    inFile2 = copy.deepcopy(inObj)
    
    newCellParams = paramPos[0:9]
    newPositions = paramPos[9: 3*inObj.size + 9]
    
    inFile2.cellParams = np.reshape(newCellParams, (3,3))
    inFile2.positions = np.reshape(newPositions, (inObj.size,3))
    inFile2.control['calculation'] = '\'scf\''
    inFile2.control['tprnfor'] = '.true.'
    inFile2.write(inObj.stub +'counter' +str(index) + '.in')
    
    inObjNew = pyq.EspressoParser(inObj.stub + 'counter' +str(index)+ '.in')
    inObjNew.submitJob(ppn, time)
    
    
    arrayInputs2 = np.empty((3, 3), dtype = object)
    for i in range(3):
        for j in range(3):
            inFile3 = copy.deepcopy(inObjNew)
#                    inFile3.positions = positions[-1]
            inFile3.cellParams[i, j] += delta
            inFile3.control['calculation'] = '\'scf\''
            inFile3.write(inObjNew.stub +'cellParam'+str(i) + str(j) + '.in')
            arrayInputs2[i, j] = pyq.EspressoParser(inObjNew.stub +'cellParam'+str(i) + str(j) + '.in')             
    
    #run calculations for cell gradient at new point cellGradient
    for i in range(3):
        for j in range(3):
            arrayInputs2[i,j].submitJob(ppn,time)   
            
    # check start 
    inObjNew.checkJobStarted()
    for i in range(3):
        for j in range(3):
            arrayInputs2[i,j].checkJobStarted()   
    
    #check finish
    inObjNew.checkJobFinished()
    for i in range(3):
        for j in range(3):
            arrayInputs2[i,j].checkJobFinished()
    
    #data collection         
    outObj = pyq.EspressoOutputParser(inObjNew.stub + '.out')
    grad0 = outObj.extractGradient()
    energy0 = outObj.extractEnergy()

    outArray2 = np.empty((3, 3), dtype = object)
    for i in range(3):
        for j in range(3):
            outArray2[i, j] = pyq.EspressoOutputParser(arrayInputs2[i,j].stub + ".out")
    
    energyOutputs2 = np.zeros((3, 3))
    for i in range(3):
        for j in range(3):
            energyOutputs2[i, j] = outArray2[i, j].extractEnergy() 

    gradient2 = np.zeros((3,3))
    for i in range(3):
        for j in range(3):
            gradient2[i, j] = (energyOutputs2[i, j] - energy0)/delta
            
    gradFinal = np.hstack((gradient2.flatten(), grad0))
        
    return [index, gradFinal]


            
            


class qEoptForRealThisTime:
    def __init__(self, inputFile, singlePointTime, singlePpn, delta, deltaField):
        self.reset()
        self.delta = delta
        self.deltaField = deltaField
        self.inputFile = inputFile
        self.firstinputObj = pyq.EspressoParser(inputFile)

        self.singlePointTime = singlePointTime
        self.singlePpn = singlePpn
        self.inputObjects.append(self.firstinputObj)
        self.positions.append(self.inputObjects[-1].positions)
        self.cellParams.append(self.inputObjects[-1].cellParams)
        
    def reset(self):
        self.inputFile = None
        self.inputObjects = []
        self.cycleCounter = 0
        self.positions = []
        self.energies = []
        self.delta = 0.0
        self.deltaField = 0.0        
        
        self.energyGrad = []
        self.totalGrad = []
        self.cellParams = []
        self.inverHessians = []
        self.rotationalVectors = []
        self.transformationMatrix = []
        self.energyChangeList = []
        self.hessians = []
        self.absDispList = []
        self.absGradList = []
        self.convergence = []
        self.gradTol = 1.5e-6
        self.dispTol = 2e-5
        self.energyTol = 1e-7
        self.step = []
        self.maxCycles = 100
        self.cellGrad = []
        self.paramPos = []
        
        self.latticeEnergies = []
        self.inLatticeArays = []
        self.outLatticeArrays = []
        self.latticeEnergies = []        
        
    def main(self):
        self.singlePoint()

        self.inputObjects[-1].submitJob(self.singlePpn, self.singlePointTime)
        self.inputObjects[-1].checkJobStarted()
        self.inputObjects[-1].checkJobFinished()
        self.getEnergies()
        self.getParamPos()

        for i in range(1,self.maxCycles +1):
            self.makecellParamInputObj()
            self.runCellEnergyCalc()
            self.createOutLattice()
            self.getLatticeEnergies()
            self.getCellGrad()
            self.getTotalGradient()
      
            self.stepBFGS()
            self.lineSearch()
            
            self.checkConvergence() 
            if self.convergence[-1]==True:
                print ":) THE PROGRAM ACTUALLY CONVERGED!!!!!!!!!"
                break
            else:
                self.cycleCounter +=1
                continue      
            
    def makecellParamInputObj(self):
        arrayInputs = np.empty((3, 3), dtype = object)
        for i in range(3):
            for j in range(3):
                inFile = copy.deepcopy(self.inputObjects[-1])
                inFile.positions = self.positions[-1]
                inFile.cellParams[i, j] += self.delta
                inFile.control['calculation'] = '\'scf\''
                inFile.write(inFile.stub +'Lattice'+str(i) + str(j) + '.in')
                arrayInputs[i, j] = pyq.EspressoParser(inFile.stub +'Lattice'+ str(i) + str(j) + '.in') #Adds delta to all cell Parameters
        self.inLatticeArays.append(arrayInputs) #Appends changed cell Parameters to inLatticeArrays
        
    def getEnergies(self):
        outObj = pyq.EspressoOutputParser(self.inputObjects[-1].stub + '.out')
        outObj.extractEnergy()
        outObj.extractGradient()
        self.energyGrad.append(outObj.gradient)
        self.energies.append(outObj.finalEnergy)
    
    def runCellEnergyCalc(self): 
        for i in range(3):
            for j in range(3):
                self.inLatticeArays[-1][i, j].submitJob(self.singlePpn, self.singlePointTime) #Submits singlePoint Calc for changed cell Parameters
        for i in range(3):
            for j in range(3):
                self.inLatticeArays[-1][i, j].checkJobStarted()
        for i in range(3):
            for j in range(3):
                self.inLatticeArays[-1][i, j].checkJobFinished()
    
    def createOutLattice(self): #creates array of outputObjects
        latticeArray = np.empty((3, 3), dtype = object)
        for i in range(3):
            for j in range(3):
                latticeArray[i, j] = pyq.EspressoOutputParser(self.inLatticeArays[-1][i, j].stub + ".out")
        self.outLatticeArrays.append(latticeArray)
        
    def getLatticeEnergies(self): #creates array of energies from outputObjects
        energyOutputs = np.zeros((3, 3))
        for i in range(3):
            for j in range(3):
                energyOutputs[i, j] = self.outLatticeArrays[-1][i, j].extractEnergy()
        self.latticeEnergies.append(energyOutputs)
            
    def getCellGrad(self):#calculates gradient from change in energies
        gradient = np.zeros((3,3))
        for i in range(3):
            for j in range(3):
                gradient[i, j] = (self.latticeEnergies[-1][i, j] - self.energies[-1])/self.delta
        gradient = gradient.flatten()        
        print 'cell gradient is ' + str(gradient)
        self.cellGrad.append(gradient)
        
    def getParamPos(self):
        paramPos = np.hstack((self.cellParams[-1].flatten(), self.positions[-1].flatten()))
        self.paramPos.append(paramPos)
        
    def getTotalGradient(self):
        self.totalGrad.append(np.hstack((self.cellGrad[-1], self.energyGrad[-1])))
           
    def singlePoint(self):
        inFile = copy.deepcopy(self.inputObjects[-1])
        inFile.control['calculation'] = '\'scf\''
        inFile.control['tprnfor'] = '.true.'
        inFile.write(inFile.stub + 'Cycle' + str(self.cycleCounter) + '.in')
        inObj = pyq.EspressoParser(inFile.stub +'Cycle' +str(self.cycleCounter) + '.in')
        self.inputObjects.append(inObj)
        
    def buildRotationalVectors(self):
        cellA = np.zeros(shape=[3,3])
        cellB = np.zeros(shape=[3,3])
        cellC = np.zeros(shape=[3,3])
        
        cellA[1, 2] = -self.cellParams[-1][0, 0]
        cellA[2, 1] = self.cellParams[-1][0, 0]
        cellA[0, 2] = self.cellParams[-1][0, 1]
        cellA[2, 0] = -self.cellParams[-1][0, 1]
        cellA[0, 1] = -self.cellParams[-1][0, 2]
        cellA[1, 0] = self.cellParams[-1][0, 2]

        cellB[1, 2] = -self.cellParams[-1][1, 0]
        cellB[2, 1] = self.cellParams[-1][1, 0]
        cellB[0, 2] = self.cellParams[-1][1, 1]
        cellB[2, 0] = -self.cellParams[-1][1, 1]
        cellB[0, 1] = -self.cellParams[-1][1, 2]
        cellB[1, 0] = self.cellParams[-1][1, 2]

        cellC[1, 2] = -self.cellParams[-1][2, 0]
        cellC[2, 1] = self.cellParams[-1][2, 0]
        cellC[0, 2] = self.cellParams[-1][2, 1]
        cellC[2, 0] = -self.cellParams[-1][2, 1]
        cellC[0, 1] = -self.cellParams[-1][2, 2]
        cellC[1, 0] = self.cellParams[-1][2, 2]
        
        self.relativePositions = self.positions[-1] - self.inputObjects[-1].geomCen
        S = np.zeros(shape=[3,3])
        for i in range(self.inputObjects[0].size):
            Ri = np.zeros(shape=[3,3])
            Ri[0, 1] = -self.relativePositions[i, 2]
            Ri[0, 2] = self.relativePositions[i, 1]
            Ri[1, 0] = self.relativePositions[i, 2]
            Ri[1, 2] = -self.relativePositions[i, 0]
            Ri[2, 0] = -self.relativePositions[i, 1]
            Ri[2, 1] = self.relativePositions[i, 0]
            S += np.dot(Ri.transpose(), Ri)
        rotCell = np.dot(cellA.transpose(), cellA) + np.dot(cellB.transpose(), cellB) + np.dot(cellC.transpose(), cellC) + S

        eval, evec = np.linalg.eigh(rotCell)

        a = self.cellParams[-1][0, :]
        b = self.cellParams[-1][1, :]
        c = self.cellParams[-1][2, :]

        rotationMatrix = np.zeros((3*self.inputObjects[0].size + 9, 3))    
        for i in range(3):
            rotationMatrix[0:3, i] = np.cross(a, evec[:, i])
            rotationMatrix[3:6, i] = np.cross(b, evec[:, i])
            rotationMatrix[6:9, i] = np.cross(c, evec[:, i])
            for j in range(self.inputObjects[0].size):
                rotationMatrix[j*3+9: j*3 + 12, i] = np.cross(self.relativePositions[j, :], evec[:, i])
        # normalize        
        for i in range(3):
            rotationMatrix[:,i] = rotationMatrix[:,i]/np.linalg.norm(rotationMatrix[:,i])
        
        
        

#        print self.rotationalVectors
        translationVectors = np.zeros((3*self.inputObjects[0].size + 9, 3))
        for i in range(self.inputObjects[0].size):
            for j in range(3):
                translationVectors[3*i+j +9, j] = 1.0
        
        translationVectors /= np.sqrt(self.inputObjects[0].size)
#        print translationVectors
#        for i in range(3):
#            for j in range(3):
#                print np.dot(rotationMatrix[:,i], translationVectors[:,j])
        
        rotTransMat = np.hstack((translationVectors, rotationMatrix))
        self.rotationalVectors.append(rotTransMat)
        
    def calculateTransformationMatrix(self, hessian): 
        self.buildRotationalVectors()
        hess = pyq.hessian.HessianManual(hessian)
        hess.buildRotTransLessVecSpace(self.rotationalVectors[-1])
        self.transformationMatrix.append(hess.vibBasis)
        
    def makeLinesearchInputObj(self, i, cellParams, positions):
        inFile = copy.deepcopy(self.inputObjects[-1])
        inFile.cellParams = cellParams
        inFile.positions = positions
        inFile.control['calculation'] = '\'scf\''
        inFile.write(inFile.stub +'Cycle' +str(self.cycleCounter)+ 'Linesearch' + str(i) + '.in')
        inObj = pyq.EspressoParser(inFile.stub +'Cycle' +str(self.cycleCounter)+ 'Linesearch' + str(i) + '.in')
        return inObj
           
    def formInvHessianRot(self):
        hNonInv = self.inverHessians[-1]
        #transform hessian to cartesian space
        vOld=self.transformationMatrix[-2]
        HessCart = np.dot(vOld, np.dot(hNonInv,vOld.transpose()))
        #calculate new basis to transform to
        vNew = self.transformationMatrix[-1]
        #transform to new basis
        hNonInvNew = np.dot(vNew.transpose(),np.dot(HessCart,vNew))
        #form inverse in the new space
        hNew = np.linalg.inv(hNonInvNew)
        
        delg = self.totalGrad[-1] - self.totalGrad[-2]
        delx = self.paramPos[-1] - self.paramPos[-2]
        
        vx = np.dot(vNew.transpose(),delx).flatten()
        vg = np.dot(vNew.transpose(),delg).flatten()
        vxvg = np.dot(vx,vg)
        hg = np.dot(hNew,vg)
        ghg = np.dot(vg,hg)
        
        u = (vx/vxvg)-(hg/ghg)
        h1 = np.outer(vx,vx)/vxvg
        h2 = np.outer(hg,hg)/ghg
        h3 = ghg*(np.outer(u,u))
        newInvHessian = hNew + h1-h2+h3

        self.inverHessians.append(newInvHessian)        
        
    def stepBFGS(self):               
        if self.cycleCounter%5 == 0:
            tentativeInvHess = np.eye(3*self.inputObjects[0].size + 9)
            self.calculateTransformationMatrix(tentativeInvHess)
            transformMat = self.transformationMatrix[-1]
            self.inverHessians.append(np.linalg.inv(np.dot(transformMat.transpose(),np.dot(tentativeInvHess, transformMat))))
        else:
            self.calculateTransformationMatrix(np.dot(self.transformationMatrix[-1],np.dot(np.linalg.inv(self.inverHessians[-1]),self.transformationMatrix[-1].transpose())))
            self.formInvHessianRot()
            
        Vmat = self.transformationMatrix[-1]
        Hinv = self.inverHessians[-1]
        stepDirection = -np.dot(Vmat,(np.dot(Hinv, np.dot(Vmat.transpose(), self.totalGrad[-1]))))
        self.step.append(stepDirection)
                    
    def lineSearch(self):
        step = self.step[-1]
        
        print 'step is ' + str(step)
        print 'old paramPos is ' + str(self.paramPos[-1])
        maxStep = np.amax(np.absolute(step))
        if maxStep < 0.1:
            paramPos = self.paramPos[-1] + step
        else:
            scaleFactor = 0.1/maxStep
            step = scaleFactor * step
            paramPos = self.paramPos[-1] + step
        print 'new paramPos is ' + str(paramPos)    
        param = np.reshape(paramPos[0:9], (3, 3))
        Pos = np.reshape(paramPos[9: 3*self.inputObjects[0].size +9], (self.inputObjects[0].size, 3))
        inObj = self.makeLinesearchInputObj(0, param, Pos)
        inObj.submitJob(self.singlePpn, self.singlePointTime)
        inObj.checkJobStarted()
        inObj.checkJobFinished()
        outObj = pyq.EspressoOutputParser(inObj.stub + '.out')
        energy = outObj.extractEnergy()  
        
        for i in range(1,3):
            if energy > self.energies[-1]:
                paramPos = self.paramPos[-1] + (3.00-i)/4.00*step
            elif energy < self.energies[-1]:
                break
            param = np.reshape(paramPos[0:9], (3, 3))
            Pos = np.reshape(paramPos[9: 3*self.inputObjects[0].size + 9], (self.inputObjects[0].size, 3))
            inObj = self.makeLinesearchInputObj(i, param, Pos)
            inObj.submitJob(self.singlePpn, self.singlePointTime)
            inObj.checkJobStarted()
            inObj.checkJobFinished()
            outObj = pyq.EspressoOutputParser(inObj.stub + '.out')
            energy = outObj.extractEnergy()

        self.paramPos.append(paramPos)
        self.positions.append(Pos)
        self.cellParams.append(param)
        self.energies.append(energy)
        outObj.extractGradient()
        self.energyGrad.append(outObj.gradient)

    def checkConvergence(self):
        disp = self.paramPos[-1]-self.paramPos[-2]
        absDisp = np.absolute(disp)
        dispMax = np.amax(absDisp)
        
        self.absDispList.append(dispMax)

        grad = np.dot(self.transformationMatrix[-1], np.dot(self.transformationMatrix[-1].transpose(), self.totalGrad[-1]))
        absGrad = np.absolute(grad)
        gradMax = np.amax(absGrad)
        
        self.absGradList.append(gradMax)
        
        energyChange = np.abs(self.energies[-1]-self.energies[-2])
        self.energyChangeList.append(energyChange)
        
        toleranceList = []
        
        if energyChange < self.energyTol:
            toleranceList.append(1)
        elif energyChange > self.energyTol:
            toleranceList.append(0)
        if dispMax < self.dispTol:
            toleranceList.append(1)
        elif dispMax > self.dispTol:
            toleranceList.append(0)
        if gradMax < self.gradTol:
            toleranceList.append(1)
        elif gradMax > self.gradTol:
            toleranceList.append(0)
            
        handle = open('printedInfo', 'a')
        handle.write('cycle' + str(self.cycleCounter) + '\n')
        handle.write('energyChange ' + str(energyChange) + '\n')
        handle.write('dispMax ' + str(dispMax) + '\n')
        handle.write('gradMax ' + str(gradMax) + '\n')
        handle.write('energy ' + str(self.energies[-1]) + '\n')
	
        total = np.sum(toleranceList)
        if total == 3:
            self.convergence.append([True])
        elif total < 3:
            self.convergence.append([False])

    def calc3PointCellGrad(self, cellParams, positions, delta):
        pass
        # returns cell parameter gradient at a given point in position cell param space
    def buildHessian(self, delta):
#        counter = 0
#        gradientFinal = []
#        gradientInitial = []
        positions = []
        cellParams = []
#        newCellParams = []
#        newPositions = []
#        cellGradInitial = []
        energies = []
#        latticeEnergies = []
#        outLatticeArrays = []
        inLatticeArrays = []
        
        #creat hessian
        hessian = np.zeros((3*self.inputObjects[0].size + 9, 3*self.inputObjects[0].size + 9))     
        
        #initial gradient calc
        inFile = copy.deepcopy(self.inputObjects[-1])
        inFile.control['calculation'] = '\'scf\''
        inFile.control['tprnfor'] = '.true.'
        inFile.write(inFile.stub + 'Hessian' + '.in')
        inObj = pyq.EspressoParser(inFile.stub +'Hessian' + '.in')

        

        #set up initial cell gradient calc
        # + and - shifts for cell parameters
        arrayInputsPlus = np.empty((3, 3), dtype = object)
        arrayInputsMinus = np.empty((3,3), dtype = object)
        
        for i in range(3):
            for j in range(3):
                inFile = copy.deepcopy(self.inputObjects[-1])
                inFile.cellParams[i, j] += delta
                inFile.control['calculation'] = '\'scf\''
                inFile.write(inFile.stub +'Cell'+str(i) + str(j) + '.in')
                arrayInputsPlus[i, j] = pyq.EspressoParser(inFile.stub +'Cell'+ str(i) + str(j) + '.in')
        
        for i in range(3):
            for j in range(3):
                inFile = copy.deepcopy(self.inputObjects[-1])
                inFile.cellParams[i, j] += -delta
                inFile.control['calculation'] = '\'scf\''
                inFile.write(inFile.stub +'Cell'+str(i) + str(j) + '.in')
                arrayInputsMinus[i, j] = pyq.EspressoParser(inFile.stub +'Cell'+ str(i) + str(j) + '.in')
        
#        inLatticeArrays.append(arrayInputs)   
        
        #run calculations for Initial gradient and cellGradient
        inObj.submitJob(self.singlePpn, self.singlePointTime)
        for i in range(3):
            for j in range(3):
                arrayInputsPlus[i,j].submitJob(self.singlePpn,self.singlePointTime)    
                arrayInputsMinus[i,j].submitJob(self.singlePpn,self.singlePointTime)  
                
        # check initial calcs started        
        inObj.checkJobStarted()
        for i in range(3):
            for j in range(3):
                arrayInputsPlus[i,j].checkJobStarted()     
                arrayInputsMinus[i,j].checkJobStarted()
                
        # check initial calcs finishe
        inObj.checkJobFinished() 
        for i in range(3):
            for j in range(3):
                arrayInputsPlus[i,j].checkJobFinished()
                arrayInputsMinus[i,j].checkJobFinished()
                
        # collect data for initial calc      
        outObj = pyq.EspressoOutputParser(inObj.stub + '.out')
        grad0 = outObj.extractGradient()
        energy0 = outObj.extractEnergy()
        
        positions.append(inObj.positions)
        cellParams.append(inObj.cellParams)
        energies.append(outObj.finalEnergy)        
        
        # collect data for initial cell calc
        latticeArrayPlus = np.empty((3, 3), dtype = object)
        latticeArrayMinus = np.empty((3, 3), dtype = object)
        
        for i in range(3):
            for j in range(3):
                latticeArrayPlus[i, j] = pyq.EspressoOutputParser(arrayInputsPlus[i, j].stub + ".out")
                latticeArrayMinus[i,j] = pyq.EspressoOutputParser(arrayInputsMinus[i, j].stub + ".out")

#        outLatticeArrays.append(latticeArray)
        
        energyOutputsPlus = np.zeros((3, 3))
        energyOutputsMinus = np.zeros((3,3))
        for i in range(3):
            for j in range(3):
                energyOutputsPlus[i, j] = latticeArrayPlus[i, j].extractEnergy()
                energyOutputsMinus[i,j] = latticeArrayPlus[i,j].extractEnergy()
#                latticeEnergies.append(energyOutputs)        
        
        gradient = np.zeros((3,3))
        for i in range(3):
            for j in range(3):
                energy = []
                energy.append(energyOutputsMinus[i,j])
                energy.append(energy0)
                energy.append(energyOutputsPlus[i,j])
                x = [-delta, 0.0, delta]
                
                z = np.polyfit(x, energy, 1)
                
                gradient[i, j] = z[0]
        cellGradient = gradient.flatten()        
     
        initialGrad = np.hstack((cellGradient, grad0))
        
        
#        paramPos = np.hstack((cellParams[-1].flatten(), positions[-1].flatten()))
        masterList = []
        for i in range(3*inObj.size + 9):
            smallList = [i, inObj, copy.copy(self.singlePpn), copy.copy(self.singlePointTime), copy.copy(self.delta)]
            masterList.append(smallList)
            
            
#        hessian[i, 9:3*self.inputObjects[0].size + 9] = (gradientFinal[counter]-gradientInitial)/delta
#        hessian[i, 0:9] = (cellGradFinal[counter] - cellGradInitial)/delta
            
#        a = range(3*self.inputObjects[0].size + 9)
        
        p = Pool(32)
        result = p.map(f,masterList)
        
        for entry in result:
            hessian[entry[0],:]= (entry[1]-initialGrad)/delta
            
        print 'hessian is \n'
        print hessian
        print hessian[5,20]
        print hessian[20,5]
        print hessian[39,30]
        print hessian[30,39]
        print hessian[55,32]
        print hessian[32,55]
        print hessian[3, 54]
        print hessian[54,3]
        return hessian
        
    def buildDipoleMomentDerivative(self):
        
        #############Create all jobs first #######################################
        #initial gradient job
        inFile = copy.deepcopy(self.inputObjects[-1])
        inFile.control['calculation'] = '\'scf\''
        inFile.control['tprnfor'] = '.true.'
        inFile.write(inFile.stub + 'grad0' + '.in')
        #create espressor parser object and submit job
        inObj = pyq.EspressoParser(inFile.stub +'grad0' + '.in')     
        
        #initial field calculations for positions gradient
        fieldInObjList = []
        fieldArray = np.eye(3) * self.deltaField
        xyzList = ['x','y', 'z']
        for i in range(3):
            inObjNew = copy.deepcopy(inObj)
            inObjNew.addField(fieldArray[i,:])
            inObjNew.write(inFile.stub + 'gradF' + xyzList[i] + '.in')
            inObjNew = pyq.EspressoParser(inFile.stub + 'gradF' + xyzList[i] + '.in')
            fieldInObjList.append(inObjNew)    
        
        #creates obj with no field but shift in cell parameters for 0 field cell gradient 
        arrayInputsNoField = np.empty((3, 3), dtype = object)
        for i in range(3):
            for j in range(3):
                inObjNoField = copy.deepcopy(inObj)
                inObjNoField.cellParams[i, j] += self.delta
                inObjNoField.write(inObjNoField.stub + 'cellGrad' + str(i) + str(j) + 'NoField' + '.in')
                arrayInputsNoField[i, j] = pyq.EspressoParser(inObjNoField.stub + 'cellGrad' + str(i) + str(j) + 'NoField' + '.in')
        
        
        
        # create shifted cell param jobs at finite field 
        arrayInputsField = np.empty((3,3,3), dtype = object)
        for i in range(3):
            for j in range(3):
                for k in range(3):    
                    inObjField = copy.deepcopy(fieldInObjList[i])
                    inObjField.cellParams[j, k] += self.delta
                    inObjField.write(inObj.stub + 'cellGrad' + str(j) + str(k) + 'Field' + xyzList[i] + '.in')
                    arrayInputsField[i, j, k] = pyq.EspressoParser(inObj.stub + 'cellGrad' + str(j) + str(k) + 'Field' + xyzList[i] + '.in')
                    
        ######### submit all jobs ###############################
        inObj.submitJob(self.singlePpn, self.singlePointTime)
        
        # submit field jobs
        for obj in fieldInObjList:
            obj.submitJob(self.singlePpn, self.singlePointTime)
        
        # submit no field cell param jobs
        for i in range(3):
            for j in range(3):
                arrayInputsNoField[i, j].submitJob(self.singlePpn, self.singlePointTime)
                
        # submit field cell param jobs
        for i in range(3):
            for j in range(3):
                for k in range(3):
                    arrayInputsField[i,j,k].submitJob(self.singlePpn, self.singlePointTime)
                    
                    
        
        ############ check all jobs started ################################
        
        inObj.checkJobStarted()
        
        for obj in fieldInObjList:
            obj.checkJobStarted()
            
        for i in range(3):
            for j in range(3):
                arrayInputsNoField[i, j].checkJobStarted()
                
        for i in range(3):
            for j in range(3):
                for k in range(3):
                    arrayInputsField[i,j,k].checkJobStarted()
                    
                    
        ################# check all jobs finished ###################################
        inObj.checkJobFinished()
        
        for obj in fieldInObjList:
            obj.checkJobFinished()
        
        
        for i in range(3):
            for j in range(3):
                arrayInputsNoField[i, j].checkJobFinished()
                
        for i in range(3):
            for j in range(3):
                for k in range(3):
                    arrayInputsField[i,j,k].checkJobFinished()    
         
        ############## create output objects ####################################
        
        outObj = pyq.EspressoOutputParser(inFile.stub +'grad0' + '.out')
        
        fieldOutObjList = []
        for i in range(3):
            outObjNew = pyq.EspressoOutputParser(inFile.stub + 'gradF' + xyzList[i] + '.out')
            fieldOutObjList.append(outObjNew)
        
        


        arrayOutputsNoField = np.empty((3, 3), dtype = object)
        for i in range(3):
            for j in range(3):
                arrayOutputsNoField[i, j] = pyq.EspressoOutputParser(inObjNoField.stub + 'cellGrad' + str(i) + str(j) + 'NoField' + '.out')
        
        
        
        # create shifted cell param jobs at finite field 
        arrayOutputsField = np.empty((3,3,3), dtype = object)
        for i in range(3):
            for j in range(3):
                for k in range(3):    
                    arrayOutputsField[i, j, k] = pyq.EspressoOutputParser(inObj.stub + 'cellGrad' + str(j) + str(k) + 'Field' + xyzList[i] + '.out')

        
        ####### get energies and gradients  #############
        
        E0 = outObj.extractEnergy()
        g0 =outObj.extractGradient()
        
        cellParamEnergies0 = np.zeros((3,3))
        
        for i in range(3):
            for j in range(3):
                 cellParamEnergies0[i,j] = arrayOutputsNoField[i, j].extractEnergy()
        
        
        Ex0 = fieldOutObjList[0].extractEnergy()
        Ey0 = fieldOutObjList[1].extractEnergy()
        Ez0 = fieldOutObjList[2].extractEnergy()
        gx0 = fieldOutObjList[0].extractGradient()
        gy0 = fieldOutObjList[1].extractGradient()
        gz0 = fieldOutObjList[2].extractGradient()
        
        cellParamEnergiesField = np.zeros((3,3,3))
        
        for i in range(3):
            for j in range(3):
                for k in range(3): 
                    cellParamEnergiesField[i,j,k] = arrayOutputsField[i, j, k].extractEnergy()
                    
        #### cell gradients at 0 and finite fields           
        cellGrad0 = (cellParamEnergies0 - E0)/self.delta
        
        cellGradX = (cellParamEnergiesField[0,:,:]-Ex0)/self.delta
        cellGradY = (cellParamEnergiesField[1,:,:]-Ey0)/self.delta
        cellGradZ = (cellParamEnergiesField[2,:,:]-Ez0)/self.delta      
        
        
        ### final total gradients at small field 
        G0 = np.hstack((cellGrad0.flatten(), g0))
        
        Gx =np.hstack((cellGradX.flatten(), gx0))
        Gy =np.hstack((cellGradY.flatten(), gy0))
        Gz =np.hstack((cellGradZ.flatten(), gz0))

        dipDiv = np.zeros((3*inFile.size + 9, 3))
        
        dipDiv[:,0] = (Gx -G0)/self.deltaField
        dipDiv[:,1] = (Gy -G0)/self.deltaField
        dipDiv[:,2] = (Gz -G0)/self.deltaField
        
#        for i in range(3):
#            dipDiv[:,i] = (fieldArray[:,i]-grad0)/self.deltaField
    
        print 'dipDiv is \n'
        print dipDiv
        
        return dipDiv
              
#        fieldGradArray = np.zeros((inFile.size, 3))
#        
#
#        
#        #No field, no changed cell parameter, no cap             
#        outObj.extractGradient() 
#        outObj.extractEnergy()
#        energy0 = outObj.finalEnergy
#        grad0 = outObj.gradient
#
#
#                
#        latticeArrayNoField = np.empty((3, 3), dtype = object)
#        for i in range(3):
#            for j in range(3):
#                latticeArrayNoField[i, j] = pyq.EspressoOutputParser(arrayInputsNoField[i, j].stub + ".out")
#        
#        energyOutputsNoField = np.zeros((3, 3))
#        for i in range(3):
#            for j in range(3):
#                energyOutputsNoField[i, j] = latticeArrayNoField[i, j].extractEnergy()
#
#        gradientCellParamNoField = (energyOutputsNoField - energy0)/self.delta
#
#        # list of energies at different field directions, but not changes in cell params or positions
#        energiesFieldNoDelta = []
#        for i in range(3):
#            outObjNew = pyq.EspressoOutputParser(inFile.stub + 'gradF' + xyzList[i] + '.out')
#            outObjNew.extractGradient()
#            fieldGradArray[:,i]= outObjNew.gradient  
#            outObjNew.extractEnergy()
#            energiesFieldNoDelta[i].append(outObjNew.finalEnergy)
#            
#        #creates input objects with field and changed cell paramters
#        arrayInputs = np.empty((9, 3), dtype = object)
#        counter = 0
#        for i in range(3):
#            for j in range(3):
#                for z in range(3):
#                    counter += 1
#                    inFile = copy.deepcopy(fieldInObjList[i])
#                    inFile.cellParams[j, z] += self.delta
#                    inFile.write(inFile.stub +'Gradient' +str(j) + str(z) + 'Field' + str(i) + '.in')
#                arrayInputs[counter, i] = pyq.EspressoParser(inFile.stub +'Lattice'+ str(j) + str(z) + 'Field' + str(i) + '.in')
#                            
#        latticeArray = np.empty((9, 3), dtype = object)
#        for i in range(9):
#            for j in range(3):
#                latticeArray[i, j] = pyq.EspressoOutputParser(arrayInputs[i, j].stub + ".out")
#        
#        energyOutputs = np.zeros((9, 3))
#        for i in range(9):
#            for j in range(3):
#                energyOutputs[i, j] = latticeArray[i, j].extractEnergy()
#
#        gradientFieldCellParam = []
#        for i in range(3):
#            gradientFieldCellParam.append((energyOutputs[:, i] - energiesFieldNoDelta[i])/self.delta)
      
#/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      #Is this still correct? Neither of the gradients are ever used or return idk what to do

    
    def getTensor(self):
        dipDiv = self.buildDipoleMomentDerivative()
        hessian = self.buildHessian(self.delta)
        hessianNew = 0.5 * (hessian + hessian.transpose())
        TransMat = self.calculateTransformationMatrix(hessianNew)
        
        xL = []
        yL = []
        for i in range(1,np.shape(hessian)[0]):
            for j in range(i+1, np.shape(hessian)[0]):
                xL.append(hessian[i,j])
                yL.append(hessian[j,i])
        
        plt.scatter(xL,yL)
        plt.savefig('hessianSym.png')
        
        handle = open('hessianEigSpec', 'w')
        eVal, eVec = np.linalg.eigh(hessianNew)
        handle.write(str(eVal))
        handle.write('\n')
        handle.write(str(eVal))
        handle.close()
        
        Dv  = np.dot(TransMat.transpose(), dipDiv)
        Hvv = np.dot(np.dot(TransMat.transpose(), hessianNew), TransMat)
        HvvInvv = np.linalg.inv(Hvv)
        
        eVal, eVec= np.linalg.eigh(Hvv)
        
        derivative = np.dot(TransMat, -np.dot(HvvInvv, Dv))
        Tinv = np.linalg.inv(self.cellParams).transpose()

        derivAX = derivative[0:3, 0]
        derivBX = derivative[3:6, 0]
        derivCX = derivative[6:9, 0]
        derivX = np.hstack((derivAX, derivBX, derivCX))
        jawn1 = np.dot(derivX, Tinv)
        
        derivAY = derivative[0:3, 1]
        derivBY = derivative[3:6, 1]
        derivCY = derivative[6:9, 1]
        derivY = np.hstack((derivAY, derivBY, derivCY))
        jawn2 = np.dot(derivY, Tinv)
        
        derivAZ = derivative[0:3, 2]
        derivBZ = derivative[3:6, 2]
        derivCZ = derivative[6:9, 2]
        derivZ = np.hstack((derivAZ, derivBZ, derivCZ))
        jawn3 = np.dot(derivZ, Tinv)
        
        jawn1V2 = (jawn1.transpose() + jawn1)/2
        jawn2V2 = (jawn2.transpose() + jawn2)/2
        jawn3V2 = (jawn3.transpose() + jawn3)/2
        
        Tensor = np.zeros((3, 3, 3))
        Tensor[:, :, 0] = jawn1V2
        Tensor[:, :, 1] = jawn2V2
        Tensor[:, :, 2] = jawn3V2
        
        
        handle = open('piezoTensor', 'w')
        
        s = ''
        for i in range(3):
            for j in range(3):
                s += '%10.10f  %10.10f  %10.10f\n' % (Tensor[j,:, i])
                if j ==2:
                    s+='\n'
                    
        handle.write(s)
        handle.close()
        
        
        
        print 'piezoTensor is \n'
        print Tensor
        return Tensor






































#class qEoptimizer:
#    def __init__(self, inputFile, nucTime, nucPpn, singlePointTime, singlePpn, delta):
#        self.reset()
#        self.inputFile = inputFile
#        self.firstinputObj = pyq.EspressoParser(inputFile)
#        self.nucTime = nucTime
#        self.nucPpn = nucPpn
#        self.singlePointTime = singlePointTime
#        self.singlePpn = singlePpn
#        self.delta = delta
#        self.inputObjects.append(self.firstinputObj)
#    
#    def reset(self):
#        self.inputFile = None
#
#        # for nuclear opts
#        self.nucTime = 0
#        self.nucPpn = 0
#        self.inputObjects = []
#        self.outputObjects = []
#        self.positions = []
#        self.energies = []
#        
#        #for gradient construction
#        self.latticeEnergies = []
#        self.inLatticeArays = []
#        self.outLatticeArrays = []
#        self.latticeEnergies = []
#        
#        
#        self.energyGrad = []
#        self.cellParams = []
#        self.inverHessians = []
#        self.rotationalVectors = []
#        self.transformationMatrix = []
#        self.energyChangeList = []
#        self.hessians = []
#        self.absDispList = []
#        self.absGradList = []
#        self.convergence = []
#        self.gradTol = 1.5e-6
#        self.dispTol = 2e-5
#        self.energyTol = 1e-7
#        self.cycleCounter = 0
#        self.step = []
#        self.maxCycles = 100
#     
#    def main(self):
#        self.firstinputObj.submitJob(self.nucPpn, self.nucTime) #Submits nuclear optimization on input file
#        self.firstinputObj.checkJobStarted()
#        self.firstinputObj.checkJobFinished()
#        self.firstoutputObj = pyq.EspressoOutputParser(self.firstinputObj.stub + ".out")
#        self.firstoutputObj.getFinalPositions()
#        self.positions.append(self.firstoutputObj.finalPositions) #appends optimized positions from nuclear optimization
#        self.firstoutputObj.extractEnergy()
#        self.energies.append(self.firstoutputObj.finalEnergy) #appends new energy from nuclear optimization
#        self.cellParams.append(self.firstinputObj.cellParams)
#        
#        
#        self.makecellParamInputObj()
#        self.runCellEnergyCalc()
#        self.createOutLattice()
#        self.getLatticeEnergies()
#        self.getEnergyGrad()
#        
#        for i in range(self.maxCycles):
#            self.stepBFGS()
#            self.lineSearch()
#            
#            self.makeNuclearOptFile(i)
#            self.runNucOpt()
#            self.makecellParamInputObj()
#            self.runCellEnergyCalc()
#            self.createOutLattice()
#            self.getLatticeEnergies()
#            self.getEnergyGrad()
#            
#            self.checkConvergence()
#            if self.convergence[-1]==True:
#                print "Boo yeahhhhhhhhhhhh!!!!!!!!!!"
#                break
#            else:
#                self.cycleCounter +=1
#                continue
#            
#    def makeNuclearOptFile(self, i):
#        inObj = copy.deepcopy(self.inputObjects[0])
#        inObj.cellParams = self.cellParams[-1]
#        inObj.positions = self.positions[-1]
#        inObj.write(inObj.stub + 'nucOpt' + str(i)+ '.in')
#        inObj = pyq.EspressoParser(inObj.stub + 'nucOpt' + str(i)+ '.in')
#        self.inputObjects.append(inObj)
#        
#    def runNucOpt(self):
#        self.inputObjects[-1].submitJob(self.nucPpn, self.nucTime)
#        self.inputObjects[-1].checkJobStarted()        
#        self.inputObjects[-1].checkJobFinished()
#        outObj = pyq.EspressoOutputParser(self.inputObjects[-1].stub + '.out')
#        self.outputObjects.append(outObj)
#        outObj.getFinalPositions()
#        self.positions.append(outObj.finalPositions)
#        
#    def makecellParamInputObj(self):
#        arrayInputs = np.empty((3, 3), dtype = object)
#        for i in range(3):
#            for j in range(3):
#                inFile = copy.deepcopy(self.inputObjects[-1])
#                inFile.positions = self.positions[-1]
#                inFile.cellParams[i, j] += self.delta
#                inFile.control['calculation'] = '\'scf\''
#                inFile.write(inFile.stub +'Lattice'+str(i) + str(j) + '.in')
#                arrayInputs[i, j] = pyq.EspressoParser(inFile.stub +'Lattice'+ str(i) + str(j) + '.in') #Adds delta to all cell Parameters
#        self.inLatticeArays.append(arrayInputs) #Appends changed cell Parameters to inLatticeArrays
#        
#    def makeLinesearchInputObj(self, i, cellParams):
#        inFile = copy.deepcopy(self.inputObjects[-1])
#        inFile.cellParams = cellParams
#        inFile.control['calculation'] = '\'scf\''
#        inFile.write(inFile.stub +'Cycle' +str(self.cycleCounter)+ 'Linesearch' + str(i) + '.in')
#        inObj = pyq.EspressoParser(inFile.stub +'Cycle' +str(self.cycleCounter)+ 'Linesearch' + str(i) + '.in')
#        return inObj
#        
#    def runCellEnergyCalc(self): 
#        for i in range(3):
#            for j in range(3):
#                self.inLatticeArays[-1][i, j].submitJob(self.singlePpn, self.singlePointTime) #Submits singlePoint Calc for changed cell Parameters
#        for i in range(3):
#            for j in range(3):
#                self.inLatticeArays[-1][i, j].checkJobStarted()
#        for i in range(3):
#            for j in range(3):
#                self.inLatticeArays[-1][i, j].checkJobFinished()
#    
#    def createOutLattice(self): #creates array of outputObjects
#        latticeArray = np.empty((3, 3), dtype = object)
#        for i in range(3):
#            for j in range(3):
#                latticeArray[i, j] = pyq.EspressoOutputParser(self.inLatticeArays[-1][i, j].stub + ".out")
#        self.outLatticeArrays.append(latticeArray)
#        
#    def getLatticeEnergies(self): #creates array of energies from outputObjects
#        energyOutputs = np.zeros((3, 3))
#        for i in range(3):
#            for j in range(3):
#                energyOutputs[i, j] = self.outLatticeArrays[-1][i, j].extractEnergy()
#        self.latticeEnergies.append(energyOutputs)
#            
#    def getEnergyGrad(self):#calculates gradient from change in energies
#        gradient = np.zeros((3,3))
#        for i in range(3):
#            for j in range(3):
#                gradient[i, j] = (self.latticeEnergies[-1][i, j] - self.energies[-1])/self.delta
#        gradient = gradient.flatten()        
#        self.energyGrad.append(gradient)
#        
#    def inverseToNormalHessian(self):
#        for i in range(3):
#                for j in range(3):
#                        nonInverse = np.linalg.inv(self.inverHessians[-1][i, j])
#        self.hessians.append(nonInverse)
#        
#    def updateInverseHessian(self):
#        delg = self.energyGrad[-1] - self.energyGrad[-2]
#        delx = self.cellParams[-1] - self.cellParams[-2]
#        H = self.inverHessians[-1] 
#        u = (delx/np.dot(delx, delg)) - (np.dot(H, delg)/np.dot(delg, np.dot(H, delg)))
#        extra = np.outer((np.dot(delg, np.dot(H, delg))*u), u)
#        newHessian = H + (np.outer(delx, delx)/np.dot(delx, delg)) - (np.outer(np.dot(H, delg), np.dot(H, delg))/np.dot(delg, np.dot(H, delg))) + extra
#        self.inverHessians.append(newHessian)
#
#    def buildRotationalVectors(self):
#        cellA = np.zeros(shape=[3,3])
#        cellB = np.zeros(shape=[3,3])
#        cellC = np.zeros(shape=[3,3])
#        
#        cellA[1, 2] = -self.cellParams[-1][0, 0]
#        cellA[2, 1] = self.cellParams[-1][0, 0]
#        cellA[0, 2] = self.cellParams[-1][0, 1]
#        cellA[2, 0] = -self.cellParams[-1][0, 1]
#        cellA[0, 1] = -self.cellParams[-1][0, 2]
#        cellA[1, 0] = self.cellParams[-1][0, 2]
#
#        cellB[1, 2] = -self.cellParams[-1][1, 0]
#        cellB[2, 1] = self.cellParams[-1][1, 0]
#        cellB[0, 2] = self.cellParams[-1][1, 1]
#        cellB[2, 0] = -self.cellParams[-1][1, 1]
#        cellB[0, 1] = -self.cellParams[-1][1, 2]
#        cellB[1, 0] = self.cellParams[-1][1, 2]
#
#        cellC[1, 2] = -self.cellParams[-1][2, 0]
#        cellC[2, 1] = self.cellParams[-1][2, 0]
#        cellC[0, 2] = self.cellParams[-1][2, 1]
#        cellC[2, 0] = -self.cellParams[-1][2, 1]
#        cellC[0, 1] = -self.cellParams[-1][2, 2]
#        cellC[1, 0] = self.cellParams[-1][2, 2]
#        
#        rotCell = np.dot(cellA.transpose(), cellA) + np.dot(cellB.transpose(), cellB) + np.dot(cellC.transpose(), cellC)
#
#        eval, evec = np.linalg.eigh(rotCell)
#
#        rvec1 = np.zeros(9)
#        rvec2 = np.zeros(9)
#        rvec3 = np.zeros(9)
#
#        a = self.cellParams[-1][0, :]
#        b = self.cellParams[-1][1, :]
#        c = self.cellParams[-1][2, :]
#
#        w1 = evec[:, 0]
#        w2 = evec[:, 1]
#        w3 = evec[:, 2]
#
#        rvec1[0:3] = np.cross(a, w1)
#        rvec1[3:6] = np.cross(b, w1)
#        rvec1[6:9] = np.cross(c, w1)
#
#        rvec2[0:3] = np.cross(a, w2)
#        rvec2[3:6] = np.cross(b, w2)
#        rvec2[6:9] = np.cross(c, w2)
#
#        rvec3[0:3] = np.cross(a, w3)
#        rvec3[3:6] = np.cross(b, w3)
#        rvec3[6:9] = np.cross(c, w3)
#        
#        print rvec1
#        print rvec2
#        rvecs1 = np.vstack((rvec1, rvec2))
#        rvecs = np.vstack((rvecs1, rvec3)).transpose()
#        
#        self.rotationalVectors.append(rvecs)
#        
#    def calculateTransformationMatrix(self, hessian):
#        self.buildRotationalVectors()
#        hess = pyq.hessian.HessianManual(hessian)
#        hess.buildRotTransLessVecSpace(self.rotationalVectors[-1])
#        self.transformationMatrix.append(hess.vibBasis)
#        
#    def formInvHessianRot(self):
#        hNonInv = self.inverHessians[-1]
#        #transform hessian to cartesian space
#        vOld=self.transformationMatrix[-2]
#        HessCart = np.dot(vOld, np.dot(hNonInv,vOld.transpose()))
#        #calculate new basis to transform to
#        vNew = self.transformationMatrix[-1]
#        #transform to new basis
#        hNonInvNew = np.dot(vNew.transpose(),np.dot(HessCart,vNew))
#        #form inverse in the new space
#        hNew = np.linalg.inv(hNonInvNew)
#        
#        delg = self.energyGrad[-1] - self.energyGrad[-2]
#        delx = (self.cellParams[-1] - self.cellParams[-2]).flatten()
#        
#        vx = np.dot(vNew.transpose(),delx).flatten()
#        vg = np.dot(vNew.transpose(),delg).flatten()
#        vxvg = np.dot(vx,vg)
#        hg = np.dot(hNew,vg)
#        ghg = np.dot(vg,hg)
#        
#        u = (vx/vxvg)-(hg/ghg)
#        h1 = np.outer(vx,vx)/vxvg
#        h2 = np.outer(hg,hg)/ghg
#        h3 = ghg*(np.outer(u,u))
#        newInvHessian = hNew + h1-h2+h3
#        
#        self.inverHessians.append(newInvHessian)
#        
#    def checkConvergence(self):
#        disp = self.positions[-1]-self.positions[-2]
#        absDisp = np.absolute(disp)
#        dispMax = np.amax(absDisp)
#        
#        self.absDispList.append(dispMax)
#
#        grad = np.dot(self.transformationMatrix[-1], np.dot(self.transformationMatrix[-1].transpose(), self.energyGrad[-1]))
#        absGrad = np.absolute(grad)
#        gradMax = np.amax(absGrad)
#        
#        self.absGradList.append(gradMax)
#        
#        energyChange = np.abs(self.energies[-1]-self.energies[-2])
#        self.energyChangeList.append(energyChange)
#        
#        toleranceList = []
#        
#        if energyChange < self.energyTol:
#            toleranceList.append(1)
#        elif energyChange > self.energyTol:
#            toleranceList.append(0)
#        if dispMax < self.dispTol:
#            toleranceList.append(1)
#        elif dispMax > self.dispTol:
#            toleranceList.append(0)
#        if gradMax < self.gradTol:
#            toleranceList.append(1)
#        elif gradMax > self.gradTol:
#            toleranceList.append(0)
#            
#        handle = open('printedInfo', 'a')
#        handle.write('cycle' + str(self.cycleCounter) + '\n')
#        handle.write('energyChange ' + str(energyChange) + '\n')
#        handle.write('dispMax ' + str(dispMax) + '\n')
#        handle.write('gradMax ' + str(gradMax) + '\n')
#        handle.write('energy ' + str(self.energies[-1])+ '\n')
#	
#	
#        total = np.sum(toleranceList)
#        if total == 3:
#            self.convergence.append([True])
#        elif total < 3:
#            self.convergence.append([False])
#            
#    def stepBFGS(self):               
#        if self.cycleCounter%10 == 0:
#            tentativeInvHess = np.eye(9)
#            self.calculateTransformationMatrix(tentativeInvHess)
#            transformMat = self.transformationMatrix[-1]
#            self.inverHessians.append(np.linalg.inv(np.dot(transformMat.transpose(),np.dot(tentativeInvHess, transformMat))))
#        else:
#            self.calculateTransformationMatrix(np.dot(self.transformationMatrix[-1],np.dot(np.linalg.inv(self.inverHessians[-1]),self.transformationMatrix[-1].transpose())))
#            self.formInvHessianRot()
#            transformMat = self.transformationMatrix[-1]
#            self.transformationMatrix.append(transformMat)
#
#        Vmat = self.transformationMatrix[-1]
#        Hinv = self.inverHessians[-1]
#        stepDirection = -np.dot(Vmat,(np.dot(Hinv, np.dot(Vmat.transpose(), self.energyGrad[-1]))))
#
#        self.step.append(stepDirection)
#                    
#            
#    def lineSearch(self):
#            step = self.step[-1]
#            if np.amax(np.absolute(step)) < 0.1:
#                cellParams = self.cellParams[-1] + np.reshape(step, (3, 3))
#            else:
#                scaleFactor = 0.1/np.linalg.norm(step)
#                step = scaleFactor * step
#                cellParams = self.cellParams[-1] + np.reshape(step, (3, 3))
#
#            for i in range(3):
#                inObj = self.makeLinesearchInputObj(i, cellParams)
#                inObj.submitJob(self.singlePpn, self.singlePointTime)
#                inObj.checkJobStarted()
#                inObj.checkJobFinished()
#                outObj = pyq.EspressoOutputParser(inObj.stub + '.out')
#                energy = outObj.extractEnergy()
#                if energy > self.energies[-1]:
#                    cellParams = self.cellParams[-1] + (3.00-i)/4.00*np.reshape(step, (3,3))
#                elif energy < self.energies[-1]:
#                    break
#            self.cellParams.append(cellParams)
#            self.energies.append(energy)
