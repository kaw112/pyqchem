import subprocess
import re
import struct
import numpy as np
import pyqchem
import matplotlib.pyplot as plt
regex_number = re.compile(r'((?:[-\+]?\d+\.?\d*)(?:[eE](?:[+-])?\d+)?)')

def submit(file_name, time):
    """
    Call my convoluted submit script.
    """
    file_name = file_name.rstrip(r'.n')
    subprocess.call(("submit", file_name, "1", str(time)))

def get_numbers(string, pytype=float):
    """
    Parse string and find all number in string

    :param string: string to parse
    :param pytype: python type to convert found numbers to
    """
    numbers = regex_number.findall(string)
    if not pytype is None:
        numbers = [pytype(s) for s in numbers]
    return numbers
    
def checkJobFinished(fileName, numIndex = 1):
    """
    
    """
    handle = open(fileName, 'r')
    i = 0
    for x, line in enumerate(handle):
        if "Thank you very much" in line:
            i  = i+1
            if i==numIndex:
                return True
            else:
                pass
        else:
            pass
    return False      
    
def checkJobFinishedEspresso(fileName):
    handle = open(fileName, 'r')
    for line in handle:
        if "JOB DONE." in line:
            return True
        else:
            pass
    return False

def get_string_index(list_of_strings, string_to_search, start_index=0):
    """
    Returns the index for the line containing the given string.
    (case-sensitive)

    :param list_of_strings: a list of strings that might contain the string_to_search
    :param string_to_search: the string we are looking for
    :param start_index: optional starting point in list_of_strings (zero-based)
    """
    for idx, line in enumerate(list_of_strings[start_index:]):
        if (line.find(string_to_search) > -1):
            return idx
    return -1

def get_regex_index(list_of_strings, regex_to_search, start_index=0):
    """
    Returns the index for the line matching the given regular expression
    string. (case-sensitive)

    :param list_of_strings: a list of strings that might contain the string_to_search
    :param regex_to_search: the regular expression we want to match
    :param start_index: optional starting point in list_of_strings (zero-based)
    """
    for idx, line in enumerate(list_of_strings[start_index:]):
        if (re.search(regex_to_search, line) is not None):
            return idx
    return -1

def removeTransRot(masses, positions, momentInertiaTensor):
    return 0
    
    

def gramSchmidt(vecMatrix):
    ###assume vectors are in columns###
    for i in range(np.shape(vecMatrix)[1]):
        vec = vecMatrix[:,i]
        for j in range(0,i):
            vec = vec - np.dot(vec, vecMatrix[:,j])*vecMatrix[:,j]
        if np.amax(np.abs(vec))<1e-5:
            vecMatrix[:,i]=np.zeros(np.shape(vecMatrix)[0])
        else:
            vecMatrix[:,i]=vec/np.linalg.norm(vec)

    deleteList = []
    for i in range(np.shape(vecMatrix)[1]):
        if np.amax(np.abs(vecMatrix[:,i]))<1e-6:
            deleteList.append(i)
        
    vecMatrix = np.delete(vecMatrix, deleteList, 1) 
    
    return vecMatrix
    
    
def centerOfMass(masses, positions):
    totMass = np.sum(masses)
    massPositions = np.zeros((len(masses), 3))
    
    for i, mass in enumerate(masses):
        massPositions[i, :]= mass*positions[i,:]
        
    cOM= np.sum(massPositions, axis = 0)/totMass
    
    return cOM
    
def formCOMPositions(cOM, positions):
    
    relPositions = np.zeros(np.shape(positions))
    
    for i in range(np.shape(positions)[0]):
        relPositions[i,:]= positions[i,:]-cOM
        
    return relPositions
        
def vmdVideoFromQchemOutfiles(fileName, fileList):
    """Construct ordered, nested xyz file for vmd to read from a set of 
    output files from geometry optimizations from qchem
    param fileList: list of output files; must be ordered in the desired way you would like to view video
    parma fileName: name of file to add the xyz geometries to
    """
    
    handle = open(fileName, 'w')
    
    s = ''
    for job in fileList:
        out = pyqchem.qchem_parser.QChemOutputParser(job)
        out.extractOptimizedPositions()
        s += str(out.size) + '\n\n'
        for i in range(out.size):
            s+= out.atoms[i] + '  ' + str(out.optPositions[i,0]) + '  ' + \
            str(out.optPositions[i,1]) + '  ' + str(out.optPositions[i,2]) + '\n'
        
        
        
    handle.write(s)
    
    return None
            
def plotPolyFit(x,y,deg, saveFile = None):
    """plot data and a polyfit equation of order "deg." Saved to saveFile if specified"""
    
    p = np.polyfit(x,y, deg)
    
    xp = np.linspace(np.amin(x), np.amax(x), 40)
    
    f = np.poly1d(p)
    
    yp = f(xp)
    
    plt.plot(x, y, '.',xp, yp, '-')
    
    if saveFile == None:
        pass
    else:
        plt.savefig(saveFile)
    
    plt.show()

class BinaryReaderEOFException(Exception):
    def __init__(self):
        pass
    def __str__(self):
        return 'Not enough bytes in file to satisfy read request'

class BinaryReader:
    # Map well-known type names into struct format characters.
    typeNames = {
        'int8'   :'b',
        'uint8'  :'B',
        'int16'  :'h',
        'uint16' :'H',
        'int32'  :'i',
        'uint32' :'I',
        'int64'  :'q',
        'uint64' :'Q',
        'float'  :'f',
        'double' :'d',
        'char'   :'s'}

    def __init__(self, fileName):
        self.file = open(fileName, 'rb')
        
    def read(self, typeName):
        typeFormat = BinaryReader.typeNames[typeName.lower()]
        typeSize = struct.calcsize(typeFormat)
        value = self.file.read(typeSize)
        if typeSize != len(value):
            raise BinaryReaderEOFException
        return struct.unpack(typeFormat, value)[0]
    
    def __del__(self):
        self.file.close()