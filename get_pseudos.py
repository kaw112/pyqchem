# -*- coding: utf-8 -*-
"""
Created on Tue Aug 13 14:13:01 2019

@author: michal
"""
pseudoDict = {'h': 'H.pbe-kjpaw_psl.1.0.0.UPF ', 'li': 'Li.pbe-s-kjpaw_psl.1.0.0.UPF', 'b': 'B.pbe-n-kjpaw_psl.1.0.0.UPF', 'c': 'C.pbe-n-kjpaw_psl.1.0.0.UPF', 'n': 'N.pbe-n-kjpaw_psl.1.0.0.UPF', 'o': 'O.pbe-n-kjpaw_psl.1.0.0.UPF', 'f': 'F.pbe-n-kjpaw_psl.1.0.0.UPF', 'si': 'Si.pbe-n-kjpaw_psl.1.0.0.UPF', 'p': 'P.pbe-n-kjpaw_psl.1.0.0.UPF', 's': 'S.pbe-n-kjpaw_psl.1.0.0.UPF', 'cl': 'Cl.pbe-n-kjpaw_psl.1.0.0.UPF', 'br': 'Br.pbe-dn-kjpaw_psl.1.0.0.UPF', 'i': 'I.pbe-n-kjpaw_psl.1.0.0.UPF'}
import os
import pyqchem as pyq
def getPseudos():
    a = os.listdir(os.getcwd())
    for fileName in a :
        inputObject = pyq.EspressoParser(fileName)
        atoms  = list(set(inputObject.atoms))
        inputObject.pseudos = {}
        for atom in atoms:
            inputObject.pseudos[atom.lower()] = pseudoDict[atom.lower()]
        inputObject.system['vdw_corr'] = 'grimme-d2'
        inputObject.write(inputObject.stub + '.in')
        
