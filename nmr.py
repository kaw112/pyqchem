import numpy as np
import scipy.linalg as spl
import math
import pyqchem

class NMR:
    """
    Store, extract, and handle all information 
    from an NMR shielding tensor job.
    """
    def __init__(self, job):

        self.dushifts = []
        self.pushifts = []
        self.pdshifts = []
        self.totshifts = []

        self.traces_iso_du = []
        self.traces_iso_pu = []
        self.traces_iso_pd = []
        self.traces_iso_tot = []

        self.traces_aniso_du = []
        self.traces_aniso_pu = []
        self.traces_aniso_pd = []
        self.traces_aniso_tot = []

        self.dushift_eigvals = []
        self.pushift_eigvals = []
        self.pdshift_eigvals = []
        self.totshift_eigvals = []
        self.dushift_iso = []
        self.pushift_iso = []
        self.pdshift_iso = []
        self.totshift_iso = []

        self._extract_nmr(job)

    def __str__(self):
        s = ""
        s += "NMR info for {0}:\n".format(job.file_name)
        return s

    def _extract_nmr(self, job):
        """
        Extract all information from an NMR shielding tensor job.
        """

        # first, find the section
        string_to_search = "SHIELDING TENSORS (SCF)"
        idx_section = pyqchem.utils.get_string_index(job.lines, string_to_search)
        if (idx_section == -1): return

        # now, find each atom
        for atom_num in range(job.size):
            self._extract_atom_shifts(job, atom_num, idx_section)

    def _extract_atom_shifts(self, job, atom_num, idx_section):
        """
        Extract all the NMR chemical shift information for a single atom.
        """

        # find the atom header somewhere in the file after idx_section
        regex_to_search = "ATOM\s+{}\s+{}".format(job.atoms[atom_num], atom_num+1)
        idx = pyqchem.utils.get_regex_index(job.lines, regex_to_search, idx_section)
        if (idx == -1): return
        idx += idx_section

        # -------------------
        # [00] -- ATOM  C     1 --
        # -------------------

        # [02] ISOTROPIC:    127.47253379          ANISOTROPIC:     89.21186163

        #  diamagnetic (undisturbed density) part of shielding tensor  (EFS)
        #     Trace =    252.75861654
        #     Full Tensor:
        # [06]              262.08127346           -0.50365728           -3.16492766
        # [07]               -0.29050484          244.07156498            0.33545037
        # [08]               -3.17984195            0.94413708          252.12301119
        #  paramagnetic (undisturbed density) part of shielding tensor (SOILP)
        #     Trace =     -6.74118744
        #     Full Tensor:
        # [12]               -2.07595440           -1.01645710            7.16919424
        # [13]               -0.13656778           -9.31507225            0.50982933
        # [14]               -0.67512060            0.52920485           -8.83253567
        #  paramagnetic (disturbed density) part of shielding tensor   (SOI)
        #     Trace =   -118.54489531
        #     Full Tensor:
        # [18]             -105.57351432           -5.01191613           34.11566990
        # [19]              -24.66898409         -131.34143241            7.22106144
        # [20]               48.76797957           -0.11109663         -118.71973921
        #  total shielding tensor
        #     Trace =    127.47253379
        #     Full Tensor:
        # [24]              154.43180473           -6.53203051           38.11993648
        # [25]              -25.09605671          103.41506032            8.06634114
        # [26]               44.91301702            1.36224529          124.57073631

        dushift = np.array([job.lines[idx+6].split(),
                            job.lines[idx+7].split(),
                            job.lines[idx+8].split()], dtype=np.float64)
        pushift = np.array([job.lines[idx+12].split(),
                            job.lines[idx+13].split(),
                            job.lines[idx+14].split()], dtype=np.float64)
        pdshift = np.array([job.lines[idx+18].split(),
                            job.lines[idx+19].split(),
                            job.lines[idx+20].split()], dtype=np.float64)
        totshift = dushift + pushift + pdshift
        self.dushifts.append(dushift)
        self.pushifts.append(pushift)
        self.pdshifts.append(pdshift)
        self.totshifts.append(totshift)

        # we can calculate the traces, but why not just read them in?
        trace_iso_tot = float(job.lines[idx+2].split()[1])
        trace_aniso_tot = float(job.lines[idx+2].split()[3])
        trace_iso_du = float(job.lines[idx+4].split()[-1])
        trace_iso_pu = float(job.lines[idx+10].split()[-1])
        trace_iso_pd = float(job.lines[idx+16].split()[-1])
        self.traces_iso_tot.append(trace_iso_tot)
        self.traces_aniso_tot.append(trace_aniso_tot)
        self.traces_iso_du.append(trace_iso_du)
        self.traces_iso_pu.append(trace_iso_pu)
        self.traces_iso_pd.append(trace_iso_pd)

        # trace_iso_du = self.calc_trace_iso(dushift)
        # trace_iso_pu = self.calc_trace_iso(pushift)
        # trace_iso_pd = self.calc_trace_iso(pdshift)
        # trace_iso_tot = trace_iso_du + trace_iso_pu + trace_iso_pd
        # self.traces_iso_du.append(trace_iso_du)
        # self.traces_iso_pu.append(trace_iso_pu)
        # self.traces_iso_pd.append(trace_iso_pd)
        # self.traces_iso_tot.append(trace_iso_tot)

        # trace_aniso_du = self.calc_trace_aniso(dushift)
        # trace_aniso_pu = self.calc_trace_aniso(pushift)
        # trace_aniso_pd = self.calc_trace_aniso(pdshift)
        # trace_aniso_tot = trace_aniso_du + trace_aniso_pu + trace_aniso_pd
        # self.traces_aniso_du.append(trace_aniso_du)
        # self.traces_aniso_pu.append(trace_aniso_pu)
        # self.traces_aniso_pd.append(trace_aniso_pd)
        # self.traces_aniso_tot.append(trace_aniso_tot)

        dushift_eigvals, dushift_iso = self._diag(dushift)
        pushift_eigvals, pushift_iso = self._diag(pushift)
        pdshift_eigvals, pdshift_iso = self._diag(pdshift)
        totshift_eigvals, totshift_iso = self._diag(totshift)
        self.dushift_eigvals.append(dushift_eigvals), self.dushift_iso.append(dushift_iso)
        self.pushift_eigvals.append(pushift_eigvals), self.pushift_iso.append(pushift_iso)
        self.pdshift_eigvals.append(pdshift_eigvals), self.pdshift_iso.append(pdshift_iso)
        self.totshift_eigvals.append(totshift_eigvals), self.totshift_iso.append(totshift_iso)

    def _calc_trace_iso(self, s):
        """
        Calculate the isotropic component (trace) of a given square NumPy array s.
        """
        return np.trace(s) / s.shape[0]

    def _calc_trace_aniso(self, s):
        """
        Calculate the anisotropic component (trace) of a given square NumPy array s.
        """
        trace_aniso = 0.0
        for j in range(s.shape[0]):
            for i in range(s.shape[0]):
                trace_aniso += (s[i,j] + s[j,i])**2
        trace_aniso /= 4.0
        trace_aniso = math.sqrt(abs(1.5*(trace_aniso - 3.0*self.calc_trace_iso(s)**2)))
        return trace_aniso

    def _diag(self, s):
        """
        Diagonalize the given square NumPy matrix s, resulting in eigenvalues
        and an isotropic component.
        """
        eigvals = np.sqrt(spl.eigvalsh(np.dot(s.T, s)))
        iso = np.sum(eigvals) / float(len(eigvals))
        return eigvals, iso
