#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  3 16:12:59 2018

@author: keith
"""
import pyqchem
import numpy as np
import matplotlib.pyplot as plt
import numpy as np

def newtonMethod(x, y, x0):
    p = np.polyfit(x, y, len(x)-1)
    f = np.poly1d(p)
    firstDeriv = np.polyder(f)
    secDeriv = np.polyder(firstDeriv)
    xList = []
    xList.append(x0)
    valList = []
    valList.append(firstDeriv(x0))
    slopeList = []
    slopeList.append(secDeriv(x0))
    for i in range(100):
        x = xList[-1] - valList[-1]/slopeList[-1]
        xList.append(x)
        valList.append(firstDeriv(x))
        slopeList.append(secDeriv(x))
        if firstDeriv(x)< 1e-8:
            break
    return xList[-1], f



def makeEspressoInputs(inFile, percentInt, numFiles):
    fields = np.eye(3)* 0.000275
    fieldLabel = ['x', 'y', 'z']
    cellLabel = ['a','b','c']
    for i in range(3):
        for j in range(3):
            for k in range(numFiles):
                a = pyqchem.qchem_parser.EspressoParser(inFile)
                a.addField(fields[i,:])
                percentChange = -(numFiles-1)/2.0 * percentInt +k*percentInt
                percentArray = np.zeros((3,3))
                iArray = np.eye(3)
                percentArray[j,:] = percentChange* iArray[j,:]
                a.percentCellParamChange(percentArray)
                a.write(a.stub + fieldLabel[i] + cellLabel[j] + str(k)+ '.in')
                
def analysisEspressoOutputs(infile, numFiles):
    inFileObj = pyqchem.EspressoParser(infile)
    outFileObj =pyqchem.EspressoOutputParser(inFileObj.stub + '.out')
    # indices go file number, cell coord, field 
    inArray = np.empty((numFiles, 3, 3), dtype = type(inFileObj))
    outArray = np.empty((numFiles, 3, 3), dtype = type(outFileObj))

    cellCoord = ['a','b', 'c']
    fields = ['x','y', 'z']
    for i in range(numFiles):
        for j, coord in enumerate(cellCoord):
            for k, field in enumerate(fields):
                stub = inFileObj.stub+ field + coord + str(i)
                inFile = pyqchem.EspressoParser(stub + '.in')
                outFile = pyqchem.EspressoOutputParser(stub + '.out')
                inArray[i,j, k] = inFile
                outArray[i, j, k] = outFile
                
    percentArray = np.zeros((numFiles, 3, 3))
    energyArray = np.zeros((numFiles, 3,3))
    
    for i in range(numFiles):
        for j in range(3):
            for k in range(3):
                energyArray[i,j,k]= outArray[i,j,k].extractEnergy()
                cellParam0 = np.linalg.norm(inFileObj.cellParams[j,:])
                percentArray[i,j,k]= (np.linalg.norm(inArray[i,j,k].cellParams[j,:])-cellParam0)/cellParam0
                
    xMinArray = np.zeros((3,3))       
    #i = a,b,or c j = fx,fy,fz     
    for i in range(3):
        for j in range(3):
            fig, ax = plt.subplots()
            xData = percentArray[:, i, j]
            yData = energyArray[:, i, j]    
            xmin, f = newtonMethod(xData, yData, 0.0)
            xMinArray[i,j]= xmin
            ax.scatter(xData, yData)
            x = np.linspace(xData[0], xData[-1],30)
            ax.plot(x, f(x), 'b-')
            plt.text(0.5, 0.5, 'xmin = '+ str(xmin) , horizontalalignment='center', verticalalignment='center', transform=ax.transAxes)            
            plt.savefig(cellCoord[i]+fields[j] + '.png')
            plt.clf()
            
            
            