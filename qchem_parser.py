import numpy as np
import pyqchem
import sys
import re
import math
import os
import subprocess
import time
from scipy.spatial.distance import cdist
import cclib
#import scipy.cluster.hierarchy.linkage as sciLink

def rabbits(func):
    def wrap(*args, **kwargs):
        print 'rabbits!'
        return func(*args, **kwargs)
    return wrap

massDict = {'h' :  1.008,
            'he': 4.0026,
            'li': 6.94,
            'be': 9.0122,
            'b' : 10.81,
            'c' : 12.011,
            'n' : 14.007,
            'o' : 15.999,
            'f' : 18.998,
            'ne': 20.180,
            'na': 22.990,
            'mg': 23.305,
            'al': 26.982,
            'cl': 35.453,
            'br': 79.904,
            'i' : 126.90,
            'p' : 30.975,
            'se': 78.96,
            's' : 32.06,
            'si': 28.085,
            'ca': 40.078
            }
# bond radii in angstroms
bondRadii = {'h' :  0.25,
            'li': 1.45,
            'be': 1.05,
            'b' : 0.85,
            'c' : 0.70,
            'n' : 0.65,
            'o' : 0.6,
            'f' : 0.5,
            'na': 1.80,
            'mg': 1.50,
            'al': 1.25,
            'cl': 1.0,
            'br': 1.15,
            'i' : 1.40,
            'p' : 1.00,
            'se': 1.15,
            'si' : 1.18, 
            's' : 1.0
            }
atomNumDict = {'1' : 'h',
               '2': 'he',
               '3': 'li',
               '4' : 'be',
               '5' : 'b',
               '6' : 'c',
               '7' : 'n',
               '8' : 'o',
               '9' : 'f',
               '10' : 'ne',
               '11' : 'na',
               '12' : 'mg',
               '13' : 'al',
               '14' : 'si',
               '15' : 'p',
               '16' : 's',
               '17' : 'cl',
               '34' : 'se',
               '35' : 'br',
               '53' : 'i',
               }
class QChemParser:
    """
    A parser to parser qchem input files.

    :param file_name: the qchem input file name
    :type file_name: str

    >>> molecule = QChemParser('test.n')
    >>> print molecule.charge
    >>> molecule.rem['exchange'] = 'HF'
    """
    def __init__(self, file_name):
        # order can be important here
        self.load(file_name)
        self._parse_rem()
        self._parse_molecule()
        self._parse_fields()
        self._set_masses()
        self._setCenterOfMass()
        self._setGeometricCenter()
        self._set_moment_of_inertia_tensor()
        self._set_rotational_tensor()
        self._constructTranslationRotationMatrix()

    def __str__(self):
        s  = 'file: %s\n' % self.file_name
        s += 'size: %d\n' % self.size
        s += 'charge: %d\n' % self.charge
        s += 'multiplicity: %d\n' % self.multiplicity
        return s.rstrip()

    @rabbits
    def to_string(self):
        """
        """
        s = ''
        s = s + '%s\n' % self.format_rem()
        s = s + '%s\n' % self.format_multipole_fields()
        s = s + '%s\n' % self.format_molecule()
        s = s + '%s\n' % self.format_basis()
        return s.rstrip()

    @rabbits
    def reset(self):
        """
        Reset parameters to default values
        """
        self.file_name = None
        self.stub = None
        self.lines = []
        self.charge = 0
        self.multiplicity = 1
        self.atoms = []
        self.masses = []
        self.positions = []
        self.massPositions = []
        self.comRelPositions = []
        self.geomCenRelPositions = []
        self.size = 0
        self.fields = {}
        self.momentInertiaTensor = []
        self.rotationalTensor = []
        self.rotTransMat = []
        self.centerOfMass = 0.0
        self.geometricCenter = 0.0
        self.basis = ''
        self.rem={}
        self.command='submitH2P'
        self.queue='shared'
        self.ppn=1
        self.timing=96

    def load(self, file_name):
        """
        Load all lines from file into a list (self.lines)

        :param file_name: the qchem input file name
        :type file_name: str
        """
        self.reset()
        stub, ext = os.path.splitext(file_name)
        self.stub = stub
        self.file_name = file_name
        self.lines = []

        handle = open(self.file_name, 'r')
        line = handle.readline()
        while line:
            line = line.strip()
            if line:
                self.lines.append(line)
            line = handle.readline()

        handle.close()

    @rabbits
    def _parse_molecule(self):
        """
        parse the $molcule keyword for atom names, positions,
        charge and multiplicity.
        """
        lines = self._extract('$molecule')

        tokens = lines[0].strip().split()
        self.charge = int(tokens[0])
        self.multiplicity = int(tokens[1])

        atoms = []
        positions  = []

        for line in lines[1:]:
            tokens = line.strip().split()
            atoms.append(tokens[0])
            floats = []
            for token in tokens[1:]:
                floats.append(float(token))
            positions.append(floats)

        self.atoms = np.array(atoms, dtype='|S16')
        self.positions = np.array(positions)
        self.size = self.atoms.size
        print self.positions
    def _parse_basis(self):
        lines = self._extract('$basis')
        
        self.basis = lines
    @rabbits
    def _set_masses(self):
        """make a list of masses corresponding to the
        masses of the atoms in self.atoms
        """
        self.masses = []
        for key in self.atoms:
            try:
                self.masses.append(massDict[key.lower()])
            except:
                self.masses.append(massDict[atomNumDict[key]])

    @rabbits
    def _parse_rem(self):
        """
        parse the $rem keyword and save key, values into a dictionary.

        To update that dictionary (which i'm totally going to forget how to do):

        >>> self.rem[key] = value # inside the class
        >>> molecule_1.rem[key] = value # outside the class
        """
        lines = self._extract('$rem')
        self.rem = {}
        for line in lines:
            line = re.sub('=', ' ', line) # substitue equal sign for space
            tokens = [token.strip() for token in line.strip().split()] # split up lines
            if (len(tokens) == 2):
                key, value = tokens
                self.rem[key.lower()] = value.lower() # insert key into dict

    @rabbits
    def _parse_fields(self):
        lines = self._extract('$multipole_field')
        self.fields = {}
        for line in lines:
            line = re.sub('=', ' ', line)
            tokens = [token.strip() for token in line.strip().split()]
            key, value = tokens[0], float(tokens[1])
            self.fields[key.lower()] = value

    @rabbits
    def _parse_charges(self):
        pass

    @rabbits
    def _extract(self, keyword):
        """
        Find lines from keyword until $end.

        :param keyword: qchem input file keyword
        :type keyword: str
        """
        try:
            start_index = self.lines.index(keyword)
        except ValueError:
            print 'keyword not found: %s' % keyword
            return []

        for i in range(start_index, len(self.lines)):
            if '$end' in self.lines[i]:
                stop_index = i
                break
            elif i == len(self.lines) - 1:
                print 'can not find $end for keyword: %s' % keyword
                sys.exit(-1)

        return self.lines[start_index + 1:stop_index]

    @rabbits
    def format_rem(self):
        s = '$rem\n'
        for key, value in self.rem.iteritems():
            s += '%-20s %s\n' % (key, value)
        s += '$end\n'
        return s

    def format_basis(self):
        s = '$basis\n'
        s += self.basis
        s += '$end\n'
        return s
        
    @rabbits
    def format_molecule(self):
        s = '$molecule\n'
        s = s + '%d %d\n' % (self.charge, self.multiplicity)
        for i in range(self.size):
            x, y, z = self.positions[i]
            s += '%-3s %10.10f %10.10f %10.10f\n' % (self.atoms[i], x, y, z)
        s = s + '$end\n'
        return s

    @rabbits
    def format_multipole_fields(self):
        s = '$multipole_field\n'
        for key, value in self.fields.iteritems():
            s = s + '%-3s %10.8f\n' % (key, value)
        s = s + '$end\n'
        return s

    @rabbits
    def write_xyz(self, filename):
        handle = open(filename, 'w')
        handle.write(str(self.size) + '\n\n')
        s = ''
        for i in range(self.size):
            x, y, z = self.positions[i]
            s += '%-3s %10.10f %10.10f %10.10f\n' % (self.atoms[i], x, y, z)
        handle.write(s)

    @rabbits
    def save(self, output_name):
        handle = open(output_name, 'w')
        handle.write(self.to_string())
        handle.close()

    @rabbits
    def addJobandSave(self, jobtype, output_name):
        """
        add a job in addition to the end of the one already present in the calculation
        """
        handle = open(output_name, 'w')
        handle.write(self.to_string())
        self.rem['jobtype'] = jobtype
        s = ''
        s = s + '%s\n' % self.format_rem()
        s = s + '%s\n' % self.format_multipole_fields()
        s.rstrip()
        handle.write('\n\n@@@@\n\n')
        handle.write(s + '\n')
        handle.write('$molecule\n')
        handle.write('read\n')
        handle.write('$end\n')
        handle.close()

    @rabbits
    def interatomic_distance(self, atoms_index):
        """
        gets distance between two atoms in file

        param atoms_index: constains index of positions of atoms of interest 0 is first atom
        type atoms_index: list
        """
        first_atom = self.positions[atoms_index[0]]
        second_atom = self.positions[atoms_index[1]]
        difference_vector = first_atom - second_atom
        distance = np.linalg.norm(difference_vector)
        return distance

    @rabbits
    def calc_angle(self, atoms_index):
        """
        gets angle between three atoms in file

        param atoms_index: contains index of positions of atoms of interest 0 is first atom
        type atoms_index: list
        """
        first_atom = self.positions[atoms_index[0]]
        second_atom = self.positions[atoms_index[1]]
        third_atom = self.positions[atoms_index[2]]
        vec12 = first_atom - second_atom
        vec23 = second_atom - third_atom
        angle = np.dot(vec12, vec23) * (180.0/math.acos(-1.0))
        return angle

    @rabbits
    def interatomic_distance_vector(self, atoms_index):
        first_atom = self.positions
        first_atom = self.positions[atoms_index[0]]
        second_atom = self.positions[atoms_index[1]]

        difference_vector = first_atom - second_atom

        return difference_vector

    @rabbits
    def setPositions(self, xyzArray):
        """
        sets new positions for the system
        each row is of course the x, y, and z coordinate
        """
        self.positions = xyzArray
        self._setCenterOfMass()
        self._setGeometricCenter()
        self._set_moment_of_inertia_tensor()
        self._set_rotational_tensor()
        self._constructTranslationRotationMatrix()

    @rabbits
    def setAtoms(self, atomsArray):
        """
        sets new atoms for the system
        """
        self.atoms = atomsArray

    @rabbits
    def lastInstance(self, regex, lines):
        """
        returns the index of the last instance of a regular expression being
        searched for in list of lines
        """
        index = -1
        for i, line in enumerate(lines):
            if (re.search(regex,line) is not None):
                index = i
        if index == -1:
            raise IndexError('regular expression not found')
        else:
            return index

    @rabbits
    def firstInstance(self, regex, lines):
        index = -1
        for i, line in enumerate(lines):
            if (re.search(regex, line) is not None):
                index = i
                break
        if index == -1:
            raise IndexError('regular expression not found')
        else:
            return index

    @rabbits
    def _set_moment_of_inertia_tensor(self):
        #calculate moment of inertia tensor

        self.comRelPositions = np.zeros(np.shape(self.positions))

        for i in range(self.size):
            self.comRelPositions[i,:]= self.positions[i,:] - self.centerOfMass


        self.momentInertiaTensor = np.zeros((3,3))

        #first the diagonal
        for k in range(self.size):
            self.momentInertiaTensor[0,0] += self.masses[k]*(self.comRelPositions[k,1]**2 +self.comRelPositions[k,2]**2)
            self.momentInertiaTensor[1,1] += self.masses[k]*(self.comRelPositions[k,0]**2 +self.comRelPositions[k,2]**2)
            self.momentInertiaTensor[2,2] += self.masses[k]*(self.comRelPositions[k,0]**2 +self.comRelPositions[k,1]**2)

        #now ze off-diagonals
        for k in range(self.size):
            for i in range(3):
                for j in range(i+1,3):
                    self.momentInertiaTensor[i,j]+= -self.masses[k]*self.comRelPositions[k,i]*self.comRelPositions[k,j]
                    self.momentInertiaTensor[j,i]= self.momentInertiaTensor[i,j]

    @rabbits
    def getSubCenterOfMass(self, atomList):
        """
        returns a center of mass for a subset of atoms in a molecule.  The atoms indexes are given in atomList
        """
        subMassTot= 0.0
        subCOM= np.zeros(3)
        for index in atomList:
            subMassTot += self.masses[index]
            subCOM += self.masses[index]*self.positions[index,:]

        subCOM = subCOM/subMassTot

        return subCOM


    @rabbits
    def _setCenterOfMass(self):
        #set the center of mass for the system
        massTot = sum(self.masses)

        self.massPositions = np.zeros(np.shape(self.positions))

        for i in range(self.size):
            self.massPositions[i,:] = self.masses[i]*self.positions[i,:]

        self.centerOfMass = np.sum(self.massPositions, axis = 0)/massTot

    @rabbits
    def _setGeometricCenter(self):
        self.geometricCenter = np.sum(self.positions, axis = 0)/self.size

    def getSubGeomCenter(self, atomList):
        """ find the geometric center for a subset of atoms in the system"""

        totAtoms = len(atomList)
        subGC = np.zeros(3)
        for index in atomList:
            subGC += self.positions[index,:]

        subGC = subGC/float(totAtoms)
        return subGC

    @rabbits
    def _set_rotational_tensor(self):

        self.geomCenRelPositions = np.zeros(np.shape(self.positions))

        for i in range(self.size):
            self.geomCenRelPositions[i,:]= self.positions[i,:] - self.geometricCenter

        self.rotationalTensor = np.zeros((3,3))

                #first the diagonal
        for k in range(self.size):
            self.rotationalTensor[0,0] += (self.geomCenRelPositions[k,1]**2 +self.geomCenRelPositions[k,2]**2)
            self.rotationalTensor[1,1] += (self.geomCenRelPositions[k,0]**2 +self.geomCenRelPositions[k,2]**2)
            self.rotationalTensor[2,2] += (self.geomCenRelPositions[k,0]**2 +self.geomCenRelPositions[k,1]**2)

        #now ze off-diagonals
        for k in range(self.size):
            for i in range(3):
                for j in range(i+1,3):
                    self.rotationalTensor[i,j]+= -self.geomCenRelPositions[k,i]*self.geomCenRelPositions[k,j]
                    self.rotationalTensor[j,i]= self.rotationalTensor[i,j]


    @rabbits
    def _constructTranslationRotationMatrix(self):
        ###construct the rotation translation matrix vector matrix for the molecule using rotational tensor###

        ###translation matrix   [[1,0,0,1,0,0,...1,0,0],[0,1,0,0,1,0 ...] ...]###
        T = np.zeros((3, self.size*3))
        for i in range(self.size):
            for j in range(3):
                T[j,i*3 + j] = 1.0

        ###rotation Matrix###
        eigval, eigvec = np.linalg.eigh(self.rotationalTensor)

        ###check for linearity of molecule###
        delList = []
        for i, value in enumerate(eigval):
            if np.abs(value) < 1e-5:
                delList.append(i)
            else:
                continue
        eigvec = np.delete(eigvec, delList, 1)

        ##############
        R = np.zeros((np.shape(eigvec)[1], self.size*3))

        for i in range(self.size):
            for j in range(np.shape(eigvec)[1]):
                velocity = np.cross(eigvec[:,j], self.geomCenRelPositions[i,:])
                R[j,i*3:i*3+3] = velocity

        ###normalize the vectors###
        for i in range(3):
            T[i,:] = T[i,:]/np.linalg.norm(T[i,:])

        for i in range(np.shape(eigvec)[1]):
            R[i,:] = R[i,:]/np.linalg.norm(R[i,:])

        ###combine###
        D = np.vstack((T,R))
        print "shape of D matrix \n"
        print np.shape(D)
        #set as column vector matrix
        self.rotTransMat = D.transpose()

    def runJobInBackground(self):
        commandString = r'{command} {stub}'

        try:
            handle = open(self.stub+ '.out', 'r' )
            handle.close()
            if pyqchem.utils.checkJobFinished(self.stub + '.out')== True:
                pass

        except:
            subprocess.call(commandString.format(command='xbackground', stub = self.stub).split(), 0)
            
    def submitJob(self, command, ppn, timing):
        self.command=command
        self.ppn=ppn
        self.timing=timing

        commandString = r'{command} {stub} {coresToRun:d} {timeToRun:d}'

        try:
            handle = open(self.stub+ '.out', 'r' )
            handle.close()
            if pyqchem.utils.checkJobFinished(self.stub + '.out')== True:
                pass

        except:
            subprocess.call(commandString.format(command=self.command, stub=self.stub, coresToRun=self.ppn, timeToRun=self.timing).split(),0)

    def checkJobStarted(self):
        while True:
            try:
                handle = open(self.stub + '.out', 'r' )
                handle.close()
                break
            except:
                time.sleep(30)
                continue

    def checkJobFinished(self):

        counter = 0
        while pyqchem.utils.checkJobFinished(self.stub + '.out')== False:
            time.sleep(30) #sleep for 5 minutes
            counter = counter + 30
            if counter >= self.timing * 3600:
                raise RuntimeError('QChem job: %s,did not finish in allotted time' %self.stub)

    def shiftGivenAtoms(self, atomList, vector):
        """
        shift positions of some atoms (indices given in list) by some vector
        """
        for atom in atomList:
            self.positions[atom,:]+= vector
            
        

class QChemInputParser(QChemParser):
    """
    A parser to parse qchem input files.
    """
    def __init__(self, file_name):
        QChemParser.__init__(self, file_name)

class QChemOutputParser(QChemParser):
    """
    A parser to parse qchem output files.
    """
    def __init__(self, file_name):
        QChemParser.__init__(self, file_name)
        self.outputReset()

    def outputReset(self):
        self.frequencies = []
        self.forceConsts = []
        self.irAct = []
        self.ramanAct = []
        self.irIntensities = []
        self.normalModes = []
        self.redMasses = []
        self.optPositions = None
        self.optPositionsPerCycle = None
        self.hessian = []
        self.dipoleDeriv = []
        self.dipoleMoment = []
        self.scfEnergies = []
        self.gradients = []
        self.gradient = []
    def get_energy(self, string_to_search = None):
        """
        for to extract energies from output
        param string_to_search: special string used to find energies in an output file
        type string_to_search: string
        """
        if not string_to_search:
            ex_var, cor_var = self.check_method_type   # set exchange and correlation variable type
            string_to_search = self._set_energy_keystring

        for line in self.lines:
            if string_to_search in line:
                energy = pyqchem.utils.get_numbers(line)[-1]
                return energy
                break
    def cclibGetEnergy(self):
        # more robust than the above get_energy function

        obj = cclib.parser.ccopen(self.file_name)
        parseObj = obj.parse()
        #ensures energy is in hartrees from electron volts
        self.scfEnergies = parseObj.scfenergies*0.0367493

    def cclibGetGradients(self):
        obj = cclib.parser.ccopen(self.file_name)
        parseObj = obj.parse()
        self.gradients = parseObj.grads

    def getEnergyRIMP2Manual(self):
        index = -1

        index = self.lastInstance('total energy', self.lines)

        if index == -1:
            raise ValueError
            print "cannot find total energy in QChem output file"

        else:
            line = self.lines[index]
            
        tokens = line.strip().split()
        
        self.energy = float(tokens[4])
        
    def getGradientRIMP2Manual(self):
        index = -1

        index = self.lastInstance('Full Analytical Gradient', self.lines)

        if index == -1:
            raise ValueError
            print "cannot find gradient in QChem output file"

        else:
            self.gradient= np.zeros( 3* self.size)
            
        lines = self.lines[index +1:]
        print lines
        #number of atoms so far 
        counter = 0
        for i, line in enumerate(lines):
            if i%4 == 0:
                continue
            else:
                index = (i-1)%4 #helps figure out if x,y or z component
                tokens = line.strip().split()
                tokLen = len(tokens)

                for j in range(0,tokLen-1):
                    self.gradient[(counter +j)*3 +index ] =float(tokens[j +1])
                if index==2 and j == tokLen-2:
                    counter +=tokLen-1
                    if counter == self.size:
                        break
                
        
    def extractOptimizedPositions(self):
        """extract optimized geometry positions; returns numpy Array"""


        self.optPositions = np.zeros((self.size,3))

        try:
            self.lines.index('**  OPTIMIZATION CONVERGED  **')
        except ValueError:
            print 'Optimization incomplete'

        regex = r'Optimization Cycle:\s+\d+'

        index = self.lastInstance(regex, self.lines)

        linesNew = self.lines[index:]

        regex2 = r'^\d*\s*\w+\s+[+-]?\d+\.\d+\w?[+-]?\d*\s+[+-]?\d+\.\d+\w?[+-]?\d*\w?[+-]?\d*\s+[+-]?\d+\.\d+\w?[+-]?\d*$'

        counter = 0
        for line in linesNew:
            if (re.match(regex2, line)) is not None:
                if counter < self.size:
                    tokens = line.strip().split()
                    self.optPositions[counter,0] = float(tokens[2])
                    self.optPositions[counter,1] = float(tokens[3])
                    self.optPositions[counter,2] = float(tokens[4])
                    counter += 1
                else:
                    break
        return None


    def optimizedGeometry(self, xyzFile):
        """
        determines the last likely optimized geometry in a geometry calculation,
        and outputs xyz file
        """

        try:
            self.lines.index('**  OPTIMIZATION CONVERGED  **')
        except ValueError:
            print 'Optimization incomplete'

        regex = r'Optimization Cycle:\s+\d+'

        index = self.lastInstance(regex, self.lines)

        linesNew = self.lines[index:]

        s = ''
        regex3 = r'^\d*\s*\w+\s+[+-]?\d+\.\d+\w?[+-]?\d*\s+[+-]?\d+\.\d+\w?[+-]?\d*\w?[+-]?\d*\s+[+-]?\d+\.\d+\w?[+-]?\d*$'
#        regex3 = r'^\d+\s+\w+\s+[+-]?\d+\.\d+\s+[+-]?\d+\.\d+\s+[+-]?\d+\.\d+$'
        counter = 0
        for line in linesNew:
            if (re.match(regex3, line) is not None):
                counter+=1
                if counter > self.size:
                    break
                else:
                    tokens = line.strip().split()
                    length = tokens[0]
                    s = s + tokens[1]+' '+tokens[2]+' '+tokens[3]+' '+tokens[4]+'\n'
        snew = length + '\n\n'
        snew = snew +s

        handle= open(xyzFile,'w')
        handle.write(snew)
        handle.close()

    def getGeomsOpt(self):
        """get geometries from optimization"""
        try:
            self.lines.index('**  OPTIMIZATION CONVERGED  **')
        except ValueError:
            print 'Optimization incomplete'

        regex = r'Optimization Cycle:\s+\d+'
        regex2 = r'Energy is\s+\-\d+\.\d+'
        regex3 = r'^\d*\s*\w+\s+[+-]?\d+\.\d+\w?[+-]?\d*\s+[+-]?\d+\.\d+\w?[+-]?\d*\w?[+-]?\d*\s+[+-]?\d+\.\d+\w?[+-]?\d*$'
        indexBeginList = []
        indexEndList   = []

        self.optPositionsPerCycle = []

        for i, line in enumerate(self.lines):
            if (re.match(regex,line) is not None):
                indexBeginList.append(i)
            elif (re.match(regex2,line) is not None):
                indexEndList.append(i)
        print indexBeginList
        print indexEndList
        for i in range(len(indexBeginList)):

            #make positions array to fill out self.optPositionsPerCycle
            positions = np.zeros((self.size, 3))
            counter = 0

            newLines = self.lines[indexBeginList[i]:indexEndList[i]]
            for line in newLines:
                if (re.match(regex3, line) is not None):
                    tokens = line.strip().split()
                    positions[counter,:] = np.array([float(tokens[2]), float(tokens[3]), float(tokens[4])])
                    counter +=1

            self.optPositionsPerCycle.append(positions)

    def makeOptAnimation(self, xyzFile):

        if self.optPositionsPerCycle == None:
            self.getGeomsOpt()

        s = ''

        for i in range(len(self.optPositionsPerCycle)):
            s += str(self.size) + '\n\n'

            for j in range(self.size):
                s += "%s  %10.9f  %10.9f  %10.9f\n" %(self.atoms[j], self.optPositionsPerCycle[i][j,0], self.optPositionsPerCycle[i][j,1], self.optPositionsPerCycle[i][j,2])


        handle = open(xyzFile,'w')
        handle.write(s)
        handle.close()
#        indexList = []
#        index = -1
#        for i, line in enumerate(lines):
#            if (re.search(regex,line) is not None):
#                index = i
#                indexList.append(index)
#        if index == -1:
#            raise IndexError('regular expression not found')
#        else:
#            regex2 = r'^\d*\s*\w+\s+[+-]?\d+\.\d+\w?[+-]?\d*\s+[+-]?\d+\.\d+\w?[+-]?\d*\w?[+-]?\d*\s+[+-]?\d+\.\d+\w?[+-]?\d*$'
#            for index in indexList:
#                linesNew = self.lines[index:]


    def _set_output_keywords(self):
        pass

    def check_method_type(self):
        for key, value in self.rem.iteritems():
            if key == 'exchange':
                exchange_variable = value
            elif key == 'correlation':
                correlation_variable = value
        return exchange_variable, correlation_variable

    def vibrationalAnalysis(self):

        self.frequencies = []

        index = -1
        for i, line in enumerate(self.lines):
            if "VIBRATIONAL ANALYSIS"  in line:
                index = i
                break
        if index == -1:
            raise ValueError
            print "QChem output file is not a frequecy calculation"

        for line in self.lines[index:]:
            if "Frequency:" in line:
                tokens = line.strip().split()
                for i in range(1,len(tokens)):
                    self.frequencies.append(float(tokens[i]))

            elif "Force Cnst:" in line:
                tokens = line.strip().split()
                for i in range(2, len(tokens)):
                    self.forceConsts.append(float(tokens[i]))

            elif "Red. Mass:" in line:
                tokens = line.strip().split()
                for i in range(2, len(tokens)):
                    self.redMasses.append(float(tokens[i]))

            elif "IR Active:" in line:
                tokens = line.strip().split()
                for i in range(2, len(tokens)):
                    self.irAct.append(str(tokens[i]))

            elif "IR Intens:" in line:
                tokens = line.strip().split()
                for i in range(2, len(tokens)):
                    self.irIntensities.append(float(tokens[i]))

            elif "Raman Active:" in line:
                tokens = line.strip().split()
                for i in range(2, len(tokens)):
                    self.ramanAct.append(str(tokens[i]))

        # the normal modes will be stored in column vectors
        self.normalModes = np.zeros((3*self.size, len(self.frequencies)))

        counterColumns = 0
        for i, line in enumerate(self.lines[index:]):
            if "X      Y      Z" in line:
                counterRows = 0
                for j, line in enumerate(self.lines[index +i + 1:index + i + 1 +self.size]):
                    tokens = line.strip().split()
                    for k in range(3):
                        try:
                            self.normalModes[counterRows,counterColumns+k] = float(tokens[k *3 +1])
                            self.normalModes[counterRows + 1,counterColumns+k] = float(tokens[k*3 +2])
                            self.normalModes[counterRows + 2, counterColumns+k] = float(tokens[k*3 +3])
                        except:
                            break
                    counterRows +=3

                counterColumns += 3

    def getHessian(self):

        self.hessian = []

        dimension = self.size * 3

        index = -1

        index = self.lastInstance('Hessian',self.lines)


        if index == -1:
            raise ValueError
            print "QChem output file is not a frequecy calculation"

        else:
            self.hessian = np.zeros((dimension, dimension))


        rowIndex = 0
        colIndex = 0
        counter = 0
        colNum = len(self.lines[index+2].strip().split())-1



        for line in self.lines[index+2:]:
            tokens = line.strip().split()
            if rowIndex > dimension-1:
                counter +=1
                rowIndex = 0
                colIndex = colNum* counter
#                print colIndex
                continue
            if colIndex >=dimension:
                break
            try:
                for i in range(1, (rowIndex +2)-(colNum*counter)):
#                    print "here"
                    self.hessian[rowIndex,colIndex]=float(tokens[i])
                    self.hessian[colIndex,rowIndex]=float(tokens[i])
                    colIndex += 1
            except:
                if colIndex >= dimension:
                    break
                else:
                    pass
            colIndex = colNum* counter
            rowIndex +=1


    def writeHessianFile(self, fileName):
        handle = open(fileName, 'w')
        s = ''
        s+='Hessian \n'
        for i in range(self.size*3):
            for j in range(i+1):
                s+= '%10.7f ' %(self.hessian[i,j])
            s+='\n'

        handle.write(s)
        handle.close()
        
    def getDipoleDeriv(self):


        self.dipoleDeriv = []

        dimension = self.size * 3

        index = -1

        index = self.lastInstance('dipole drv.', self.lines)

        if index == -1:
            raise ValueError
            print "QChem output file is not a frequecy calculation"

        else:
            self.dipoleDeriv = np.zeros((dimension, 3))

        for i, line in enumerate(self.lines[index+2:index+2+ dimension]):
            tokens = line.strip().split()
            self.dipoleDeriv[i,0] = tokens[1]
            self.dipoleDeriv[i,1] = tokens[2]
            self.dipoleDeriv[i,2] = tokens[3]

    def extractDipoleMoment(self):
        
        index = -1

        index = self.lastInstance('Dipole Moment', self.lines)

        if index == -1:
            raise ValueError
            print "cannot find dipole moment in QChem output file"

        else:
            self.dipoleMoment= np.zeros( 3)
            
        line = self.lines[index +1: index + 2][0]
        
        
        tokens = line.strip().split()
        
        self.dipoleMoment[0]= tokens[1]
        self.dipoleMoment[1]= tokens[3]
        self.dipoleMoment[2]= tokens[5]

    def writeDipoleDerivFile(self, fileName):
        handle = open(fileName, 'w')
        s = ''
        s+='Dipole Derivative \n'
        for i in range(self.size*3):
            s += '%10.8f %10.8f %10.8f\n' % (self.dipoleDeriv[i, 0], self.dipoleDeriv[i, 1], self.dipoleDeriv[i, 2])

        handle.write(s)
        handle.close()


class xyzParser:
    """
    A class used to parse xyz files
    """
    def __init__(self, file_name, atoms=None, positions=None):
        if atoms!=None and positions !=None:
            self.noFileLoad(file_name, atoms, positions)
        else:
            self.load(file_name)

    def load(self, file_name):
        self.file_name = file_name
        self.reset()
        self.parseFile()
        self.setMasses()
        self._setCenterOfMass()
        self._set_moment_of_inertia_tensor()
        self._set_rotational_tensor()

        ### implement later ###
        pass

    def noFileLoad(self, file_name, atoms, positions):
        self.file_name = file_name
        self.reset()
        self.atoms = atoms
        self.positions = positions
        self.numAtoms = len(self.atoms)
        self.setMasses()
        self._setCenterOfMass()
        self._set_moment_of_inertia_tensor()
        self._set_rotational_tensor()


    def reset(self):
        self.positions = []
        self.atoms     = []
        self.numAtoms  = 0
        self.lines     = []
        self.masses    = []
        self.momentInertiaTensor = []
        self.centerOfMass = 0.0
        self.comRelPositions = []
        self.massPositions = []
        self.rotationalTensor=[]
        #list of sets of atom indices which correspond to molecules
        self.clusters=[]
        #length of a monomer given xyz file is made up of periodic images of unit cell for single species
        self.monomerSize=0
        #list of sets of atom indices which correspond to full monomers
        self.monomers=[]
        # list of geometric centers as numpy arrays for monomers
        self.monomerGCs = []

    def parseFile(self):
        handle = open(self.file_name, 'r')
        line = handle.readline()
        while line:
            line = line.strip()
            if line:
                self.lines.append(line)
            line = handle.readline()

        regex  = r'^\s*\d+\s*$'
        regex2 = r'^\s*\w+\s+[+-]?\d+\.\d+\w?[+-]?\d*\s+[+-]?\d+\.\d+\w?[+-]?\d*\w?[+-]?\d*\s+[+-]?\d+\.\d+\w?[+-]?\d*$'

        counter = 0
        for line in self.lines:
            if (re.match(regex,line) is not None):
                counter = counter +1
                if counter>1:
                    raise RuntimeError('there is more than one line corresponding to number of atoms in xyz file')
                self.numAtoms =int(line.strip())

        positions = []
        atoms     = []
        for line in self.lines:
            if (re.match(regex2, line) is not None):
                tokens = line.strip().split()
                atoms.append(tokens[0])
                floats = []
                for token in tokens[1:]:
                    floats.append(float(token))
                positions.append(floats)

        self.atoms = np.array(atoms, dtype='|S16')
        self.positions = np.array(positions)
        if self.numAtoms != self.atoms.size:
            raise RuntimeError('number of atoms promised does not equal number received')

    def setMasses(self):
        # set the masses for the system
        self.masses = []
        for key in self.atoms:
            try:
                self.masses.append(massDict[key.lower()])
            except:
                self.masses.append(massDict[atomNumDict[key]])
    def _set_moment_of_inertia_tensor(self):

        self.comRelPositions = np.zeros(np.shape(self.positions))

        for i in range(self.numAtoms):
            self.comRelPositions[i,:]= self.positions[i,:] - self.centerOfMass


        self.momentInertiaTensor = np.zeros((3,3))

        #first the diagonal
        for k in range(self.numAtoms):
            self.momentInertiaTensor[0,0] += self.masses[k]*(self.comRelPositions[k,1]**2 +self.comRelPositions[k,2]**2)
            self.momentInertiaTensor[1,1] += self.masses[k]*(self.comRelPositions[k,0]**2 +self.comRelPositions[k,2]**2)
            self.momentInertiaTensor[2,2] += self.masses[k]*(self.comRelPositions[k,0]**2 +self.comRelPositions[k,1]**2)

        #now ze off-diagonals
        for k in range(self.numAtoms):
            for i in range(3):
                for j in range(i+1,3):
                    self.momentInertiaTensor[i,j]+= -self.masses[k]*self.comRelPositions[k,i]*self.comRelPositions[k,j]
                    self.momentInertiaTensor[j,i]= self.momentInertiaTensor[i,j]


    def _setCenterOfMass(self):
        #set the center of mass for the system
        massTot = sum(self.masses)

        self.massPositions = np.zeros(np.shape(self.positions))
        for i in range(self.numAtoms):

            self.massPositions[i,:] = self.masses[i]*self.positions[i,:]

        self.centerOfMass = np.sum(self.massPositions, axis = 0)/massTot

    def getSubCenterOfMass(self, atomList):
        """
        returns a center of mass for a subset of atoms in a molecule.  The atoms indexes are given in atomList
        """
        subMassTot= 0.0
        subCOM= np.zeros(3)
        for index in atomList:
            subMassTot += self.masses[index]
            subCOM += self.masses[index]*self.positions[index,:]

        subCOM = subCOM/subMassTot

        return subCOM

    def getSubGeomCenter(self, atomList):
        """ find the geometric center for a subset of atoms in the system"""

        totAtoms = len(atomList)
        subGC = np.zeros(3)
        for index in atomList:
            subGC += self.positions[index,:]

        subGC = subGC/float(totAtoms)
        return subGC

    def removeDuplicateAtoms(self):

#        dupList = []
#
#        for i in range(self.numAtoms):
#            dupList.append((self.atoms[i],self.positions[i,:]))
#
#        dupList = list(set(dupList))
#
#        self.reset()
#        self.numAtoms = len(dupList)

        indexes = []
        for i in range(self.numAtoms):
            for j in range(i+1, self.numAtoms):
                if list(self.positions[i,:]) == list(self.positions[j,:]):
                    indexes.append(j)

        indexes = list(set(indexes))
        indexes.sort(reverse= True)

        for index in indexes:
            self.atoms = np.delete(self.atoms, index)
            self.positions = np.delete(self.positions, index, axis=0)

        self.numAtoms = len(self.atoms)


        self.rewrite_xyz()
        self.reset()
        self.parseFile()

    def _set_rotational_tensor(self):

        self.comRelPositions = np.zeros(np.shape(self.positions))

        for i in range(self.numAtoms):
            self.comRelPositions[i,:]= self.positions[i,:] - self.centerOfMass

        self.rotationalTensor = np.zeros((3,3))

                #first the diagonal
        for k in range(self.numAtoms):
            self.rotationalTensor[0,0] += (self.comRelPositions[k,1]**2 +self.comRelPositions[k,2]**2)
            self.rotationalTensor[1,1] += (self.comRelPositions[k,0]**2 +self.comRelPositions[k,2]**2)
            self.rotationalTensor[2,2] += (self.comRelPositions[k,0]**2 +self.comRelPositions[k,1]**2)

        #now ze off-diagonals
        for k in range(self.numAtoms):
            for i in range(3):
                for j in range(i+1,3):
                    self.rotationalTensor[i,j]+= -self.comRelPositions[k,i]*self.comRelPositions[k,j]
                    self.rotationalTensor[j,i]= self.rotationalTensor[i,j]
    def setAtoms(self, atomsArray):
        """
        sets new atoms for the system
        """
        self.atoms = atomsArray
    def setPositions(self, xyzArray):
        """
        sets new positions for the system
        each row is of course the x, y, and z coordinate
        """

        self.positions = xyzArray
        self._setCenterOfMass()
        self._set_moment_of_inertia_tensor()
        self._set_rotational_tensor()


    def interatomic_distance_vector(self, atoms_index):
        first_atom = self.positions
        first_atom = self.positions[atoms_index[0]]
        second_atom = self.positions[atoms_index[1]]

        difference_vector = first_atom - second_atom

        return difference_vector

    def interMonomerGeomCenDistVec(self, atomList1, atomList2):
        """finds the distance vector between the geometric centers of two subsets of the system"""

        gc1 = self.getSubGeomCenter(atomList1)
        gc2 = self.getSubGeomCenter(atomList2)

        return gc2-gc1

    def rewrite_xyz(self):
        handle = open(self.file_name, 'w')
        handle.write(str(self.numAtoms) + '\n\n')
        s = ''
        for i in range(self.numAtoms):
            x, y, z = self.positions[i]
            s += '%-3s %10.10f %10.10f %10.10f\n' % (self.atoms[i], x, y, z)
        handle.write(s)

    def saveXyz(self, newFile):
        handle = open(newFile, 'w')
        handle.write(str(self.numAtoms) + '\n\n')
        s = ''
        for i in range(self.numAtoms):
            x, y, z = self.positions[i]
            s += '%-3s %10.10f %10.10f %10.10f\n' % (self.atoms[i], x, y, z)
        handle.write(s)

    def fractionalToXYZ(self, newFile, a=1.0,b=1.0, c=1.0, alpha=90.0,beta=90.0,gamma=90.0):
        """
        if coordinates are fractional this converts them to xyz coordinates by finding
        the cell vectors.  a,b, c should be in angstroms
        """
        #convert angles from degrees to radians

        alphaRad=alpha*np.pi/180.00
        betaRad =beta*np.pi/180.00
        gammaRad=gamma*np.pi/180.00


        aVec=np.array([1.0,0.0,0.0])
        bVec=np.array([np.cos(gammaRad), np.sin(gammaRad), 0.0])
        #finding cVec is a litle more complicated

        pVec = np.array([np.cos(betaRad), 0.0, np.sin(betaRad)])
        x=np.arcsin((pVec[0]*bVec[0]-np.cos(alphaRad))/(pVec[2]*bVec[1]))

        cVec = np.array([pVec[0], -np.sin(x)*pVec[2], np.cos(x)*pVec[2]])

        #test
        print np.arccos(np.dot(bVec,cVec))*180.0/np.pi
        print np.arccos(np.dot(aVec,cVec))*180.0/np.pi
        print np.arccos(np.dot(aVec,bVec))*180.0/np.pi
        print aVec
        print bVec
        print cVec

        TransformMat= np.zeros((3,3))
        TransformMat[:,0]=aVec*a
        TransformMat[:,1]=bVec*b
        TransformMat[:,2]=cVec*c

        print self.positions
        for i in range(self.numAtoms):
            self.positions[i,:]=np.dot(TransformMat, self.positions[i,:])

        print self.positions

        self.saveXyz(newFile)

    def createSuperCell(self, saveFile, aVec, bVec, cVec, aTimes, bTimes, cTimes):
        """
        create and save a super cell from current system
        saveFile: file to save system as an xyz file to; do not include .xyz extension, string
        avec: one of the vectors of the triclinic box, numpy array
        bvec: second vector, numpy array
        cvec: third, numpy array
        note:vectors should be given in cartesian basis
        aTimes:  how many times to repeat the cell in the a direction, int
        bTimes:  how many times to repeat the cell in the b direction, int
        cTimes:  how many times to repeat the cell in the c direction, int
        """
        #copy old info
        oldPositions = self.positions
        oldAtoms = self.atoms

        # list of indices to loop over to create cell
        indA = range(aTimes)
        indB = range(bTimes)
        indC = range(cTimes)

        # expected new number of atoms in system
        newSize = self.numAtoms*aTimes*bTimes*cTimes

        # new positions matrix
        newPositions = np.zeros((newSize, 3))

        # list for new atom types
        newAtoms = []

        # populate newPositions with original atoms and images
        counter = 0
        for i in indA:
            for j in indB:
                for k in indC:
                    for l in range(self.numAtoms):
                        newPositions[counter, :] = self.positions[l,:]+ i *aVec + j*bVec + k *cVec
                        newAtoms.append(self.atoms[l])
                        counter += 1

        self.setPositions(newPositions)
        self.numAtoms = newSize
        self.atoms = newAtoms
        self.saveXyz(saveFile)

        # reset and reload current instance of xyzParser
        self.reset()
        # note this won't work if you did a no file load
        self.noFileLoad(self.file_name, oldAtoms, oldPositions)

    def findClusters(self):

        #distance matrix
        dm = cdist(self.positions,self.positions)

        # bond radius matrix
        br = np.zeros((self.numAtoms,self.numAtoms))

        for i, key1 in enumerate(self.atoms):
            for j, key2 in enumerate(self.atoms):
                try:
                    br[i,j] = bondRadii[key1.lower()]+ bondRadii[key2.lower()]
                except:
                    br[i,j] = bondRadii[atomNumDict[key1]]+ bondRadii[atomNumDict[key2]]

        #corrected distance matrix
        cdm = dm/br
        #bond matrix
        bm = np.array(cdm<1.5, dtype = int)
        #cluster list of lists, each list contains atoms of a single cluster
        clusterList = []
        for i in range(self.numAtoms):
            newList = []
            for j in range(self.numAtoms):
                if bm[i,j]==1:
                    newList.append(j)
            clusterList.append(set(newList))

        def findNewSet(inSet, noSkipSet):
            newSet = inSet
            for value in list(noSkipSet):
                newSet = newSet | clusterList[value]
            return newSet

        #find intersections and unions of clusters
        newList = []
        skipSet = set([])
        for i in range(len(clusterList)):

            if i in skipSet:
                continue
            else:
                notSame = True
                newSet = clusterList[i]
                while notSame == True:
                    newestSet = findNewSet(newSet, newSet.difference(skipSet))
                    if newestSet == newSet:
                        notSame = False
                    skipSet = skipSet | newSet
                    newSet = newestSet

                newList.append(newSet)

        self.clusters = newList

    def findMonomerClusters(self):
        """
        for crystals of single monomer composition only.  Identifies clusters of monomers given
        some repetition or single unit cell
        """

        if not self.clusters:
            self.findClusters()

                # identify length of monomers
        for curSet in self.clusters:
            if len(curSet) > self.monomerSize:
                self.monomerSize = len(curSet)
            else:
                continue

        monomers = list(np.copy(self.clusters))
        numClus = len(self.clusters)
        counter = 0
        index = 0
        while counter != numClus:
            if len(monomers[index]) < self.monomerSize:
                del monomers[index]
                counter += 1
            else:
                index += 1
                counter +=1

        self.monomers =monomers

    def writeMonomerFile(self, monomerFileName):
        """write file containing monomer index and atom indices and species of each monomer"""


        if not self.monomers:
            self.findMonomerClusters()

        handle = open(monomerFileName, 'w')

        handle.write('Monomer Indices:    Atom Indices:\n'    )
        s = ''
        for i, monomer in enumerate(self.monomers):
            s+= str(i)
            monList = list(monomer)
            for j in range(self.monomerSize):
                s += '                  %-3s %10.8f\n' %(self.atoms[monList[j]], monList[j])
            s+='\n'
        handle.write(s)
        handle.close()

    def findGeomCentersMonomers(self):
        if not self.monomers:
            self.findMonomerClusters()

        for i in range(len(self.monomers)):
            self.monomerGCs.append(self.getSubGeomCenter(list(self.monomers[i])))

    def redefineAtomsForUnitCell(self, avec, bvec, cvec):

        """this function will take unit cell parameters and current positions and make it so that the full molecules are
        are now the unit cell instead of partial molecules
        """
        ### first we make a super cell.  we extend the unit cell to add a layer in each direction an the corners as well
        newSize = self.numAtoms*8
        newPositions = np.zeros((newSize, 3))
        newAtoms = []
        index = [0,1]
        counter = 0
        for i in index:
            for j in index:
                for k in index:
                    for l in range(self.numAtoms):
                        newPositions[counter, :] = self.positions[l,:]+ i *avec + j*bvec + k *cvec
                        newAtoms.append(self.atoms[l])
                        counter += 1

        self.setPositions(newPositions)
        self.numAtoms = newSize
        self.atoms = newAtoms
        self.saveXyz('bigCell.xyz')
        #distance matrix
        dm = cdist(newPositions,newPositions)

        # bond radius matrix
        br = np.zeros((newSize,newSize))

        for i, key1 in enumerate(newAtoms):
            for j, key2 in enumerate(newAtoms):
                br[i,j] = bondRadii[key1.lower()]+ bondRadii[key2.lower()]


        #corrected distance matrix
        cdm = dm/br
        #bond matrix
        bm = np.array(cdm<1.2, dtype = int)
        #cluster list of lists, each list contains atoms of a single cluster
        clusterList = []
        for i in range(newSize):
            newList = []
            for j in range(newSize):
                if bm[i,j]==1:
                    newList.append(j)
            clusterList.append(set(newList))

        def findNewSet(inSet, noSkipSet):
            newSet = inSet
            for value in list(noSkipSet):
                newSet = newSet | clusterList[value]
            return newSet

        #find intersections and unions of clusters
        newList = []
        skipSet = set([])
        for i in range(len(clusterList)):

            if i in skipSet:
                continue
            else:
                notSame = True
                newSet = clusterList[i]
                while notSame == True:
                    newestSet = findNewSet(newSet, newSet.difference(skipSet))
                    if newestSet == newSet:
                        notSame = False
                    skipSet = skipSet | newSet
                    newSet = newestSet

                newList.append(newSet)

        # identify length of monomers
        monSize = 0
        for curSet in newList:
            if len(curSet) > monSize:
                monSize = len(curSet)
            else:
                continue

        numClus = len(newList)
        counter = 0
        index = 0
        while counter != numClus:
            if len(newList[index]) < monSize:
                del newList[index]
                counter += 1
            else:
                index += 1
                counter +=1

        print len(newList)
        print newList



class MopacParser:
    """ a class to parse mopac files"""
    def __init__(self,fileName):
        self.reset()
        self.stub, self.ext = os.path.splitext(fileName)
        self.fileName = fileName
        self.parseMopacFile()
        self.parseMolecule()
    def reset(self):
        self.stub = ''
        self.ext = ''
        self.positions = []
        self.atoms = []
        self.fileName = ''
        self.abcVecs = []
        self.lines= []
        self.numAtoms = 0

    def parseMolecule(self):
        posList = []
        regex =  r'^\s*\w+(\s+[+-]?\d+\.\d+\s?[+-]?\d*)+$'
        self.abcVecs = np.zeros((3,3))
        counter = 0
        for line in self.lines:
            if (re.match(regex, line) is not None):
                tokens = line.strip().split()

                if tokens[0].lower() == 'tv':
                    print counter
                    self.abcVecs[counter,0]=float(tokens[1])
                    self.abcVecs[counter,1]=float(tokens[3])
                    self.abcVecs[counter,2]=float(tokens[5])
                    counter += 1
                    continue
                else:
                    pos = np.zeros(3)
                    self.atoms.append(tokens[0])
                    pos[0]= float(tokens[1])
                    pos[1]= float(tokens[3])
                    pos[2]= float(tokens[5])
                    posList.append(pos)

        self.positions = np.reshape(posList,(len(posList), 3))
        self.numAtoms = len(self.atoms)



    def parseMopacFile(self):

        handle = open(self.fileName, 'r')

        line = handle.readline()
        while line:
            line = line.strip()
            if line:
                self.lines.append(line)
            line = handle.readline()

        handle.close()


    def writeXyzFile(self, xyzFile):
        handle = open(xyzFile, 'w')
        handle.write(str(self.numAtoms) + '\n\n')
        s = ''
        for i in range(self.numAtoms):
            x, y, z = self.positions[i]
            s += '%-3s %10.8f %10.8f %10.8f\n' % (self.atoms[i], x, y, z)
        handle.write(s)
        handle.close()

    def writeQEspressoInFile(self, fileName, calculation = 'vc-relax', ibrav = '0', ecutwfc = 25.0, ecutrho = 200.0):
        atomTypes = list(set(self.atoms))

        handle = open(fileName, 'w')
        handle.write('&control\n')
        handle.write('     calculation = ' + '\'' + calculation + '\'' + '\n')
        handle.write('     outdir = ' + '\'' './' '\'\n')
        handle.write('/\n')
        handle.write('&system\n')
        handle.write('     ibrav = 0\n')
        handle.write('     nat = ' + str(self.numAtoms) + '\n')
        handle.write('     ntyp = ' + str(len(atomTypes))+ '\n')
        handle.write('     ecutwfc = ' + str(ecutwfc)+ '\n' )
        handle.write('     ecutrho = ' + str(ecutrho)+ '\n' )
        handle.write('/\n')
        handle.write('&electrons\n')
        handle.write('     conv_thr = 1e-12\n')
        handle.write('     mixing_beta = 0.3\n')
        handle.write('     tqr = .true.\n')
        handle.write('/\n')
        handle.write('&ions\n')
        handle.write('     ion_dynamics = ' + '\''+ 'bfgs'+ '\'\n')
        handle.write('/\n')
        handle.write('&cell\n')
        handle.write('     cell_dynamics = ' + '\'' + 'bfgs' + '\'\n')
        handle.write('/\n')
        handle.write('ATOMIC_SPECIES\n')
        for species in atomTypes:
            handle.write(species +' ' + str(massDict[species.lower()]) + ' '+ species.lower().capitalize()+ '.pz-van_ak.UPF\n')
        handle.write('ATOMIC_POSITIONS (angstrom)\n')
        s = ''
        for i in range(self.numAtoms):
            x, y, z = self.positions[i]
            s += '%-3s %10.8f %10.8f %10.8f\n' % (self.atoms[i], x, y, z)
        handle.write(s)
        handle.write('CELL_PARAMETERS (angstrom)\n')
        s = ''
        for i in range(3):
            x,y,z = self.abcVecs[i]
            s += '%10.8f %10.8f %10.8f\n' % (x, y, z)
        handle.write(s)
        handle.close()





class MopacOutputParser:
    def __init__(self,fileName):
        self.reset()
        self.stub, self.ext = os.path.splitext(fileName)
        self.fileName = fileName
        self.parseMopacFile()
        try:
            self.extractFinalGeometry()
        except:
            pass
        try:
            self.extractFinalUnitCellVecs()
        except:
            pass
    def reset(self):
        self.stub = ''
        self.ext = ''
        self.initialPositions = []
        self.finalPositions = []
        self.atoms = []
        self.fileName = ''
        self.abcVecs = []
        self.finalAbcVecs = []
        self.lines= []
        self.numAtoms = 0
        self.jobFinishedStatus = None

    def parseMopacFile(self):

        handle = open(self.fileName, 'r')

        line = handle.readline()
        while line:
            line = line.strip()
            if line:
                self.lines.append(line)
            line = handle.readline()

        handle.close()

    def extractFinalGeometry(self):

        indices = [index for index, value in enumerate(self.lines) if value == 'CARTESIAN COORDINATES']

        newLines = self.lines[indices[-1]+1:]

        regex =  r'^\d*\s*\w+(\s+[+-]?\d+\.\d+\s*)+$'
        posList = []

        while re.match(regex,newLines[0]) is None:
            newLines = newLines[1:]

        for line in newLines:
            if (re.match(regex, line) is not None):
                tokens = line.strip().split()
                pos = np.zeros(3)
                self.atoms.append(tokens[1])
                pos[0]= float(tokens[2])
                pos[1]= float(tokens[3])
                pos[2]= float(tokens[4])
                posList.append(pos)
            else:
                break

        self.finalPositions = np.reshape(posList,(len(posList), 3))
        self.numAtoms = len(self.atoms)

    def extractFinalUnitCellVecs(self):
        self.finalAbcVecs = np.zeros((3,3))

        indices = [index for index,value in enumerate(self.lines) if 'tv' in value.lower()]

#        if len(indices) ==3:
#            indices = [index for index,value in enumerate(self.lines) if 't1' in value.lower()]
#            newLines = self.lines[indices[-1]:indices[-1]+3]
#            if 't1' in line.lower():
#
#        else:
        newLines = self.lines[indices[-1] -2:indices[-1]+1]
        counter = 0
        for line in newLines:
            if 'tv' in line.lower():
                tokens = line.strip().split()
                self.finalAbcVecs[counter, 0] = float(tokens[2])
                self.finalAbcVecs[counter, 1] = float(tokens[4])
                self.finalAbcVecs[counter, 2] = float(tokens[6])
                counter +=1

        # this is gonna be shady


    def saveOptGeomXyz(self, fileName):

        if self.finalPositions == []:
            self.extractFinalGeometry()
        handle = open(fileName, 'w')

        handle.write(str(self.numAtoms))
        handle.write('\n\n')
        s = ''
        for i in range(self.numAtoms):
            x, y, z = self.finalPositions[i]
            s += '%-3s %10.8f %10.8f %10.8f\n' % (self.atoms[i], x, y, z)
        handle.write(s)
        handle.close()

class EspressoParser:
    def __init__(self, inFile):
        self.reset()
        self.name = inFile
        self.stub, self.ext = os.path.splitext(inFile)
        self.getLines()
        self.parseControl()
        self.parseSystem()
        self.parseElectrons()
        self.parseIons()
        self.parseCell()
        self.parseSpecies()
        self.parsePositions()
        self.parseCellParameters()
        self.parseKPoints()
        self.geometricCenter()
        self.buildRotationalVectors()
        
    def reset(self):
        self.name = ''
        self.stub = ''
        self.ext = ''
        self.lines = []
        self.control = {}
        self.system = {}
        self.electrons = {}
        self.ions = {}
        self.cell = {}
        self.atoms = []
        self.positions = []
        self.size = 0
        self.cellParams = np.zeros((3,3))
        self.pseudos = {}
        self.kPoints = []
        self.ppn = 0
        self.timing = 0
        self.geomCen = np.zeros(3)
        self.rotTransVectors = []
    def getLines(self):
        handle = open(self.name, 'r')
        line = handle.readline()
        while line:
            line = line.strip()
            if line:
                self.lines.append(line)
            line = handle.readline()

        handle.close()

    def _extract(self, keyword):
        """
        Find lines from keyword until $end.

        :param keyword: espresso input file keyword
        :type keyword: str
        """
        lines = [line.lower() for line in self.lines]
        try:
            start_index = lines.index(keyword)
        except ValueError:
            print 'keyword not found: %s' % keyword
            return []

        for i in range(start_index, len(self.lines)):
            if '/'==self.lines[i]:
                stop_index = i
                break
            elif i == len(self.lines) - 1:
                print 'can not find $end for keyword: %s' % keyword
                sys.exit(-1)

        return self.lines[start_index + 1:stop_index]

    def _toString(self):
        s =''
        s+='&control\n'
        s+=self.sectionString(self.control)
        s+='/\n'
        s+='&system\n'
        s+=self.sectionString(self.system)
        s+='/\n'
        s+='&electrons\n'
        s+=self.sectionString(self.electrons)
        s+='/\n'
        s+='&ions\n'
        s+=self.sectionString(self.ions)
        s+='/\n'
        s+='&cell\n'
        s+=self.sectionString(self.cell)
        s+= '/\n'
        s+='ATOMIC_SPECIES\n'
        s+=self.atomicSpeciesString()
        s+='ATOMIC_POSITIONS (angstrom)\n'
        s+=self.positionsString()
        s+='CELL_PARAMETERS (angstrom)\n'
        s+=self.cellParamString()
        s+='K_POINTS {automatic}\n'
        s+=self.kPointsString()
        return s

    def write(self, fileName):
        handle = open(fileName,'w')
        handle.write(self._toString())
        handle.close()


    def sectionString(self, sectionDict):
        s = ''
        for key in sectionDict:
            s += key + '=' +sectionDict[key] + '\n'
        return s
    def atomicSpeciesString(self):
        s = ''
        for key in self.pseudos:
            s+= key + ' ' + str(massDict[key.lower()]) + ' ' + self.pseudos[key] + '\n'
        return s
    def positionsString(self):
        s = ''
        for i, atom in enumerate(self.atoms):
            x, y, z = self.positions[i]
            s += '%-3s %10.8f %10.8f %10.8f\n' % (self.atoms[i], x, y, z)
        return s
    def cellParamString(self):
        s = ''
        for i in range(3):
            s += '%10.8f %10.8f %10.8f\n' % (tuple(self.cellParams[i,:]))
        return s
    def kPointsString(self):
        s = ''
        for k in self.kPoints:
            s+= str(k) + ' '
        return s
    def parseControl(self):
        lines = self._extract('&control')
        for line in lines:
            line = re.sub('=', ' ', line) # substitue equal sign for space
            tokens = [token.strip() for token in line.strip().split()] # split up lines
            if (len(tokens) == 2):
                key, value = tokens
                self.control[key.lower()] = value.lower() # insert key into dict
    def parseSystem(self):
        lines = self._extract('&system')
        for line in lines:
            line = re.sub('=', ' ', line) # substitue equal sign for space
            tokens = [token.strip() for token in line.strip().split()] # split up lines
            if (len(tokens) == 2):
                key, value = tokens
                self.system[key.lower()] = value.lower() # insert key into dict
    def parseElectrons(self):
        lines = self._extract('&electrons')
        for line in lines:
            line = re.sub('=', ' ', line) # substitue equal sign for space
            tokens = [token.strip() for token in line.strip().split()] # split up lines
            if (len(tokens) == 2):
                key, value = tokens
                self.electrons[key.lower()] = value.lower() # insert key into dict

    def parseIons(self):
        lines = self._extract('&ions')
        for line in lines:
            line = re.sub('=', ' ', line) # substitue equal sign for space
            tokens = [token.strip() for token in line.strip().split()] # split up lines
            if (len(tokens) == 2):
                key, value = tokens
                self.ions[key.lower()] = value.lower() # insert key into dict

    def parseCell(self):
        lines = self._extract('&cell')
        for line in lines:
            line = re.sub('=', ' ', line) # substitue equal sign for space
            tokens = [token.strip() for token in line.strip().split()] # split up lines
            if (len(tokens) == 2):
                key, value = tokens
                self.cell[key.lower()] = value.lower() # insert key into dict
    def parsePositions(self):
        regex =  r'^\s*\w+(\s+[+-]?\d+\.\d+\s?[+-]?\d*)+$'
        lines = [line.lower() for line in self.lines]
        for i, line in enumerate(lines):
            if "atomic_positions" in line:
                lines = self.lines[i+1:]

        posList = []
        for line in lines:
            if (re.match(regex, line) is not None):
                tokens = line.strip().split()
                pos = np.zeros(3)
                self.atoms.append(tokens[0])
                pos[0]= float(tokens[1])
                pos[1]= float(tokens[2])
                pos[2]= float(tokens[3])
                posList.append(pos)
            else:
                break
        self.positions = np.reshape(posList, (len(posList), 3))
        self.size = np.shape(self.positions)[0]
        
    def parseSpecies(self):
        regex = r'^\s*\w\w?\s+\d*\.\d*\s+\S+$'
        lines = [line.lower() for line in self.lines]
        for i, line in enumerate(lines):
            if "atomic_species" in line:
                lines = self.lines[i+1:]
                break
        for line in lines:
            if (re.match(regex, line) is not None):
                tokens = line.strip().split()
                self.pseudos[tokens[0]] = tokens[2]
            else:
                break
    def parseCellParameters(self):
        regex =  r'^(\s*[+-]?\d*\.\d*)+$'
        lines = [line.lower() for line in self.lines]
        for i, line in enumerate(lines):
            if "cell_parameters" in line:
                lines = self.lines[i+1:]
                break
        for i, line in enumerate(lines):
            if (re.match(regex, line) is not None):
                tokens = line.strip().split()
                self.cellParams[i,:] = [float(token) for token in tokens]
            else:
                break
    def parseKPoints(self):
        lines = [line.lower() for line in self.lines]
        for i, line in enumerate(lines):
            if "k_points" in line:
                line = self.lines[i+1]
                break
        tokens = line.strip().split()
        self.kPoints = [int(token) for token in tokens]
    def addField(self, fieldVec):
        self.control['lelfield'] = '.true.'
        self.electrons['efield_cart(1)']= str(fieldVec[0])
        self.electrons['efield_cart(2)']= str(fieldVec[1])
        self.electrons['efield_cart(3)']= str(fieldVec[2])
    def percentCellParamChange(self, percentArray):
        #### percents in array can be positive or neg ######
        self.cellParams += percentArray* self.cellParams

    def changeKPoints(self, kList):
        if len(kList) != 6:
            raise RuntimeError('length of k points list needs to be 6')
        else:
            self.kPoints = kList

    def runJobInBackground(self):
        commandString = r'{command} {stub}'

        try:
            handle = open(self.stub+ '.out', 'r' )
            handle.close()
            if pyqchem.utils.checkJobFinished(self.stub + '.out')== True:
                pass

        except:
            subprocess.call(commandString.format(command='xbackgroundEspressoH2P', stub = self.stub).split(), 0)
            
            
    def submitJob(self, ppn, timing):
        self.ppn=ppn
        self.timing=timing

        commandString = r'{command} {stub} {coresToRun:d} {timeToRun:d}'

        try:
            handle = open(self.stub+ '.out', 'r' )
            handle.close()
            if pyqchem.utils.checkJobFinished(self.stub + '.out')== True:
                pass

        except:
            subprocess.call(commandString.format(command='submitEspressoH2P', stub=self.stub, coresToRun=self.ppn, timeToRun=self.timing).split(),0)

    def checkJobStarted(self):
        while True:
            try:
                handle = open(self.stub + '.out', 'r' )
                handle.close()
                break
            except:
                time.sleep(30)
                continue

    def checkJobFinished(self):

        counter = 0
        while pyqchem.utils.checkJobFinishedEspresso(self.stub + '.out')== False:
            time.sleep(30) #sleep for 5 minutes
            counter = counter + 30
            if counter >= self.timing * 3600:
                raise RuntimeError('Espresso job: %s,did not finish in allotted time' %self.stub)

    def geometricCenter(self):

        for i in range(self.size):
            self.geomCen += self.positions[i, :]
            
        self.geomCen /= self.size
        
    def buildRotationalVectors(self):    
        cellA = np.zeros(shape=[3,3])
        cellB = np.zeros(shape=[3,3])
        cellC = np.zeros(shape=[3,3])
        
        cellA[1, 2] = -self.cellParams[0, 0]
        cellA[2, 1] = self.cellParams[0, 0]
        cellA[0, 2] = self.cellParams[0, 1]
        cellA[2, 0] = -self.cellParams[0, 1]
        cellA[0, 1] = -self.cellParams[0, 2]
        cellA[1, 0] = self.cellParams[0, 2]

        cellB[1, 2] = -self.cellParams[1, 0]
        cellB[2, 1] = self.cellParams[1, 0]
        cellB[0, 2] = self.cellParams[1, 1]
        cellB[2, 0] = -self.cellParams[1, 1]
        cellB[0, 1] = -self.cellParams[1, 2]
        cellB[1, 0] = self.cellParams[1, 2]

        cellC[1, 2] = -self.cellParams[2, 0]
        cellC[2, 1] = self.cellParams[2, 0]
        cellC[0, 2] = self.cellParams[2, 1]
        cellC[2, 0] = -self.cellParams[2, 1]
        cellC[0, 1] = -self.cellParams[2, 2]
        cellC[1, 0] = self.cellParams[2, 2]
        
        relativePositions = self.positions - self.geomCen
        
        S = np.zeros(shape=[3,3])
        for i in range(self.size):
            Ri = np.zeros(shape=[3,3])
            Ri[0, 1] = -relativePositions[i, 2]
            Ri[0, 2] = relativePositions[i, 1]
            Ri[1, 0] = relativePositions[i, 2]
            Ri[1, 2] = -relativePositions[i, 0]
            Ri[2, 0] = -relativePositions[i, 1]
            Ri[2, 1] = relativePositions[i, 0]
            S += np.dot(Ri.transpose(), Ri)
        
        rotCell = np.dot(cellA.transpose(), cellA) + np.dot(cellB.transpose(), cellB) + np.dot(cellC.transpose(), cellC) + S

        eval, evec = np.linalg.eigh(rotCell)

        a = self.cellParams[0, :]
        b = self.cellParams[1, :]
        c = self.cellParams[2, :]

        rotationMatrix = np.zeros((3*self.size + 9, 3))    
        for i in range(3):
            rotationMatrix[0:3, i] = np.cross(a, evec[:, i])
            rotationMatrix[3:6, i] = np.cross(b, evec[:, i])
            rotationMatrix[6:9, i] = np.cross(c, evec[:, i])
            for j in range(self.size):
                rotationMatrix[j*3+9: j*3 + 12, i] = np.cross(relativePositions[j, :], evec[:, i])
        # normalize        
        for i in range(3):
            rotationMatrix[:,i] = rotationMatrix[:,i]/np.linalg.norm(rotationMatrix[:,i])
        
        
        

#        print rotationMatrix
        translationVectors = np.zeros((3*self.size + 9, 3))
        for i in range(self.size):
            for j in range(3):
                translationVectors[3*i+j +9, j] = 1.0
        
        translationVectors /= np.sqrt(self.size)
#        print translationVectors
#        for i in range(3):
#            for j in range(3):
#                print np.dot(rotationMatrix[:,i], translationVectors[:,j])
        
        rotTransMat = np.hstack((translationVectors, rotationMatrix))
        self.rotTransVectors.append(rotTransMat)
        
class EspressoOutputParser:
    def __init__(self, inFile):
        self.reset()
        self.name = inFile
        self.stub, self.ext = os.path.splitext(inFile)
        self.getLines()


    def reset(self):
        self.name = ''
        self.stub = ''
        self.ext = ''
        self.lines = []
        self.atoms = []
        self.positions = []
        self.cellParams = np.zeros((3,3))
        self.finalEnergy = 0.0
        self.finalPositions = []
        self.gradient = []


    def getLines(self):
        handle = open(self.name, 'r')
        line = handle.readline()
        while line:
            line = line.strip()
            if line:
                self.lines.append(line)
            line = handle.readline()

        handle.close()
    def lastInstanceOf(self, keyphrase):
        ### keyphrase should be lowercase ###
        lines = [line.lower() for line in self.lines]
        indicesList = []
        for i, line in enumerate(lines):
            if keyphrase in line:
                indicesList.append(i)

        return indicesList[-1]

    def extractEnergy(self):
        lines = [line.lower() for line in self.lines]
        for i, line in enumerate(lines):
            if "total energy" in line and "!" in line:
                lineNum = i

        tokens = self.lines[lineNum].strip().split()
        self.finalEnergy = float(tokens[4])
        return self.finalEnergy

    def getFinalPositions(self):
        regex = r'^\s*\w+(\s+[+-]?\d+\.\d+\s?[+-]?\d*)+$'
        positions = []
        index = self.lastInstanceOf('atomic_positions')
        lines = self.lines[index+1:]
        newLines = [line.lower() for line in lines]
        for line in newLines:
            if (re.match(regex, line) is not None):
                tokens = line.strip().split()
                self.atoms.append(tokens[0])
                for tok in tokens[1:]:
                    positions.append(float(tok))
            else:
                break
        self.finalPositions = np.reshape(np.array(positions), (len(positions)/3,3))
        print self.finalPositions
        
    def extractGradient(self):
        """ in Rydberg/ang """
        gradient = []
        for i, line in enumerate(self.lines):
            if "Forces acting" in line:
                lineNum = i
                break 
            
        lines = self.lines[lineNum+1:]
        for line in lines:
            tokens = line.strip().split()
            if tokens[0] != 'atom':
                break
            else:
                gradient.append(float(tokens[6]))
                gradient.append(float(tokens[7]))
                gradient.append(float(tokens[8]))
    
        #  convert bohr^-1 to ang^-1
        self.gradient = np.array(gradient) * -1.8897161646320724
        return self.gradient
        
class Trajectory:
    def __init__(self, fileName):
        self.reset()
        self.name = fileName
    def reset(self):
        self.name = None
        self.nAtoms = 0
        self.trajectory = []
        self.lines = []

    def getTrajectory(self):

        regex  = r'^\s*\d+\s*$'
        regex2 = r'^\s*\w+(\s+[+-]?\d+\.\d+\w?[+-]?\d*\s+\d)+$'

        handle = open(self.name, 'r')
        line = handle.readline()
        while line:
            line = line.strip()
            if line:
                self.lines.append(line)
            line = handle.readline()


#class trajectoryParser(self, fileName):
