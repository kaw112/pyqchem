import numpy as np
import pyqchem
import os

        
class MoleculeTransform:
    def __init__(self):
        pass

    def translate(self, positions, vector):
        return positions + vector

    def shift(self, positions, vector, sub_index):
        """
        :param positions: array of atom positions
        :param vector   : vector to shift by
        :param sub_index: indices of atoms to shift
        """
        sub_positions = positions[sub_index]
        sub_positions = self.translate(sub_positions, vector)
        positions[sub_index] = sub_positions
        return positions

    def rotate(self, positions, axis, angle):
        pass

def perform_shifts(input_file, shift_vector, num_files, sub_index, time):
    transform = MoleculeTransform()
    shift_vector = np.asanyarray(shift_vector)

    assert num_files % 2 == 0

    stub, ext = os.path.splitext(input_file)

    pyqchem.utils.submit(stub, time)

    i = 1
    for j in range(-num_files/2, num_files/2 + 1):
        if not j == 0:
            vector = j * shift_vector
            print 'shifting %s by (%6.3f, %6.3f, %6.3f)... (%3d of %3d)' % (
                input_file, vector[0], vector[1], vector[2], i, num_files)

            molecule = pyqchem.QChemInputParser(input_file)
            molecule.positions = transform.shift(
                molecule.positions, vector, sub_index)
            molecule.rem['jobtype'] = 'sp'
            molecule.save('%s%d.n' % (stub, i))

            pyqchem.utils.submit('%s%d' % (stub, i), time)
            i += 1

def perform_shifts_energy_extract(input_file, num_files, atoms_index, string_to_search):
    """
    extracts energies from files made by perform_shifts and place along
    with distance of interest in files
    """
    stub, ext = os.path.splitext(input_file)
    handle = open(stub + '_energy', 'w')
    distances = []
    energies  = []

    molecule = pyqchem.QChemOutputParser(input_file)

    distance = molecule.interatomic_distance_vector(atoms_index)[2]
    energy   = molecule.get_energy(string_to_search)

    distances.append(distance)
    energies.append(energy)

    for i in range(1, num_files+1):
        molecule = pyqchem.QChemOutputParser(stub + str(i) + ext)
        distance = molecule.interatomic_distance_vector(atoms_index)[2]
        energy   = molecule.get_energy(string_to_search)

        distances.append(distance)
        energies.append(energy)

    for distance, energy in zip(distances, energies):
        handle.write("%10.5f %30.16f\n" % (distance, energy))

    handle.close()
