# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 17:49:00 2015

@author: keith
"""
import numpy as np

def iterativePiezoSolver(A, b):
    
    eigval, eigvec = np.linalg.eigh(A)
    
    e1 = eigval[0]
    e2 = eigval[1]
    e3 = eigval[2]
    
    vec1= eigvec[:,0]
    vec2= eigvec[:,1]
    vec3= eigvec[:,2]
    
    c1 = np.dot(b,vec1)
    c2 = np.dot(b, vec2)
    c3 = np.dot(b, vec3)
    
    print c1,c2, c3
    print e1,e2,e3
    
    def f1(C1New):
        return e1+c1/C1New
    def f2(C2New):
        return e2+c2/C2New
    def f3(C3New):
        return e3+c3/C3New
    def C2Func(e):
        return c2/(e-e2)
    def C3Func(e):
        return c3/(e-e3)
    def C1Func(e):
        return c1/(e-e1)
    def Normalize(Ci,Cj,Ck):
        n = np.sqrt(Ci**2+Cj**2+Ck**2)
        return Ci/n, Cj/n, Ck/n
        
    def Iteration(CjFunc, CkFunc, f,  Cinit):
        Ci = Cinit
        
        for i in range(1000):
            e = f(Ci)
            Cj = CjFunc(e)
            Ck = CkFunc(e)
        
            CiNew,CjNew, CkNew = Normalize(Ci, Cj, Ck)
        
            if np.abs(CiNew-Ci)<1e-8 and np.abs(CjNew-Cj)<1e-8 and np.abs(CkNew-Ck)<1e-8:
                print 'Number of steps: %d' %i
                print f(CiNew)
                return CiNew, CjNew, CkNew, e
            else:
                Ci = CiNew

        print "failed"
        print np.sqrt(Ci**2 + Cj**2 + Ck**2)
        return 0
        
    SolutionList = []
    ValueList = []
        
    try:
        C1, C2, C3, e = Iteration(C2Func, C3Func, f1, .7)
        
        SolutionList.append(C1*vec1+C2*vec2+ C3*vec3)
        ValueList.append(e)
    except:
        pass
    
    try:
        C1, C2, C3, e  = Iteration(C2Func, C3Func, f1, -.7)
        SolutionList.append(C1*vec1+C2*vec2+ C3*vec3)
        ValueList.append(e)
    except:
        pass
    
    try:
        C2, C1, C3, e = Iteration(C1Func, C3Func, f2, .7)
        SolutionList.append(C1*vec1+C2*vec2+ C3*vec3)
        ValueList.append(e)
    except:
        pass
    
    try:
        C2, C1, C3, e  = Iteration(C1Func, C3Func, f2, -.7)
        SolutionList.append(C1*vec1+C2*vec2+ C3*vec3)
        ValueList.append(e)
    except:
        pass
    
    try:
        C3, C1, C2, e = Iteration(C1Func, C2Func, f3, .7)
        SolutionList.append(C1*vec1+C2*vec2+ C3*vec3)
        ValueList.append(e)
    except:
        pass
    
    try:
        C3, C1, C2,e  = Iteration(C1Func, C2Func, f3, -.7)
        SolutionList.append(C1*vec1+C2*vec2+ C3*vec3)
        ValueList.append(e)
    except:
        pass
    
    print SolutionList
    print ValueList
    
    return ValueList
    