import numpy as np
import scipy
import pyqchem
import os
import subprocess
import time
import math
import matplotlib.pyplot as plt
import random
import copy
import sys
from multiprocessing import Pool

def optCalc(optObj):
    optObj.main()
    
    
class PiezoMoleculeSolver:
    
    def __init__(self, filename):
        """use a qchem input file,  should contain information that is helpful later"""
        self.reset()
        self.qchemFile = pyqchem.QChemInputParser(filename)
        stub, ext = os.path.splitext(filename)
        self.stub = stub
        self.piezoStub = stub + 'Piezo'
        self.piezoOptStub = stub + 'PiezoOpt'
        self.piezoFieldStubX = stub + 'PiezoFieldX'
        self.piezoFieldStubY = stub + 'PiezoFieldY'
        self.piezoFieldStubZ = stub + 'PiezoFieldZ'
        self.piezoFreqStub   = stub + 'PiezoFreq'
        self.piezoOptReorStub = stub + 'PiezoOptReor'
        self.piezoMatStub    = stub + 'PiezoMat'
        self.piezoMatFullStub = stub + 'fullPiezoMat'
        
    def reset(self):
        #qchem parser obj's and xyz obj lists
        self.qchemFile        = None 
        self.xyzFile          = None
        self.qchemFileFreq    = None
        self.qchemFileFreqOut = None
        self.qchemFileOpt     = None
        self.qchemFileOPtOut  = None
        self.optXyzFile       = None
        self.optXyzFileReor   = None
        self.singleModeOptsIn = []
        self.singleModeOptsOut= []
        
        self.optPositions = []
        #analysis lists will act like arrays, rows are x, y, and z field in that order
        self.analysisInList   = []
        self.analysisZInList  = []
        self.analysisFiniteZInMatrix = []
        self.analysisOutList  = []
        self.analysisZOutList = []
        self.analysisFiniteZOutMatrix = []
        self.analysisFiniteZEnergyMatrix = []
        self.analysisXyzList  = []
        self.analysisZXyzList = []
        #list of h-bond distance for min energy for different field strengths
        self.analysisFiniteZMin = []
        # original hBond distance
        self.hbondDist0 = 0
        
        self.d33 = 0.0
        # list of field values for finite z field calculations
        self.finiteZFieldList = []
        # list of change in h_bond distance for finite z field calculations
        self.finiteZDzList = []
        # list of vectors from mnumpys polyfit
        self.polyFitList = []
        # list of field strengths in z direction a.u. = []
        self.fieldZ = []
        #list of qchem optimizer objects
        self.optList          = []
        #file names and stubs and stub lists
        self.normalModeFile   = None
        self.stub = ''
        self.piezoStub = ''
        self.piezoOptStub= ''
        self.piezoFieldStubX = ''
        self.piezoFieldStubY = ''
        self.piezoFieldStubZ = ''
        self.piezoFreqStub = ''
        self.analysisStubArray=[]
        self.analysisZstub = None

        # a rank 3 tensor row and columns are displacement and field, third index is field number
        # will contain differences in displacement from equiblibrium 
        self.analysisTensor= []
        self.analysisZvector = []
        self.analysisFieldList=[] #in 
        #normalMode obj
        self.normalModeObj    = None
        
        #various matrices
        self.Hessian = None
        self.dipoleDeriv = None
        self.displacementDeriv = None
        self.normalModes = None
        self.vibNormalModes = None
        self.piezoMat=None
        #dictionaries for analysis of piezo matrices
        self.piezoMatDict = {}
        self.piezoMaxEigDict = {}
        self.piezoMaxEigFieldDict = {}
        #atom index lists for material line determination
        self.atomList1 = []
        self.atomList2 = []
        #cluster lists (for monomer identification)
        self.clusters = []
        #indicates atom indices of h-bond participants
        self.hbondIndices = []
        
        self.singleMode = []
        #various keywords related to calculations
        self.df = 0.1 #V/nm
        self.dz = 0.1
        self.dfAu= 0.1/514.220652
        self.optType = 'qchem'
        self.optCommand = 'submitH2P'
        self.optTiming = 96
        self.optPpn    = 1
        self.spPpn = 1
        self.spTiming=5
        #self.optQueue  = 'shared'
        self.freqTiming=96
        self.freqPpn = 1
        #self.freqQueue = 'shared'
        self.analysisFieldNum=5
        # for finite z calculations
        self.analysisFiniteFieldNum = 5
        self.analysisFiniteZNum = 6
        #single mode curvature hartree/bohr^2
        self.singleModeCurvature = 0.0
        #single mode dipole derivative, hartree/bohr/au field
        self.singleModeDipDiv = 0.0 
        
    def setOptType(self, optype):
        """optype: optimization type (string) """
        
        self.optType = optype.lower()
        
        if self.optType == 'qchem':
            print "Optimizations will follow through Qchem"
            
        elif self.optType == 'opt':
            print "Optimizations will proceed via optimize.py" 
            
        else:
            print "Optimization routine: " + self.optype + ", not supported; changing to Optimization routine: qchem"
            self.optType='qchem'
      
    def findClusters(self):
        self.xyzFile.findClusters()
        self.clusters = self.xyzFile.clusters
        
        
    def setOptCommand(self, command):
        self.optCommand= command
        
    def _buildInitXyzFile(self):
        self.qchemFile.write_xyz(self.stub + '.xyz')
        self.xyzFile = pyqchem.xyzParser(self.stub +'.xyz')
        
    def _buildGeneralFile(self, stub, fieldX=0.0, fieldY=0.0, fieldZ=0.0, jobtype = 'opt', positions = None ):
        qchemObj = copy.deepcopy(self.qchemFile)
        
        qchemObj.rem['jobtype'] = jobtype
        qchemObj.rem['sym_ignore']= 'true'
        qchemObj.fields['x']=fieldX
        qchemObj.fields['y']=fieldY
        qchemObj.fields['z']=fieldZ
        
        if positions == None:
            qchemObj.setPositions(self.optPositions)
        else:
            qchemObj.setPositions(positions)
        
        qchemObj.save(stub + '.in')    

        qchemObj = pyqchem.QChemInputParser(stub + '.in')
        return qchemObj
        
    def _buildGeneralOptFile(self, stub, fieldX=0.0, fieldY=0.0, fieldZ=0.0, reor=False):
        
        qchemObj = self.qchemFile
        
        qchemObj.rem['jobtype']='opt'
        qchemObj.fields['x']=fieldX
        qchemObj.fields['y']=fieldY
        qchemObj.fields['z']=fieldZ
        
        if reor==True:
            qchemObj.setPositions(self.optXyzFileReor.positions)
        
        qchemObj.save(stub+ '.in')
        qchemObj = pyqchem.QChemInputParser(stub + '.in')
        return qchemObj
        
        
    def _buildOptFile(self, fieldX=0.0, fieldY=0.00, fieldZ=0.0):
        self.qchemFileOpt = self.qchemFile
        
        self.qchemFileOpt.rem['jobtype']='opt'
        self.qchemFileOpt.fields['x']=fieldX
        self.qchemFileOpt.fields['y']=fieldY
        self.qchemFileOpt.fields['z']=fieldZ
        
        self.qchemFileOpt.save(self.piezoOptStub+ '.in')
        self.qchemFileOpt = pyqchem.QChemInputParser(self.piezoOptStub + '.in')
        
    def _buildFreqFile(self, fieldX=0.000, fieldY=0.000, fieldZ=0.000):
        """fields should be in atomic unints. builds with current atom positions etc."""
        #first copy file information so as not to change anything
        self.qchemFileFreq = self.qchemFile
        
        self.qchemFileFreq.rem['jobtype']='freq'
        self.qchemFileFreq.rem['sym_ignore'] = 'true'
        self.qchemFileFreq.rem['ideriv']=1
        self.qchemFileFreq.fields['x']=fieldX
        self.qchemFileFreq.fields['y']=fieldY
        self.qchemFileFreq.fields['z']=fieldZ
        self.qchemFileFreq.setPositions(self.optXyzFileReor.positions)
        
        self.qchemFileFreq.save(self.piezoFreqStub+'.in')
        
        #reload for later
        self.qchemFileFreq = pyqchem.QChemInputParser(self.piezoFreqStub+'.in')
        
    def _calculateFreq(self):
        self.qchemFileFreq.submitJob('submitH2PHessian', self.freqPpn, self.freqTiming)
        self.qchemFileFreq.checkJobStarted()
        self.qchemFileFreq.checkJobFinished()
        self.qchemFileFreqOut = pyqchem.QChemOutputParser(self.piezoFreqStub +'.out')
#        self._qchemCalc('submitHessianQueue', self.piezoFreqStub,self.freqQueue, self.freqPpn, self.freqTiming)
#        self.qchemFileFreqOut = pyqchem.QChemOutputParser(self.piezoFreqStub +'.out')
        self.normalModeFile  = self.piezoFreqStub + '/389.0'
        
    def _calculateOptQchem(self):
        self.qchemFileOpt.submitJob('submitH2P', self.optPpn, self.optTiming)
        self.qchemFileOpt.checkJobStarted()
        self.qchemFileOpt.checkJobFinished()
        self.qchemFileOptOut = pyqchem.QChemOutputParser(self.piezoOptStub +'.out')
#        self._qchemCalc( 'submitQueue',self.piezoOptStub,self.optQueue, self.optPpn, self.optTiming)
#        self.qchemFileOptOut = pyqchem.QChemOutputParser(self.piezoOptStub +'.out')
    def _calculateOptOpt(self):
        job = pyqchem.optimize.QchemOptimizer(self.qchemFileOpt.file_name)
        job.main()
        self.qchemFileOptOut = job.gradCalcOutList[-1]
    
    def _saveOptXyz(self):
        """save the xyz file for optimized geometry and create xyzParser object"""
        self.qchemFileOptOut.optimizedGeometry(self.piezoOptStub + '.xyz')
        self.optXyzFile = pyqchem.xyzParser(self.piezoOptStub + '.xyz')
    def _saveOptXzyOpt(self):
        self.qchemFileOptOut.write_xyz(self.piezoOptStub + '.xyz')
        self.optXyzFile = pyqchem.xyzParser(self.piezoOptStub + '.xyz')
        
    def _reorientAndSave(self):
        
        vec = self._getCOMDistanceVector(self.optXyzFile)
        com1 = self.optXyzFile.getSubCenterOfMass(self.atomList1)

        self.optXyzFileReor = self.optXyzFile
        newPositions = self._rotateTranslate(vec, self.optXyzFileReor.positions, center=com1)
        self.optXyzFileReor.setPositions(newPositions)
        self.optXyzFileReor.saveXyz(self.piezoOptReorStub+'.xyz')
        #load new reoriented xyz file
        self.optXyzFileReor = pyqchem.xyzParser(self.piezoOptReorStub+'.xyz')
        
    def _buildCartNormalModes(self):
#        self.normalModeObj  = pyqchem.hessian.NormalModeCart(self.normalModeFile)
#        self.normalModes    = self.normalModeObj.cartNormalModes
#        #assume nonlinear molecule; add test for linearity later
#        self.vibNormalModes = self.normalModes[:,:-6]
        hessObj = pyqchem.hessian.HessianManual(self.Hessian)
        hessObj.buildRotTransLessVecSpace(self.qchemFileFreq.rotTransMat)
        self.vibNormalModes = hessObj.vibBasis
        
    def _buildHessian(self):
        self.qchemFileFreqOut.getHessian()
        self.Hessian=self.qchemFileFreqOut.hessian
        
    def _buildDipoleDeriv(self):
        self.qchemFileFreqOut.getDipoleDeriv()
        self.dipoleDeriv=self.qchemFileFreqOut.dipoleDeriv
        
    def _constructDisplacementDeriv(self):
        """Without rotations and translations"""
        Ut = np.transpose(self.vibNormalModes)
        subHess = np.dot(Ut,np.dot(self.Hessian, self.vibNormalModes))
        subDipoleDeriv =np.dot(Ut,self.dipoleDeriv)
        subDxDf = scipy.linalg.solve(subHess,-1.0 * subDipoleDeriv)
        self.displacementDeriv = np.dot(self.vibNormalModes,subDxDf)
        
    def _constructPiezoelectricMatrix(self):
        rvec= self._getCOMDistanceVector(self.optXyzFileReor)
        rdist = np.linalg.norm(rvec)
    
        atomList1Mat=np.zeros((3,3))
        atomList2Mat=np.zeros((3,3))
    
        totMass1 = 0.0
        totMass2 = 0.0
    
        for index in self.atomList1:
            atomList1Mat += self.displacementDeriv[3*index:3*index+3,:]*self.optXyzFileReor.masses[index]
            totMass1     += self.optXyzFileReor.masses[index]
        
        for index in self.atomList2:
            atomList2Mat += self.displacementDeriv[3*index:3*index+3,:]*self.optXyzFileReor.masses[index]
            totMass2     += self.optXyzFileReor.masses[index]
        
        com1Mat = atomList1Mat/totMass1
        com2Mat = atomList2Mat/totMass2
    
        self.piezoMat = (com2Mat-com1Mat)/(rdist* 1.88973*0.51422)
         
    def _getCOMDistanceVector(self, fileObj):
        """gets vector between center of masses for self.atomlists whether file obj is xyz or input or output parser"""
        
        com1=fileObj.getSubCenterOfMass(self.atomList1)
        com2=fileObj.getSubCenterOfMass(self.atomList2)
  
    
        rVec= com2-com1
        
        return rVec
        
    def _rotateTranslate(self, vector, xyzArray, vectorXZ=np.array([0.0,0.0,0.0]), center=np.array([0.0,0.0,0.0])):
        """align a vector with z-axis and place center at the origin"""
        zVec = np.array([0, 0, 1.0])
        yVec = np.array([0, 1.0, 0])
        xVec = np.array([1.0,0 , 0])
    
        #we will shift the origin to coincide with the point given
        xyzArray = xyzArray-center
    
        #project vector onto xy plane    
        vecXy = np.array([vector[0],vector[1], 0.0])
        vecYz = np.array([0.0 ,np.linalg.norm(vecXy), vector[2]])
    
        if np.linalg.norm(vecXy)==0.0:
            return xyzArray

        vecYzNorm = vecYz * 1.0/(np.linalg.norm(vecYz))        
        vecXyNorm = vecXy* 1.0/(np.linalg.norm(vecXy))
        #vecYzNorm =  vecYz * 1.0/(np.linalg.norm(vecYz))

        theta1 = np.arccos(np.dot(vecXyNorm, yVec))
        theta2 = np.arccos(np.dot(vecYzNorm, zVec))

        cross1 = np.cross(vecXyNorm, yVec)
        cross2 = np.cross(vecYzNorm, zVec)

        if cross1[2] < 0:
            theta1 = 2*np.pi -theta1
        if cross2[0] < 0:
            theta2 = 2 *np.pi -theta2

        rotationMatrix =np.array([[np.cos(theta1), -np.sin(theta1), 0.0],
        [np.cos(theta2)*np.sin(theta1), np.cos(theta2)* np.cos(theta1), -np.sin(theta2)],
        [np.sin(theta2)*np.sin(theta1), np.sin(theta2)* np.cos(theta1), np.cos(theta2)]])

        for i in range(xyzArray.shape[0]):
            vecCurrent = xyzArray[i]
            vecNew = np.reshape(np.dot(rotationMatrix, np.reshape(vecCurrent, [3,1])), [3])
            xyzArray[i] = vecNew
            
        #now we rotate vectorX/Z to lie in the xz plane
        #project into xy plane
        vectorXy = np.array([vectorXZ[0], vectorXZ[1], vectorXZ[2]])
        if np.linalg.norm(vectorXy)<1e-6:
            return xyzArray

        vectorXyNorm = vectorXy/np.linalg.norm(vectorXy)
        
        #calculate angle by which to rotate (going counter clockwise)
        theta3 = np.arccos(np.dot(vectorXyNorm,xVec))
        cross3 = np.cross(vectorXyNorm, xVec)
        
        if cross3[2]<0:
            theta3 = 2*np.pi-theta3
        
        #form rotation  matrix
        
        rotMat2 = np.array([[np.cos(theta3),-np.sin(theta3),0],[np.sin(theta3),np.cos(theta3),0],[0,0,1]])
        
        for i in range(xyzArray.shape[0]):
            vecCurrent = xyzArray[i]
            vecNew = np.reshape(np.dot(rotMat2, np.reshape(vecCurrent, [3,1])), [3])
            xyzArray[i] = vecNew
            
        return xyzArray
        
    def _writePiezoMatFile(self):
        handle = open(self.piezoMatStub, 'w')
        
        handle.write('Piezoelectric Matrix:\n\n')
        s=''
        for i in range(3):
            line = self.piezoMat[i,:]
            s+='%10.5f   %10.5f   %10.5f\n' %(line[0], line[1], line[2]) 
        
        s+='\n\n'
        handle.write(s)
        
        handle.write('Atoms used for center of mass 1:\n\n')
        
        s=''
        for i in range(len(self.atomList1)):
            s+='%10.5f\n' % self.atomList1[i]
            
        s+='\n'
        handle.write(s)
        
        handle.write('Atoms used for center of mass 2:\n\n')
        
        s=''
        for i in range(len(self.atomList2)):
            s+='%10.5f\n' % self.atomList2[i]
            
        s+='\n'
        handle.write(s)
        
            
    def _buildAnalysisFileList(self):
        for i in range(3):
            fileObjList =[]
            for j in range(self.analysisFieldNum):
                if i==0:
                    fieldX= self._indexToFieldAu(j)
                    qchemObj= self._buildGeneralOptFile(self.piezoFieldStubX+str(j), fieldX=fieldX, reor=True)
                    
                elif i==1:
                    fieldY= self._indexToFieldAu(j)
                    qchemObj= self._buildGeneralOptFile(self.piezoFieldStubY+str(j), fieldY=fieldY, reor=True)
                    
                elif i==2:
                    fieldZ= self._indexToFieldAu(j)
                    qchemObj= self._buildGeneralOptFile(self.piezoFieldStubZ+str(j), fieldZ=fieldZ, reor=True)
                fileObjList.append(qchemObj)
            self.analysisInList.append(fileObjList)
            
    def _buildAnalysisZFiniteFieldFiles(self):
        pos = copy.copy(self.optPositions)
        for i in range(self.analysisFiniteFieldNum):
            InList = []
            f = self.finiteZFieldList[i]
            for j in range(self.analysisFiniteZNum):
                z = self.finiteZDzList[j]
                newPos = copy.copy(pos)
                print pos
                for atom in self.clusters[1]:
                    newPos[atom,:] += np.array([0.0, 0.0, z])
                qchemObj = self._buildGeneralFile( self.stub+'Field' + str(i) + 'Z' +str(j), fieldZ=f, jobtype = 'sp', positions = newPos )
                InList.append(qchemObj)
            self.analysisFiniteZInMatrix.append(InList)
            
    def _buildAnalysisZFileList(self):
        for i in range(self.analysisFieldNum):
            fieldZ= self._indexToFieldAu(i)
            qchemObj= self._buildGeneralOptFile(self.piezoFieldStubZ+str(i), fieldZ=fieldZ, reor=True)
            self.analysisZInList.append(qchemObj)
        
    def _indexToFieldAu(self, index):
        start = -(self.analysisFieldNum-1)/2.0*self.dfAu
        current =  start + self.dfAu*index
        return current
        
    def _indexToField(self,index):
        return self._indexToFieldAu(index)*514.220652
        
        
    def _runQchemCalcAnalysis(self):
        if self.optType == 'qchem':
            
            if self.optCommand == 'submitH2P':
                for i in range(3):
                    for qchemJob in self.analysisInList[i]:
                        qchemJob.submitJob('submitH2P', self.optPpn, self.optTiming)
                        
                
        for i in range(3):
            for qchemJob in self.analysisInList[i]:
                qchemJob.checkJobStarted()
        
        for i in range(3):
            for qchemJob in self.analysisInList[i]:
                qchemJob.checkJobFinished()
                
        for i in range(3):
            fileObjList =[]
            for j in range(self.analysisFieldNum):
                if i==0:
                    qchemObj = pyqchem.QChemOutputParser(self.piezoFieldStubX+str(j) +'.out')
                    
                elif i==1:
                    qchemObj = pyqchem.QChemOutputParser(self.piezoFieldStubY+str(j) +'.out')                    
                elif i==2:
                    qchemObj = pyqchem.QChemOutputParser(self.piezoFieldStubZ+str(j) +'.out')   
                    
                fileObjList.append(qchemObj)
            self.analysisOutList.append(fileObjList)
            
    def _runQchemCalcAnalysisZ(self):
        for qchemJob in self.analysisZInList:
            qchemJob.submitJob('submitH2P', self.optPpn, self.optTiming)
        for qchemJob in self.analysisZInList:
            qchemJob.checkJobStarted()
        for qchemJob in self.analysisZInList:
            qchemJob.checkJobFinished()
                
        for i in range(self.analysisFieldNum):
            qchemObj = pyqchem.QChemOutputParser(self.piezoFieldStubZ+str(i) +'.out')
            self.analysisZOutList.append(qchemObj)
            
    def _runQchemCalcAnalysisZFiniteField(self):
        for qchemJobList in self.analysisFiniteZInMatrix:
            for qchemJob in qchemJobList:
                qchemJob.submitJob('submitH2P', self.spPpn, self.spTiming)
        for qchemJobList in self.analysisFiniteZInMatrix:
            for qchemJob in qchemJobList:
                qchemJob.checkJobStarted()
        for qchemJobList in self.analysisFiniteZInMatrix:
            for qchemJob in qchemJobList:
                qchemJob.checkJobFinished()
            
        for qchemJobList in self.analysisFiniteZInMatrix:
            outList = []
            for qchemJob in qchemJobList:
                qchemObj = pyqchem.QChemOutputParser(qchemJob.stub + '.out')
                outList.append(qchemObj)                
            self.analysisFiniteZOutMatrix.append(outList)
            
    def _makeOptimizerObjectList(self):
        
        for i in range(3):
            optList = []
            for qchemJob in self.analysisInList[i]:
                optJob = pyqchem.optimize.QchemOptimizer(qchemJob.file_name)
                optList.append(optJob)
            self.optList.append(optList)    
        
    def _runOptCalcAnalysis(self):

            
        jobList = self.optList[0]+self.optList[1]+self.optList[2]
        p = Pool(8)
        p.map(optCalc, jobList)
        
        for i in range(3):
            outFileList = []
            for job in self.optList[i]:
                ### This step is really shady.  I blame the gil ###
                job.main()
                ####
                outFileList.append(job.gradCalcOutList[-1])
            self.analysisOutList.append(outFileList)
        
    def _extractAnalysisOptGeomsOpt(self):  
        for i in range(3):
            xyzList = []
            for j in range(self.analysisFieldNum):
                self.analysisOutList[i][j].write_xyz(self.analysisOutList[i][j].stub + '.xyz')
                xyzFile = pyqchem.xyzParser(self.analysisOutList[i][j].stub + '.xyz')
                xyzList.append(xyzFile)
            self.analysisXyzList.append(xyzList)
        
    def _extractAnalysisOptGeoms(self):
        for i in range(3):
            xyzList = []
            for j in range(self.analysisFieldNum):
                self.analysisOutList[i][j].optimizedGeometry(self.analysisOutList[i][j].stub + '.xyz')
                xyzFile = pyqchem.xyzParser(self.analysisOutList[i][j].stub + '.xyz')
                xyzList.append(xyzFile)
            self.analysisXyzList.append(xyzList)
                
    def _extractAnalysisOptGeomsZ(self):
        for i in range(self.analysisFieldNum):
            self.analysisZOutList[i].optimizedGeometry(self.analysisZOutList[i].stub + '.xyz')
            xyzFile = pyqchem.xyzParser(self.analysisZOutList[i].stub + '.xyz')
            self.analysisZXyzList.append(xyzFile)
            
    def _makeAnalysisMatrixTensor(self):
        
        vec0 = self._getCOMDistanceVector(self.optXyzFileReor)
        self.analysisTensor=np.zeros((3,3,self.analysisFieldNum))
        
        for j in range(3):
            for k in range(self.analysisFieldNum):
                vec1=self._getCOMDistanceVector(self.analysisXyzList[j][k])
                diffVec= vec1-vec0
                print diffVec
                self.analysisTensor[0,j,k]=diffVec[0]
                self.analysisTensor[1,j,k]=diffVec[1]
                self.analysisTensor[2,j,k]=diffVec[2]
                
    def _makeAnalysisZVector(self):
        vec0 = self._getCOMDistanceVector(self.optXyzFileReor)
        self.analysisZvector = np.zeros(self.analysisFieldNum)
        
        for i in range(self.analysisFieldNum):
            vec1 = self._getCOMDistanceVector(self.analysisZXyzList[i])
            diffVec = vec1-vec0
            self.analysisZvector[i]= diffVec[2]
            
    def _makeAnalysisStubArray(self):
        
        xyz=['x','y','z']
        
        for i, letteri in enumerate(xyz):
            stubList = []
            for j,letterj in enumerate(xyz):
                s = '%sf%s'%(letteri, letterj)
                stubList.append(s)
            self.analysisStubArray.append(stubList)
    def _makeAnalysisZStub(self):
        self.analysisZstub = 'zfz'
        
    def _makeAnalysisFiles(self):
        for i in range(3):
            for j in range(3):
                handle = open(self.analysisStubArray[i][j], 'w')
                s=''
                s+= 'field     displacement\n'
                for k in range(self.analysisFieldNum):
                    
                    field = self._indexToField(k)
                    s+='%10.8f   %10.8f\n' %(field, self.analysisTensor[i,j,k])
                handle.write(s)
                handle.close()
                
    def _makeAnalysisZFile(self):
        handle = open(self.analysisZstub, 'w')
        s = ''
        s+= 'field     displacement\n'
        for i in range(self.analysisFieldNum):
            field = self._indexToField(i)
            s+='%10.5f   %10.5f\n' %(field, self.analysisZvector[i])
        handle.close()
        
    def _makePlotsAnalysis(self):
        rVec= self._getCOMDistanceVector(self.optXyzFileReor)
        r0 = np.linalg.norm(rVec)
        print r0
        fList =[]
        for i in range(self.analysisFieldNum):
            fList.append(self._indexToField(i))
            
        xyz  =  [r'$u_x$',r'$u_y$',r'$u_z$']
        fxyz  = [r'$f_x$',r'$f_y$',r'$f_z$']
        
        f, axarr = plt.subplots(3, 3, sharex=True, sharey=True)
        for i in range(3):
            for j in range(3):
                p = np.polyfit(fList, self.analysisTensor[i,j,:],1)
                z = np.poly1d(p)
                axarr[i,j].scatter(fList, self.analysisTensor[i,j,:])
                axarr[i,j].plot(fList, z(fList), 'r-')
                axarr[i,j].set_ylabel('%s' %xyz[i])
                axarr[i,j].set_xlabel('%s' %fxyz[j])
                value = ((p[0]/r0)*1000)
                #a = r'$\frac{\partial^{2}%(displacement)s}{\partial r_{0}\partial %(field)s}=%(calc)10.3f$'
                #b = a% {'displacement':xyz[i],'field':fxyz[j], 'calc':value}
                axarr[i,j].text(0.2, 0.5, str(value), fontsize=25, transform=axarr[i,j].transAxes)
        plt.savefig(self.piezoStub+'Plot')
        plt.show()        
    
    def _makePlotAnalysisZ(self):
        rVec= self._getCOMDistanceVector(self.optXyzFileReor)
        r0 = np.linalg.norm(rVec)
        
        fList =[]
        for i in range(self.analysisFieldNum):
            fList.append(self._indexToField(i))
        
        f, axarr = plt.subplots(3,3)
        
        p = np.polyfit(fList,self.analysisZvector,1)
        z =np.poly1d(p)
        axarr[2,2].scatter(fList, self.analysisZvector)
        axarr[2,2].plot(fList, z(fList), 'r-')
        axarr[2,2].set_ylabel('u_z')
        axarr[2,2].set_xlabel('f_z')
        value = ((p[0]/r0)*1000)
                #a = r'$\frac{\partial^{2}%(displacement)s}{\partial r_{0}\partial %(field)s}=%(calc)10.3f$'
                #b = a% {'displacement':xyz[i],'field':fxyz[j], 'calc':value}
        axarr[2,2].text(0.2, 0.5, str(value), fontsize=25, transform=axarr[2,2].transAxes)
        plt.savefig(self.piezoStub+'Plot')
        plt.show()
        
    def fullPiezoTensorAnalysis(self):
        for i in range(self.qchemFile.size):
            for j in range(i +1, self.qchemFile.size):
                rvec= self.optXyzFileReor.interatomic_distance_vector([i,j])
                rdist = np.linalg.norm(rvec)
    
                atom1Mat=np.zeros((3,3))
                atom2Mat=np.zeros((3,3))
    
                atom1Mat += self.displacementDeriv[3*i:3*i+3,:]
                atom2Mat += self.displacementDeriv[3*j:3*j+3,:]
        
                piezoMat = (atom2Mat-atom1Mat)/(rdist * 1.88973*0.51422)
    
                self.piezoMatDict[(i,j)] = piezoMat
                
                ptp = np.dot(piezoMat.transpose(), piezoMat)
                
                eigVal, eigVec = np.linalg.eigh(ptp)
                eigValMax = np.amax(eigVal)
                MaxIndex  = np.argmax(eigVal)
                
                self.piezoMaxEigDict[(i,j)]= eigValMax
                self.piezoMaxEigFieldDict[(i,j)] = eigVec[:, MaxIndex]
                
    def writeFullPiezoMatrices(self):
        handle = open(self.piezoMatFullStub, 'w')
        
        handle.write('Atom Indices:       Piezoelectric Matrix:          Highest Eigenvalue P^TP (sqrt):        Field Direction: \n\n')
        s=''
        
        for i in range(self.qchemFile.size):
            for j in range(i +1, self.qchemFile.size):
                s+= str(i) + ' ' + str(j) + '           '
                mat = self.piezoMatDict[(i,j)]
                s+= '%10.2f   %10.2f   %10.2f' %(mat[0,0], mat[0,1], mat[0,2]) + '       '
                s+= '%10.3f' %(np.sqrt(self.piezoMaxEigDict[(i,j)])) + '               '
                s+= '%10.2f   %10.2f   %10.2f' %(self.piezoMaxEigFieldDict[(i,j)][0], self.piezoMaxEigFieldDict[(i,j)][1], self.piezoMaxEigFieldDict[(i,j)][2])
                s+= '\n'
                s+= '              '+ '%10.2f   %10.2f   %10.2f' %(mat[1,0], mat[1,1], mat[1,2])
                s+= '\n'
                s+= '              '+ '%10.2f   %10.2f   %10.2f' %(mat[2,0], mat[2,1], mat[2,2])
                s+= '\n\n\n'

        handle.write(s)
        handle.close()
        
    def determineHBondedPair(self, clusterList):
        clus1 = list(clusterList[0])
        clus2 = list(clusterList[1])
            
        distMat = np.zeros((len(clus1), len(clus2)))
        
        for i, atom1 in enumerate(clus1):
            for j, atom2 in enumerate(clus2):
                distMat[i,j] = np.linalg.norm(self.qchemFile.positions[atom1]-self.qchemFile.positions[atom2])
        
        # returns a tuple of single valued arrays
        arrayIndices = np.where(distMat == np.amin(distMat))                               
        
        
        if len(arrayIndices)>2:
            print "More than one pair of atoms associated with hydrogen bond . . . continuing anyways"
        # reformat into list
        indices = []
        indices.append(clus1[arrayIndices[0][0]])
        indices.append(clus2[arrayIndices[1][0]])
            
        print "indices to be used are " + str(indices)      
        return indices          
     
    def _getDimerAnalysisFiniteZEnergyMatrix(self):
         for outList in self.analysisFiniteZOutMatrix:
             energyList = []
             if self.qchemFile.rem['correlation']=='rimp2':
                 for outJob in outList:
                     outJob.getEnergyRIMP2Manual()
                     energyList.append(outJob.energy)
             else:    
                 for outJob in outList:
                     outJob.cclibGetEnergy()
                     energyList.append(outJob.scfEnergies[-1])
             self.analysisFiniteZEnergyMatrix.append(energyList)
     
            
    def _buildZFiniteFieldList(self):
        for i in range(self.analysisFiniteFieldNum):
            
            f =  (-float(self.analysisFiniteFieldNum-1)/2.0 + (float(i))) * self.dfAu
            self.finiteZFieldList.append(f)
        
    def _buildZFiniteFieldDzList(self):
        
        for i in range(self.analysisFiniteZNum):
            
            z = (-float(self.analysisFiniteZNum-1)/2.0 + float(i))* self.dz
            self.finiteZDzList.append(z)
            
    def _findMinimum(self):
        
        for i in range(self.analysisFiniteFieldNum):
            energies = self.analysisFiniteZEnergyMatrix[i]
            p = np.polyfit(self.finiteZDzList, energies,self.analysisFiniteZNum-1)
            self.polyFitList.append(p)
            f = np.poly1d(p)
            zMin = self._newtonMethod(f, 0.0)
            
            self.analysisFiniteZMin.append(zMin)
    def _plotIndividualPotentialCurves(self):
        for i in range(self.analysisFiniteFieldNum):
            f, ax = plt.subplots(1,1)
            energies = self.analysisFiniteZEnergyMatrix[i]
            ax.scatter(self.finiteZDzList, energies)
            g = np.poly1d(self.polyFitList[i])
            x = np.linspace(self.finiteZDzList[0], self.finiteZDzList[-1], 30)
            ax.plot(x, g(x), 'r-')
            zmin = self.analysisFiniteZMin[i]
            ax.scatter([zmin],[g(zmin)])
            ax.text(zmin, g(zmin), 'min at '+ str(zmin))
            plt.savefig(self.piezoStub+'Field' +str(i))
            plt.clf()
    def _findPiezoCoeffientZFinite(self):
        
        print len(self.finiteZFieldList)
        print len(self.analysisFiniteZMin)
        
        p = np.polyfit(self.finiteZFieldList, self.analysisFiniteZMin, 1)
        slope = p[0]/514.220652 *1000
        piezoCoeff = slope/self.hbondDist0
        
        f, ax = plt.subplots(1,1)
        z = np.poly1d(p)
        ax.scatter(self.finiteZFieldList, self.analysisFiniteZMin )
        ax.plot(self.finiteZFieldList, z(self.finiteZFieldList), 'r-')
        ax.text(0.2, 0.5, str(piezoCoeff), fontsize=25, transform=ax.transAxes)
        plt.savefig(self.piezoStub+'Plot')
        plt.show()
    def _newtonMethod(self,f, x0):
        firstDeriv = np.polyder(f)
        secDeriv = np.polyder(firstDeriv)
        xList = []
        xList.append(x0)
        valList = []
        valList.append(firstDeriv(x0))
        slopeList = []
        slopeList.append(secDeriv(x0))
        for i in range(100):
            x = xList[-1] - valList[-1]/slopeList[-1]
            xList.append(x)
            valList.append(firstDeriv(x))
            slopeList.append(secDeriv(x))
            if firstDeriv(x)< 1e-8:
                print secDeriv(x)* (1.0/1.8897161646320724)**2
                break
        return xList[-1]
    def constructSingleMode(self):
        # create array for mode vec
        self.singleMode = np.zeros(self.qchemFile.size*3)
        for atomIndex in self.clusters[0]:
            self.singleMode[3*atomIndex:3*atomIndex + 3] = np.array([0.0, 0.0,1.0])
        for atomIndex in self.clusters[1]:
            self.singleMode[3*atomIndex:3*atomIndex +3] = np.array([0.0,0.0,-1.0])
        
        self.singleMode = self.singleMode/ np.linalg.norm(self.singleMode)
        
        
    def singleModeOpt(self):
        
        mode = self.singleMode
         
        gradList = []
        gList = []
        kList = []
        xList = []
        #doesnt matter what initial x value is
        xList.append(0.0)
        
        inObj = self._buildGeneralFile(self.piezoStub + str(0), jobtype = 'force', positions = self.qchemFile.positions )
        self.singleModeOptsIn.append(inObj)
        inObj.submitJob('submitH2P', self.spPpn, self.spTiming)
        inObj.checkJobStarted()
        inObj.checkJobFinished()
        outObj = pyqchem.QChemOutputParser(inObj.stub + '.out')
        self.singleModeOptsOut.append(outObj)
        
        for i in range(1,100):
            try:
                if inObj.rem['correlation']=='rimp2':
                    self.singleModeOptsOut[-1].getGradientMP2Manual()
                    g = self.singleModeOptsOut[-1].gradient*1.8897161646320724
            except:
                self.singleModeOptsOut[-1].cclibGetGradients()
                g = self.singleModeOptsOut[-1].gradients[-1]*1.8897161646320724
            g = np.reshape(np.dot(g.flatten(), mode)*mode, (self.qchemFile.size, 3))
            gradList.append(g)
            gList.append(np.dot(g.flatten(), mode)) 
            
            if np.amax(np.abs(g)) <1.0e-5:
                return self.singleModeOptsOut[-1]
                break
            
            if len(xList)==1:
                step = -g
                step = 0.3* step/np.linalg.norm(step)
                #step = (10.0* np.exp(-0.05*i)+1)* step
            else:
                k = (gList[-1]-gList[-2])/(xList[-1]-xList[-2])
                kList.append(k)
                if k<0.0:
                    print "curvature negative"
                    step = -g
                    step = 0.3* step/np.linalg.norm(step)
                    #step = (10.0*np.exp(-0.05*i)+1)* step
                else:
                    print "curvature positive"
                    dx = -gList[-1]/k
                    if np.abs(dx) > 0.1:
                        dx = dx/np.abs(dx)*0.1
                    step = np.reshape(dx*mode, (self.qchemFile.size, 3))
            newPos = self.singleModeOptsOut[-1].positions +step            
            xList.append(xList[-1]+ np.dot(step.flatten(),mode ))
            inObj = self._buildGeneralFile(self.piezoStub + str(i), jobtype = 'force', positions = newPos )
            inObj.submitJob('submitH2P', self.spPpn, self.spTiming)
            inObj.checkJobStarted()
            inObj.checkJobFinished()
            outObj = pyqchem.QChemOutputParser(inObj.stub + '.out')
            self.singleModeOptsOut.append(outObj)
            
    def _calcSingleModeD33(self, rotate):
#        print self.Hessian
#        print self.dipoleDeriv
#        print self.hbondDist0
                # create array for mode vec
        mode = np.zeros(self.qchemFile.size*3)
        for atomIndex in self.clusters[0]:
            mode[3*atomIndex:3*atomIndex + 3] = np.array([0.0, 0.0,-1.0/2.0])
        for atomIndex in self.clusters[1]:
            mode[3*atomIndex:3*atomIndex +3] = np.array([0.0,0.0,+1.0/2.0])
            
            
        self.singleMode = np.zeros(self.qchemFile.size*3)
        for atomIndex in self.clusters[0]:
            self.singleMode[3*atomIndex:3*atomIndex + 3] = np.array([0.0, 0.0,-1.0])
        for atomIndex in self.clusters[1]:
            self.singleMode[3*atomIndex:3*atomIndex +3] = np.array([0.0,0.0,+1.0])
        
        self.singleMode = self.singleMode/ np.linalg.norm(self.singleMode)
        
        
        if rotate == False:
            rotMat = np.eye(3* self.qchemFile.size)
            
        elif rotate == True:
            rotMat = np.eye(3* self.qchemFile.size)
            for i in range(self.qchemFile.size):
                rotMat[3*i +1, 3*i+1] = -1.0
                rotMat[3*i + 2, 3*i+2] = -1.0
                
        print rotate
        print rotMat
        
        self.singleModeCurvature = np.dot(mode, np.dot(np.dot(rotMat, np.dot(self.Hessian, rotMat)), mode))    
        k = np.dot(self.singleMode, np.dot(np.dot(rotMat, np.dot(self.Hessian, rotMat)), self.singleMode))
        
        self.singleModeDipDiv = np.dot(mode, np.dot(rotMat,self.dipoleDeriv))[2]
        dd = np.dot(self.singleMode,np.dot(rotMat,self.dipoleDeriv))
        
        d33Vec = 1/k* 1/(self.hbondDist0 * 1.88973*0.51422)*dd 
        d33Mat = np.zeros((len(self.singleMode), 3))
        for i in range(3):
            d33Mat[:,i] = d33Vec[i]*self.singleMode
            
        self.d33 =d33Mat[3*list(self.clusters[1])[0]+2, 2]- d33Mat[3*list(self.clusters[0])[0] +2, 2]
        self.d332 = self.d33 * self.hbondDist0/self.hbondDist
        
    def _writeSingleModeOut(self):
        handle = open(self.piezoStub + 'SingleModeD33' , 'w')
        s = ''
        s += 'd33(pM/V)  curvature(hartree/bohr^2)  DipoleDerive(hartree/(bohr*au))  h-bond Dist(ang)\n'
        s += '% 10.3f      % 10.5f             % 10.5f                % 10.5f\n' %(self.d33, self.singleModeCurvature, self.singleModeDipDiv, self.hbondDist0)
        s += '% 10.3f      % 10.5f             % 10.5f                % 10.5f\n' %(self.d332, self.singleModeCurvature, self.singleModeDipDiv, self.hbondDist)
        s += '\n'
        s += 'h-bond indices: \n'
        s += str(self.hbondIndices[0])+ ' and ' + str(self.hbondIndices[1])
        handle.write(s)
        handle.close()

            #get positions
            #run qchem force job
            #p30 = np.poly1d(np.polyfit(x, y, 30))
     #### controlling calculation functions of the the class ####
    def calculatePiezoelectricMatrix(self, atomList1, atomList2, optPpn=1, optTiming=96, freqPpn=1, freqTiming=96):
        self.atomList1=atomList1
        self.atomList2=atomList2
        #self.optQueue = optQueue
        self.optPpn   = optPpn
        self.optTiming= optTiming
        #self.freqQueue = freqQueue
        self.freqPpn   = freqPpn
        self.freqTiming= freqTiming
        #run main routine
        self._buildOptFile()
        self._calculateOptQchem()
        self._saveOptXyz()
        self._reorientAndSave()
        self._buildFreqFile()
        self._calculateFreq()
        self._buildHessian()
        self._buildDipoleDeriv()
        self._buildCartNormalModes()
        self._constructDisplacementDeriv()
        self._constructPiezoelectricMatrix()
        self._writePiezoMatFile()
        self.fullPiezoTensorAnalysis()
        self.writeFullPiezoMatrices()
        
    def Analysis(self, atomList1, atomList2, fieldNum=5, df=0.1, optPpn=1, optTiming=96):
        self.atomList1=atomList1
        self.atomList2=atomList2
        self.analysisFieldNum=fieldNum #number of optimizations per field value
        self.df = df
        self.dfAu=df/514.220652
        #self.optQueue = optQueue
        self.optPpn   = optPpn
        self.optTiming= optTiming
        #main routine
        #first optimization
        self._buildOptFile()
        if self.optType == 'qchem':
            self._calculateOptQchem()
            self._saveOptXyz()
        elif self.optType == 'opt':
            self._calculateOptOpt()
            self._saveOptXzyOpt()
        self._reorientAndSave()
        self._buildAnalysisFileList()
        if self.optType == 'qchem':            
            self._runQchemCalcAnalysis()
            self._extractAnalysisOptGeoms()
        if self.optType == 'opt':
            self._makeOptimizerObjectList()
            self._runOptCalcAnalysis()
            self._extractAnalysisOptGeomsOpt()
        self._makeAnalysisMatrixTensor()
        self._makeAnalysisStubArray()
        self._makeAnalysisFiles()
        self._makePlotsAnalysis()
    def AnalysisZ(self, atomList1, atomList2, fieldNum=5, df=0.1, optPpn=1, optTiming=96):
        self.atomList1=atomList1
        self.atomList2=atomList2
        self.analysisFieldNum=fieldNum #number of optimizations per field value
        self.df = df
        self.dfAu=df/514.220652
        #self.optQueue = optQueue
        self.optPpn   = optPpn
        self.optTiming= optTiming
        #main routine
        #first optimization
        self._buildOptFile()
        if self.optType == 'qchem':
            self._calculateOptQchem()
            self._saveOptXyz()
        elif self.optType == 'opt':
            self._calculateOptOpt()
            self._saveOptXzyOpt()
        self._reorientAndSave()
        self._buildAnalysisZFileList()
        if self.optType == 'qchem':            
            self._runQchemCalcAnalysisZ()
            self._extractAnalysisOptGeomsZ()
        self._makeAnalysisZVector()
        self._makeAnalysisZStub()
        self._makeAnalysisZFile()
        self._makePlotAnalysisZ()
        
    def DimerAnalysisZFiniteFields(self, optimize = False, numField = 7, df = 0.05,  numCalcs = 6, dz = 0.1, spPpn = 1, spTiming =5):
        """ identifies monomers in qchem input file and h-bonding pair, optimizes them or not,
        puts them back together based on intitial position of bonding pair, and aligns bonding pair with 
        z axis. For each field value, performs numCalcs single point energy calculations, fits resulting energy curve,
        and finds minimum z value, plots min z value vs field.  Also does zero field calculation to get z0 for
        d33 evaluation
        """
        
        if numField%2 == 0: 
            raise ValueError('Number of field calculations should be odd')
            sys.exit(-1)
        # set info for single point energy stuff
        self.spPpn = spPpn
        self.spTiming = spTiming
        self.df = df
        self.dz = dz
        self.dfAu=df/514.220652
        self.analysisFiniteFieldNum = numField
        self.analysisFiniteZNum = numCalcs
        
        # saves self.stub.xyz file and loads self.xyzFile as xyzParser obj
        self._buildInitXyzFile()
        # find clusters in self.xyzFile, save as self.clusters
        self.findClusters()
        
        if len(self.clusters) != 2:
            raise ValueError("number of monomers in system is not equal to 2. Value is equal to " + str(len(self.clusters)))
            
        # optimize individual monomers or not
        if optimize == True:
            # make sure to write this later basically rewrite self.qchemFile after optimizing monomers or different scheme
            pass
        elif optimize == False:
            # determine which pair are hydrogen bonded
            self.hbondIndices = self.determineHBondedPair(self.clusters)
        
            vec = self.qchemFile.positions[self.hbondIndices[1],:]- self.qchemFile.positions[self.hbondIndices[0],:]
            newPos = self._rotateTranslate(vec,self.qchemFile.positions)
            #self.hbondDist0 = np.linalg.norm(vec)
        # set new Positions
        self.qchemFile.setPositions(newPos)
        self.qchemFile.save(self.piezoStub + '.in')
        self.qchemFile = pyqchem.QChemInputParser(self.piezoStub + '.in')
        
        self.constructSingleMode()
        optOut = self.singleModeOpt()
        optOut.rem['jobtype']='sp'
        optOut.rem['sym_ignore'] = 'true'
        optOut.save(self.piezoStub+'PreCalc.in')
        self.optPositions = optOut.positions
                
        self.qchemFile = pyqchem.QChemInputParser(self.piezoStub+'PreCalc.in')

        vec = self.qchemFile.getSubGeomCenter(list(self.clusters[1]))- self.qchemFile.getSubGeomCenter(list(self.clusters[0]))
        self.hbondDist0 = np.linalg.norm(vec)
        #reload as self.qchemFile
        
        
        self._buildZFiniteFieldList()
        self._buildZFiniteFieldDzList()        
        
        self._buildAnalysisZFiniteFieldFiles()
        self._runQchemCalcAnalysisZFiniteField()
        self._getDimerAnalysisFiniteZEnergyMatrix()
        self._findMinimum()
        self._plotIndividualPotentialCurves()
        self._findPiezoCoeffientZFinite()
        
    def dimerSingleModePiezoCalc(self, optimize = True, spPpn = 1, spTiming =5, freqPpn = 8, freqTiming = 96 ):
        """ optimizes monomers along a single coordinate h-bond vector along z-direction using froce calculations,
        then calcualtes full hessian and projects onto mode for piezoelectric response d33"""
        self.spPpn = spPpn
        self.spTiming = spTiming
        self.freqPpn = freqPpn
        self.freqTiming = freqTiming
        
        # saves self.stub.xyz file and loads self.xyzFile as xyzParser obj
        self._buildInitXyzFile()
        # find clusters in self.xyzFile, save as self.clusters
        self.findClusters()
        
        if len(self.clusters) != 2:
            raise ValueError("number of monomers in system is not equal to 2. Value is equal to " + str(len(self.clusters)))    
        # determine indices of h-bond
        self.hbondIndices = self.determineHBondedPair(self.clusters)
        vec = self.qchemFile.positions[self.hbondIndices[1],:]- self.qchemFile.positions[self.hbondIndices[0],:]
        newPos = self._rotateTranslate(vec,self.qchemFile.positions)
        
        self.qchemFile.setPositions(newPos)
        self.qchemFile.save(self.piezoStub + '.in')
        self.qchemFile = pyqchem.QChemInputParser(self.piezoStub + '.in')
        
        self.constructSingleMode()
        optOut = self.singleModeOpt()
        #want dipole moment to point in direction of field
        optOut.extractDipoleMoment()
        
#        if np.sign(optOut.dipoleMoment[2]) == 1.:
#            pass
#        elif np.sign(optOut.dipoleMoment[2]) == -1.:
#            #equivalent to rotating about the x axis
#            optOut.positions[:,1] = optOut.positions[:,1] *-1.0
#            optOut.positions[:,2] = optOut.positions[:,2] * -1.0
        
        if np.sign(optOut.dipoleMoment[2]) == -1.:
            positions = copy.deepcopy(optOut.positions)
            rotate = False
        elif np.sign(optOut.dipoleMoment[2]) == 1.:
            #equivalent to rotating about the x axis
            positions = copy.deepcopy(optOut.positions)
            positions[:,1] = positions[:,1] * -1.0
            positions[:,2] = positions[:,2] * -1.0
            rotate = True
        
        print rotate
        print optOut.positions
        print positions
        
        # want atom lower in z direction to be first in pair, same with clusters
        if positions[self.hbondIndices[1], 2] > positions[self.hbondIndices[0],2]:
            pass
        elif positions[self.hbondIndices[1], 2] < positions[self.hbondIndices[0], 2]:
            newInd = [copy.copy(self.hbondIndices[1]), copy.copy(self.hbondIndices[0])]
            newClus = [copy.copy(self.clusters[1]), copy.copy(self.clusters[0])]
            self.hbondIndices = newInd
            self.clusters = newClus
            
        optOut.rem['jobtype']='freq'
        optOut.rem['sym_ignore'] = 'true'
        optOut.rem['ideriv']=1
        optOut.save(self.piezoStub+'Freq.in')
        optOut.write_xyz(self.piezoStub + 'Opt.xyz')
        
        
        self.qchemFileFreq = pyqchem.QChemInputParser(self.piezoStub + 'Freq.in')
        self._calculateFreq()
        self._buildHessian()
        self._buildDipoleDeriv()
        
        vec = self.qchemFileFreq.getSubGeomCenter(list(self.clusters[1]))- self.qchemFileFreq.getSubGeomCenter(list(self.clusters[0]))
        self.hbondDist0 = np.linalg.norm(vec)
        self.hbondDist = np.linalg.norm(self.qchemFileFreq.positions[self.hbondIndices[1],:]-self.qchemFileFreq.positions[self.hbondIndices[0],:])
        self._calcSingleModeD33(rotate)
        self._writeSingleModeOut()
        
        
def formDxdf(hessFile, normalModeFile, freqOutFile):
    """
    This will form the matrix of position derivatives with respect to field variables
    """
    
    
    freq = pyqchem.QChemOutputParser(freqOutFile)    
    freq.vibrationalAnalysis()
    freq.getHessian()
    freq.getDipoleDeriv()

    
    counter = 0
    for i in range(len(freq.frequencies)):
        if freq.frequencies[i] < 0:
            counter +=1
        else:
            break

  
    print "hessian"  
    print freq.hessian
    print "dipole Deriv qchem"
    print freq.dipoleDeriv
    
    normalModeObj = pyqchem.hessian.NormalModeCart(normalModeFile)
    
    eigval, eigvec = np.linalg.eigh(freq.hessian)
    U = normalModeObj.cartNormalModes[:,:-6]

    
    Ut = np.transpose(U)  

            
    subH = np.dot(Ut,np.dot(freq.hessian, U))
    
    print "here"
    print np.linalg.eigh(subH)
#    subH2 = np.dot(Xt,np.dot(freq.hessian, X))
#    print np.linalg.eigh(subH2)
#    print U
    print "eigenvalues"
    print eigval
    print eigvec
    

    subHrf =np.dot(Ut,freq.dipoleDeriv)
    
    subDxDf = scipy.linalg.solve(subH,-1.0 * subHrf)
    
    dXDF = np.dot(U,subDxDf)
    return dXDF
    
    
def piezoMatricesPeriodic(mopOut, mopOutX,mopOutY, mopOutZ, dfX,dfY, dfZ):
    """output piezoelectric matrices for a crystal.  nearest image needs to be considered.  
    for visualization purposes it is probably a good idea to extend the unit cells to 
    a 2x2x2 supercell and view the distances between these pairs.  to find the closest image
    we use fractional coordinates
    xyzFile1: initial xyz file for system
    xyzFile2: xyz file after deformation
    abcMat1: matrix of crystal vectors for 1st xyz file (vectors are columns)
    abcMat2: same but for 2nd xyzfile 
    df:  field strength used for deformation"""
    
    m  = pyqchem.MopacOutputParser(mopOut)
    mx = pyqchem.MopacOutputParser(mopOutX)    
    my = pyqchem.MopacOutputParser(mopOutY)    
    mz = pyqchem.MopacOutputParser(mopOutZ)    

    m.extractFinalGeometry()
    m.extractFinalUnitCellVecs()
    m.saveOptGeomXyz(m.stub+'opt.xyz')
    mx.extractFinalGeometry()
    mx.extractFinalUnitCellVecs()
    mx.saveOptGeomXyz(mx.stub+'opt.xyz')
    my.extractFinalGeometry()
    my.extractFinalUnitCellVecs()
    my.saveOptGeomXyz(my.stub+'opt.xyz')
    mz.extractFinalGeometry()
    mz.extractFinalUnitCellVecs()
    mz.saveOptGeomXyz(mz.stub+'opt.xyz')    
    
    
    f  = pyqchem.xyzParser(m.stub+'opt.xyz')
    fx = pyqchem.xyzParser(mx.stub+'opt.xyz')
    fy = pyqchem.xyzParser(my.stub+'opt.xyz')
    fz = pyqchem.xyzParser(mz.stub+'opt.xyz')
    
    
    xyzSuperFile = m.stub + 'Super.xyz'
    xyzSuperFileX = mx.stub + 'Super.xyz'    
    xyzSuperFileY = my.stub + 'Super.xyz'  
    xyzSuperFileZ = mz.stub + 'Super.xyz'  
    
    f.createSuperCell(xyzSuperFile, m.finalAbcVecs[0,:], m.finalAbcVecs[1,:], m.finalAbcVecs[2,:], 2, 2, 2)
    fx.createSuperCell(xyzSuperFileX, mx.finalAbcVecs[0,:], mx.finalAbcVecs[1,:], mx.finalAbcVecs[2,:], 2, 2, 2)
    fy.createSuperCell(xyzSuperFileY, my.finalAbcVecs[0,:], my.finalAbcVecs[1,:], my.finalAbcVecs[2,:], 2, 2, 2)
    fz.createSuperCell(xyzSuperFileZ, mz.finalAbcVecs[0,:], mz.finalAbcVecs[1,:], mz.finalAbcVecs[2,:], 2, 2, 2)
    
    xyzSuper = pyqchem.xyzParser(xyzSuperFile)
    xyzSuperX = pyqchem.xyzParser(xyzSuperFileX)
    xyzSuperY = pyqchem.xyzParser(xyzSuperFileY)
    xyzSuperZ = pyqchem.xyzParser(xyzSuperFileZ)
    
    xyzSuper.findMonomerClusters()
    xyzSuperX.findMonomerClusters()
    xyzSuperY.findMonomerClusters()
    xyzSuperZ.findMonomerClusters()
    
    xyzSuper.writeMonomerFile(m.stub+'Super'+'Monomers')
    
    xyzSuper.findGeomCentersMonomers()
    xyzSuperX.findGeomCentersMonomers()
    xyzSuperY.findGeomCentersMonomers()
    xyzSuperZ.findGeomCentersMonomers()
    
    numMon = len(xyzSuper.monomers)
    handle = open(m.stub + 'MonomerCluster.piezo', 'w')
    handle.write('Monomer Indices:       Piezoelectric Matrix:\n\n')
    for i in range(numMon):
        for j in range(i+1, numMon):
            
            mat = np.zeros((3,3))
            
            vec = xyzSuper.monomerGCs[j]-xyzSuper.monomerGCs[i]
            r0 = np.linalg.norm(vec)
            vecx = xyzSuperX.monomerGCs[j]-xyzSuperX.monomerGCs[i]
        
            deltaVecX = vecx-vec
            ux = deltaVecX/r0/dfX*100
   
            vecy = xyzSuperY.monomerGCs[j]-xyzSuperY.monomerGCs[i]
        
            deltaVecY = vecy-vec
            uy = deltaVecY/r0/dfY*100.0
            
            vecz = xyzSuperZ.monomerGCs[j]-xyzSuperZ.monomerGCs[i]
        
            deltaVecZ = vecz-vec
            uz = deltaVecZ/r0/dfZ*100.0
            
            mat[:,0] = ux
            mat[:,1] = uy
            mat[:,2] = uz
            
            handle.write('Monomer ' +str(i) + '   Monomer  ' +str(j) + '\n')
            s = ''
            for k in range(3):
                 s += '                   '
                 s += '%10.5f %10.5f %10.5f\n' % (mat[k,0], mat[k,1], mat[k, 2])
                 s +='\n'
            handle.write(s)

    handle.close()
    
def solveMaxPiezoResp(dTens, fieldIndex = 2, initField = np.array([0.0,0.0,1.0])):
    """Takes dTens and solves for max piezo response. Field index needs to be specified, Other
    indices should be symmetric"""

    
    
    def DerivMax(s, e, f):
        fx = f + np.array([0.01,0.0,0.0])
        fy = f + np.array([0.0,0.01,0.0])
        fz = f + np.array([0.0,0.0,0.01])
        
        sx = np.tensordot(dTens, fx, axes=([fieldIndex], [0]))
        sy = np.tensordot(dTens, fy, axes=([fieldIndex], [0]))
        sz = np.tensordot(dTens, fz, axes=([fieldIndex], [0]))
        
        ex = np.amax(np.linalg.eigh(sx)[0])
        ey = np.amax(np.linalg.eigh(sy)[0])
        ez = np.amax(np.linalg.eigh(sz)[0])
        
        g = np.array([(ex-e)/0.01,(ey-e)/0.01,(ez-e)/0.01])
        return g
        
    def DerivMin(s, e, f):
        fx = f + np.array([0.01,0.0,0.0])
        fy = f + np.array([0.0,0.01,0.0])
        fz = f + np.array([0.0,0.0,0.01])
        
        sx = np.tensordot(dTens, fx, axes=([fieldIndex], [0]))
        sy = np.tensordot(dTens, fy, axes=([fieldIndex], [0]))
        sz = np.tensordot(dTens, fz, axes=([fieldIndex], [0]))
        
        ex = np.amin(np.linalg.eigh(sx)[0])
        ey = np.amin(np.linalg.eigh(sy)[0])
        ez = np.amin(np.linalg.eigh(sz)[0])
        
        g = np.array([(ex-e)/0.01,(ey-e)/0.01,(ez-e)/0.01])
        return g
        
    eValListMax = []
#    eVecList = []    
    strainTensListMax = []
    fieldListMax = []
    fieldListMax.append(initField)
    for i in range(100):
        strainTens = np.tensordot(dTens, fieldListMax[-1], axes=([fieldIndex], [0]))
        eVal, eVec = np.linalg.eigh(strainTens)
        strainTensListMax.append(strainTens)
        eValListMax.append(np.amax(eVal))
        g = DerivMax(strainTensListMax[-1], eValListMax[-1], fieldListMax[-1])
        fNew = fieldListMax[-1]+g
        fieldListMax.append(fNew/np.linalg.norm(fNew)) 
        if np.linalg.norm(fieldListMax[-1]-fieldListMax[-2])<1.0e-5:
            break
        
    eValListMin = []
    strainTensListMin = []
    fieldListMin = []
    fieldListMin.append(initField)
    for i in range(100):
        strainTens = np.tensordot(dTens, fieldListMin[-1], axes=([fieldIndex], [0]))
        eVal, eVec = np.linalg.eigh(strainTens)
        strainTensListMin.append(strainTens)
        eValListMin.append(np.amin(eVal))
        g = DerivMin(strainTensListMin[-1], eValListMin[-1], fieldListMin[-1])
        fNew = fieldListMin[-1]-g
        fieldListMin.append(fNew/np.linalg.norm(fNew)) 
        if np.linalg.norm(fieldListMin[-1]-fieldListMin[-2])<1.0e-4:
            break
        
    return fieldListMin[-1], fieldListMax[-1], eValListMin[-1], eValListMax[-1]



    
def plotEigenValues(dTens, saveName, fieldIndex= 0):
    def f(theta, phi, level = 'low'):
        field = np.array( [np.sin(phi)* np.cos(theta), np.sin(phi)*np.sin(theta),np.cos(phi)])         
        s = np.tensordot(dTens, field, axes = ([fieldIndex], [0]))
        eVal, eVec = np.linalg.eigh(s)
        eVal.sort()
        level.lower()
        if level =='low':
            return eVal[0]
        elif level =='middle':
            return eVal[1]
        elif level == 'high':
            return eVal[2]
    
    tNum=40 
    pNum = 20 
    theta = np.linspace(0,2*np.pi, tNum)
    phi = np.linspace(0, np.pi, pNum)
    f1 = np.zeros((pNum,tNum))
    f2 = np.zeros((pNum,tNum))
    f3 = np.zeros((pNum,tNum))
    Theta, Phi = np.meshgrid(theta, phi)
    for i in range(pNum):
        for j in range(tNum):
            f1[i,j] = f(Theta[i,j], Phi[i,j], level = 'low')
            f2[i,j] = f(Theta[i,j], Phi[i,j], level = 'middle')
            f3[i,j] = f(Theta[i,j], Phi[i,j], level = 'high')
    from mpl_toolkits.mplot3d import Axes3D
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')    
    ax.plot_wireframe(Theta,Phi, f1)
    ax.plot_wireframe(Theta,Phi, f2)
    ax.plot_wireframe(Theta,Phi, f3)
    
    plt.savefig(saveName)
    
def mopacMaxPiezoSolver(TensFile):
    fieldIndex = 0
    
    bigArray = np.loadtxt(TensFile)
    d = bigArray.reshape((3,3,3))
    d [0,:,:] = (d[0,:,:]+ d[0,:,:].transpose())/2
    d [1,:,:] = (d[1,:,:]+ d[1,:,:].transpose())/2
    d [2,:,:] = (d[2,:,:]+ d[2,:,:].transpose())/2
    
    mat = np.eye(3)
    mat = np.hstack((mat, -mat))
    eMin = []
    eMax = []
    fMin = []
    fMax = []
    for i in range(6):
        fieldMin, fieldMax, eValMin, eValMax = solveMaxPiezoResp(d, fieldIndex = 0, initField = mat[:,i])
        eMin.append(eValMin)
        eMax.append(eValMax)
        fMin.append(fieldMin)
        fMax.append(fieldMax)
        
    maxVal = np.amax(eMax)
    minVal = np.amin(eMin)
    fieldMax = fMax[np.argmax(eMax)]
    fieldMin = fMin[np.argmin(eMin)]
    
    print maxVal, minVal, fieldMax, fieldMin
    print eMin, eMax, fMin, fMax
    
    plotEigenValues(d,  TensFile + 'Fin', fieldIndex)
    handle = open(TensFile + 'Max', 'w')
    s= ''
    s+= 'Max eigenvalue of Strain Tensor:           Field Direction:\n\n'
    s += str(maxVal) + '                    ' + str(fieldMax)
    handle.write(s)
    
def run():
    files = os.listdir(os.getcwd())
    
    for fileName in files:
        if 'Tens' in fileName:
            try:
                mopacMaxPiezoSolver(fileName) 
            except:
                continue
        else:
            continue
        
def test():
    for i in range(30):
        a = np.random.random((3,3))-np.random.random()
        b = np.random.random((3,3))-np.random.random()
        c = np.random.random((3,3))-np.random.random()
        
        a = np.random.random()*10*np.dot(a.transpose(),a)
        b = np.random.random()*10*np.dot(b.transpose(),b)
        b = np.random.random()*10*np.dot(c.transpose(),c)
        
        d = np.array([a,b,c])
        
        mat = np.eye(3)
        mat = np.hstack((mat, -mat))
        eValList = []
        for i in range(6):
            fieldMin, fieldMax, eValMin, eValMax = solveMaxPiezoResp(d, fieldIndex = 0, initField = mat[:,i])
            eValList.append([eValMin, eValMax])
        print eValList


def test2():
    a = np.random.random((3,3))-np.random.random()
    b = np.random.random((3,3))-np.random.random()
    c = np.random.random((3,3))-np.random.random()
        
    a = np.random.random()*10*np.dot(a.transpose(),a)
    b = np.random.random()*10*np.dot(b.transpose(),b)
    b = np.random.random()*10*np.dot(c.transpose(),c)
        
    d = np.array([a,b,c])
    
    mat = np.eye(3)
    mat = np.hstack((mat, -mat))
    eValList = []
    for i in range(6):
        fieldMin, fieldMax, eValMin, eValMax = solveMaxPiezoResp(d, fieldIndex = 0, initField = mat[:,i])
        eValList.append([eValMin, eValMax])
    print eValList    
    
    plotEigenValues(d, fieldIndex = 0)
#def formDxdf(hessFile, normalModeFile, gradFile1, gradFileX, gradFileY, gradFileZ, df, freqOutFile, masses, positions, rotationalTensor, momentInertiaTensor):
#    """
#    This will form the matrix of position derivatives with respect to field variables
#    """
#    
#    
#    freq = pyqchem.QChemOutputParser(freqOutFile)    
#    freq.vibrationalAnalysis()
#    freq.getHessian()
#    freq.getDipoleDeriv()
#
#    
#    counter = 0
#    for i in range(len(freq.frequencies)):
#        if freq.frequencies[i] < 0:
#            counter +=1
#        else:
#            break
#        
#    
##    hess = pyqchem.hessian.Hessian(hessFile)
#    
#    print "eigenvalues"
#    print np.linalg.eigh(freq.hessian)
#    
#
#    grad1 = pyqchem.gradient.Gradient(gradFile1)
#    gradX = pyqchem.gradient.Gradient(gradFileX)
#    gradY = pyqchem.gradient.Gradient(gradFileY)
#    gradZ = pyqchem.gradient.Gradient(gradFileZ)
#    
#    Hxf =np.array((gradX.gradient - grad1.gradient)/df).flatten()
#    Hyf =np.array((gradY.gradient - grad1.gradient)/df).flatten()
#    Hzf =np.array((gradZ.gradient - grad1.gradient)/df).flatten()
#    
#    Hrf = np.zeros((freq.hessian.shape[0] , 3))
#    Hrf[:,0] = Hxf
#    Hrf[:,1] = Hyf
#    Hrf[:,2] = Hzf
#  
#    print "hessian"  
#    print freq.hessian
#    
#    print "dipole Deriv finite"
#    print Hrf
#    print "dipole Deriv qchem"
#    print freq.dipoleDeriv
#    
#    normalModeObj = pyqchem.hessian.NormalModeCart(normalModeFile)
#    #print normalModeObj.cartNormalModes[:,-6:]
##    print normalModeObj.cartNormalModes
#    
##    eigval, eigvec = np.linalg.eigh(hess.hessian)
##    U = np.transpose(normalModeObj.cartNormalModes)[:,counter:-6]
##    Ut = np.transpose(U)  
#    
#    eigval, eigvec = np.linalg.eigh(freq.hessian)
#    U = normalModeObj.cartNormalModes[:,:-6]
##    X = hess.removeTransRot(masses, positions, rotationalTensor, momentInertiaTensor)
##    U = normalModeObj.cartNormalModes[:,:-5]
#
#    ###
##    U = np.reshape(eigvec[:,-1], (6, 1))
##    ###
##    print U
#    
#    Ut = np.transpose(U)  
##    Xt = np.transpose(X)
#    
##    print eigval
##    
##    ###test stuff###
##    a = np.linalg.pinv(Ut)
##    at = np.transpose(a)
##    
##    b = np.dot(a, Ut)
##    print b
##    print np.shape(b)
##
##    subHA = np.dot(at,np.dot(hess.hessian, a))
##    
##    eigval, eigvec = np.linalg.eigh(subHA)
##    print eigval
##    
##    subHrfA =np.dot(at,Hrf)
##    subDxDfA = scipy.linalg.solve(subHA,-1.0 * subHrfA)
##    dXDFA = np.dot(a,subDxDfA)
##    return dXDFA
##    ################
#            
#    subH = np.dot(Ut,np.dot(freq.hessian, U))
#    
#    print "here"
#    print np.linalg.eigh(subH)
##    subH2 = np.dot(Xt,np.dot(freq.hessian, X))
##    print np.linalg.eigh(subH2)
##    print U
#    print "eigenvalues"
#    print eigval
#    print eigvec
#    
##    print subH
#    
#    
#    
##    
##    subHrf =np.dot(Ut,freq.dipoleDeriv)
#    subHrf = np.dot(Ut,Hrf)
##    subHrf2=np.dot(Xt,Hrf)
#    
##    print subHrf
#    
#    subDxDf = scipy.linalg.solve(subH,-1.0 * subHrf)
##    subDxDf2 = scipy.linalg.solve(subH2,-1.0 * subHrf2)
#    
#    dXDF = np.dot(U,subDxDf)
##    dXDF2 = np.dot(X, subDxDf2)
##    dXDF2 = np.zeros(np.shape(dXDF))
#    
##    dXDF2 = np.dot(U,np.dot(Ut,scipy.linalg.solve(hess.hessian, -1.0*Hrf)))
#    return dXDF
    
#def getDxdf(inputFile, df, index1, index2, cores = 4, timeout = 96):
#    """Goes from input file to calculating dxdf matrix for nuclear coordinates"""
#    
#    #parsedFile = pyqchem.QChemInputParser(inputFile)
#    #calculate times for short calculation
#    timeShort = int(math.floor(timeout/5.0))
#    if timeShort < 1:
#        timeShort = 1
#
#    #split the input filename
#    stub, ext = os.path.splitext(inputFile)
#
#    #establish other useful file names to be used for the calculation
#    stubPiezo = stub + 'Piezo'
#    stubPiezoFreq = stub + 'PiezoFreq'
#    stubPiezoGrad = stub + 'PiezoGrad'
#    stubPiezoFieldX = stub + 'PiezoFieldX'
#    stubPiezoFieldY = stub + 'PiezoFieldY'
#    stubPiezoFieldZ = stub + 'PiezoFieldZ'
#
#    #use qchem parser class to format optimization file
#    inFile = pyqchem.QChemInputParser(inputFile)
#
#    inFile.rem['jobtype'] = 'opt'
#    
#    inFile.fields['x'] = 0.000
#    inFile.fields['y'] = 0.000
#    inFile.fields['z'] = 0.000
#
#    inFile.save(stubPiezo + '.n')
#
#    #submit optimization
#    command0 = r'submit {stub} {cores:d} {timing:d}'
#    #we need to make sure the job has not already been completed
#    try:
#        handle = open(stubPiezo + '.out', 'r' )
#        if pyqchem.utils.checkJobFinished(stubPiezo + '.out')== True:
#            pass
#    except:
#        subprocess.call(command0.format(stub=stubPiezo, cores=cores, timing=timeout).split(),0)
##    subprocess.call(('submit',stubPiezo, cores, timeout), 0)
#
#    while True:
#        try:
#            handle = open(stubPiezo + '.out', 'r' )
#            handle.close()
#            break
#        except:
#            time.sleep(100)
#            continue
#
#    counter = 0
#    while pyqchem.utils.checkJobFinished(stubPiezo + '.out')== False:
#        time.sleep(300) #sleep for 5 minutes
#        counter = counter + 300
#        if counter >= timeout * 3600:
#            raise RuntimeError('geometry optimization did not finish in allotted time')
#
#    outFile = pyqchem.QChemOutputParser(stubPiezo + '.out')
#    outFile.optimizedGeometry(stubPiezo + '.xyz')
#
#    # need to rotate and translate system so that two atoms of interest lie parallel to the z-axis
#    xyz = pyqchem.xyzParser(stubPiezo + '.xyz')
#
#    newPositions = rotationTranslation(xyz.positions, index1, index2)
#
#    # need a new command for the special submit_hessian routine which 
#    command = r'submit_hessian {stub} {cores:d} {timing:d}'
#
#    # set new positions to that of translated and rotated system
#    inFile.setPositions(newPositions)
#    
#    # make sure necessary fields are correct
#    inFile.rem['jobtype'] = 'freq'
#    inFile.rem['sym_ignore'] = 'true'
#    
#    if inFile.size > 25:
#        inFile.rem['ideriv'] = 1
#        cores = cores*2
#        timeout = timeout * 2
#        
#    inFile.save(stubPiezoFreq+ '.n')
##    inFile.addJobandSave('force', stubPiezoFreq + '.n')
#
#    #check if job is finished first
#    try:
#        handle = open(stubPiezoFreq + '.out', 'r' )
#        handle.close()
#        if pyqchem.utils.checkJobFinished(stubPiezoFreq + '.out')== True:
#            pass
#    except:
#        subprocess.call(command.format(stub=stubPiezoFreq, cores=cores, timing=timeout).split(),0)
#    #subprocess.call(('submit_hessian', stubPiezoFreq, str(cores), str(time/10.0)))
#
#    inFile.rem['jobtype'] = 'force'
#    inFile.save(stubPiezoGrad + '.n')
#    cores = cores/2
#    
#    try:
#        handle = open(stubPiezoGrad + '.out', 'r' )
#        handle.close()
#        if pyqchem.utils.checkJobFinished(stubPiezoGrad + '.out')== True:
#            pass
#    except:
#        subprocess.call(command.format(stub=stubPiezoGrad, cores=cores, timing=timeShort).split(),0)
#    
#    #form and save field files (for x, y, and z field)
#    inFile.rem['jobtype'] = 'force'
#    inFile.fields['x']    = df
#    inFile.fields['y'] = 0.000
#    inFile.fields['z'] = 0.000
#    inFile.save(stubPiezoFieldX + '.n')
#    inFile.fields.clear()
#
#    inFile.fields['y']   = df
#    inFile.fields['x'] = 0.000
#    inFile.fields['z'] = 0.000
#    inFile.save(stubPiezoFieldY + '.n')
#    inFile.fields.clear()
#
#    inFile.fields['z']    = df
#    inFile.fields['x'] = 0.000
#    inFile.fields['y'] = 0.000
#    inFile.save(stubPiezoFieldZ + '.n')
#    inFile.fields.clear()
#
#    # check to see if they are already done
#    try:
#        handle = open(stubPiezoFieldX + '.out', 'r' )
#        handle.close()
#        if pyqchem.utils.checkJobFinished(stubPiezoFieldX + '.out')== True:
#            pass
#    except:
#        subprocess.call(command.format(stub=stubPiezoFieldX, cores=cores, timing=timeShort).split(),0)
#    #subprocess.call(('submit_hessian', stubPiezoField, str(cores), str(time/10.0)))
#
#    try:
#        handle = open(stubPiezoFieldY + '.out', 'r' )
#        handle.close()
#        if pyqchem.utils.checkJobFinished(stubPiezoFieldY + '.out')== True:
#            pass
#    except:
#        subprocess.call(command.format(stub=stubPiezoFieldY, cores=cores, timing=timeShort).split(),0)
#
#    try:
#        handle = open(stubPiezoFieldZ + '.out', 'r' )
#        handle.close()
#        if pyqchem.utils.checkJobFinished(stubPiezoFieldZ + '.out')== True:
#            pass
#    except:
#        subprocess.call(command.format(stub=stubPiezoFieldZ, cores=cores, timing=timeShort).split(),0)
#
## check to see if the outfiles exist yet
#    while True:
#        try:
#            handle = open(stubPiezoFreq + '.out', 'r' )
#            handle.close()
#            handle = open(stubPiezoGrad + '.out', 'r' )
#            handle.close()
#            handle = open(stubPiezoFieldX + '.out', 'r' )
#            handle.close()
#            handle = open(stubPiezoFieldY + '.out', 'r' )
#            handle.close()
#            handle = open(stubPiezoFieldZ + '.out', 'r' )
#            handle.close()
#            break
#        except:
#            time.sleep(100)
#            continue
#
##check to see if finished
#    counter2=0
#    while pyqchem.utils.checkJobFinished(stubPiezoFreq + '.out')==False or\
#        pyqchem.utils.checkJobFinished(stubPiezoGrad   + '.out') ==False   or\
#        pyqchem.utils.checkJobFinished(stubPiezoFieldX + '.out')==False   or\
#        pyqchem.utils.checkJobFinished(stubPiezoFieldY + '.out')==False   or\
#        pyqchem.utils.checkJobFinished(stubPiezoFieldZ + '.out')==False:
#        time.sleep(300)
#        counter2 = counter2 + 300
#        if counter2 >= timeout *3600:
#                if pyqchem.utils.checkJobFinished(stubPiezoFreq + '.out')==False:
#                    raise RuntimeError('hessian calculation has exceeded time limit')
#        if counter2 >= timeShort * 3600:
#            if pyqchem.utils.checkJobFinished(stubPiezoGrad + '.out')==False:
#                raise RuntimeError('Zero Field Gradient calculation has exceeded time limit')
#            if pyqchem.utils.checkJobFinished(stubPiezoFieldX + '.out')==False:
#                raise RuntimeError('Gradient calculation at set field x value has exceeded time limit')
#            if pyqchem.utils.checkJobFinished(stubPiezoFieldY + '.out')==False:
#                raise RuntimeError('Gradient calculation at set field y value has exceeded time limit')
#            if pyqchem.utils.checkJobFinished(stubPiezoFieldZ + '.out')==False:
#                raise RuntimeError('Gradient calculation at set field z value has exceeded time limit')
#
#    dxdf = formDxdf(stubPiezoFreq + '/HESS', stubPiezoFreq + '/389.0', stubPiezoGrad + '/GRAD',
#                        stubPiezoFieldX + '/GRAD', stubPiezoFieldY + '/GRAD',
#                        stubPiezoFieldZ + '/GRAD', df, stubPiezoFreq +'.out', inFile.masses, inFile.positions, inFile.rotationalTensor, inFile.momentInertiaTensor)
#                    
#    return dxdf, inFile
    
def getDxdfCOM(inputFile, atomList1, atomList2, cores=4, timeout=96):
    
    """Goes from input file to calculating dxdf matrix for nuclear coordinates"""
    
    #parsedFile = pyqchem.QChemInputParser(inputFile)
    #calculate times for short calculation
    timeShort = int(math.floor(timeout/5.0))
    if timeShort < 1:
        timeShort = 1

    #split the input filename
    stub, ext = os.path.splitext(inputFile)

    #establish other useful file names to be used for the calculation
    stubPiezo = stub + 'Piezo'
    stubPiezoFreq = stub + 'PiezoFreq'
    #use qchem parser class to format optimization file
    inFile = pyqchem.QChemInputParser(inputFile)

    inFile.rem['jobtype'] = 'opt'
    
    inFile.fields['x'] = 0.000
    inFile.fields['y'] = 0.000
    inFile.fields['z'] = 0.000

    inFile.save(stubPiezo + '.n')

    #submit optimization
    command0 = r'submit {stub} {cores:d} {timing:d}'
    #we need to make sure the job has not already been completed
    try:
        handle = open(stubPiezo + '.out', 'r' )
        if pyqchem.utils.checkJobFinished(stubPiezo + '.out')== True:
            pass
    except:
        subprocess.call(command0.format(stub=stubPiezo, cores=cores, timing=timeout).split(),0)
#    subprocess.call(('submit',stubPiezo, cores, timeout), 0)

    while True:
        try:
            handle = open(stubPiezo + '.out', 'r' )
            handle.close()
            break
        except:
            time.sleep(100)
            continue

    counter = 0
    while pyqchem.utils.checkJobFinished(stubPiezo + '.out')== False:
        time.sleep(300) #sleep for 5 minutes
        counter = counter + 300
        if counter >= timeout * 3600:
            raise RuntimeError('geometry optimization did not finish in allotted time')

    outFile = pyqchem.QChemOutputParser(stubPiezo + '.out')
    outFile.optimizedGeometry(stubPiezo + '.xyz')

    # need to rotate and translate system so that two atoms of interest lie parallel to the z-axis
    xyz = pyqchem.xyzParser(stubPiezo + '.xyz')
    
    #generate vectors to feed rotationTranslationVec
    #get center of masses for atomLists (indexes of atoms)
    cOM1 = xyz.getSubCenterOfMass(atomList1)
    cOM2 = xyz.getSubCenterOfMass(atomList2)
    
    #vector to align along z-axis
    alignVec= cOM2-cOM1
    
    newPositions = rotationTranslationVec(xyz.positions, alignVec, cOM1)

    # need a new command for the special submit_hessian routine which 
    command = r'submit_hessian {stub} {cores:d} {timing:d}'

    # set new positions to that of translated and rotated system
    inFile.setPositions(newPositions)
    
    # make sure necessary fields are correct
    inFile.rem['jobtype'] = 'freq'
    inFile.rem['sym_ignore'] = 'true'
    inFile.rem['ideriv']=1
    cores =cores*2
    timeout=timeout*2
#    if inFile.size > 25:
#        inFile.rem['ideriv'] = 1
#        cores = cores*2
#        timeout = timeout * 2
        
    inFile.save(stubPiezoFreq+ '.n')
#    inFile.addJobandSave('force', stubPiezoFreq + '.n')

    #check if job is finished first
    try:
        handle = open(stubPiezoFreq + '.out', 'r' )
        handle.close()
        if pyqchem.utils.checkJobFinished(stubPiezoFreq + '.out')== True:
            pass
    except:
        subprocess.call(command.format(stub=stubPiezoFreq, cores=cores, timing=timeout).split(),0)
    #subprocess.call(('submit_hessian', stubPiezoFreq, str(cores), str(time/10.0)))


# check to see if the outfiles exist yet
    while True:
        try:
            handle = open(stubPiezoFreq + '.out', 'r' )
            handle.close()
            break
        except:
            time.sleep(100)
            continue

#check to see if finished
    counter2=0
    while pyqchem.utils.checkJobFinished(stubPiezoFreq + '.out')==False:
        time.sleep(300)
        counter2 = counter2 + 300
        if counter2 >= timeout *3600:
                if pyqchem.utils.checkJobFinished(stubPiezoFreq + '.out')==False:
                    raise RuntimeError('hessian calculation has exceeded time limit')
 
    dxdf = formDxdf(stubPiezoFreq + '/HESS', stubPiezoFreq + '/389.0',  stubPiezoFreq +'.out')
                    
    return dxdf, inFile
    
    
def getDxdf(inputFile, index1, index2, cores = 4, timeout = 96):
    """Goes from input file to calculating dxdf matrix for nuclear coordinates"""
    
    #parsedFile = pyqchem.QChemInputParser(inputFile)
    #calculate times for short calculation
    timeShort = int(math.floor(timeout/5.0))
    if timeShort < 1:
        timeShort = 1

    #split the input filename
    stub, ext = os.path.splitext(inputFile)

    #establish other useful file names to be used for the calculation
    stubPiezo = stub + 'Piezo'
    stubPiezoFreq = stub + 'PiezoFreq'
    #use qchem parser class to format optimization file
    inFile = pyqchem.QChemInputParser(inputFile)

    inFile.rem['jobtype'] = 'opt'
    
    inFile.fields['x'] = 0.000
    inFile.fields['y'] = 0.000
    inFile.fields['z'] = 0.000

    inFile.save(stubPiezo + '.n')

    #submit optimization
    command0 = r'submit {stub} {cores:d} {timing:d}'
    #we need to make sure the job has not already been completed
    try:
        handle = open(stubPiezo + '.out', 'r' )
        if pyqchem.utils.checkJobFinished(stubPiezo + '.out')== True:
            pass
    except:
        subprocess.call(command0.format(stub=stubPiezo, cores=cores, timing=timeout).split(),0)
#    subprocess.call(('submit',stubPiezo, cores, timeout), 0)

    while True:
        try:
            handle = open(stubPiezo + '.out', 'r' )
            handle.close()
            break
        except:
            time.sleep(100)
            continue

    counter = 0
    while pyqchem.utils.checkJobFinished(stubPiezo + '.out')== False:
        time.sleep(300) #sleep for 5 minutes
        counter = counter + 300
        if counter >= timeout * 3600:
            raise RuntimeError('geometry optimization did not finish in allotted time')

    outFile = pyqchem.QChemOutputParser(stubPiezo + '.out')
    outFile.optimizedGeometry(stubPiezo + '.xyz')

    # need to rotate and translate system so that two atoms of interest lie parallel to the z-axis
    xyz = pyqchem.xyzParser(stubPiezo + '.xyz')

    newPositions = rotationTranslation(xyz.positions, index1, index2)

    # need a new command for the special submit_hessian routine which 
    command = r'submit_hessian {stub} {cores:d} {timing:d}'

    # set new positions to that of translated and rotated system
    inFile.setPositions(newPositions)
    
    # make sure necessary fields are correct
    inFile.rem['jobtype'] = 'freq'
    inFile.rem['sym_ignore'] = 'true'
    inFile.renm['ideriv']=1
    cores =cores*2
    timeout=timeout*2
#    if inFile.size > 25:
#        inFile.rem['ideriv'] = 1
#        cores = cores*2
#        timeout = timeout * 2
        
    inFile.save(stubPiezoFreq+ '.n')
#    inFile.addJobandSave('force', stubPiezoFreq + '.n')

    #check if job is finished first
    try:
        handle = open(stubPiezoFreq + '.out', 'r' )
        handle.close()
        if pyqchem.utils.checkJobFinished(stubPiezoFreq + '.out')== True:
            pass
    except:
        subprocess.call(command.format(stub=stubPiezoFreq, cores=cores, timing=timeout).split(),0)
    #subprocess.call(('submit_hessian', stubPiezoFreq, str(cores), str(time/10.0)))


# check to see if the outfiles exist yet
    while True:
        try:
            handle = open(stubPiezoFreq + '.out', 'r' )
            handle.close()
            break
        except:
            time.sleep(100)
            continue

#check to see if finished
    counter2=0
    while pyqchem.utils.checkJobFinished(stubPiezoFreq + '.out')==False:
        time.sleep(300)
        counter2 = counter2 + 300
        if counter2 >= timeout *3600:
                if pyqchem.utils.checkJobFinished(stubPiezoFreq + '.out')==False:
                    raise RuntimeError('hessian calculation has exceeded time limit')
 
    dxdf = formDxdf(stubPiezoFreq + '/HESS', stubPiezoFreq + '/389.0',  stubPiezoFreq +'.out')
                    
    return dxdf, inFile
    
def getPiezoD33(dxdf, r0, index1, index2):
    """
    a function for calculating the piezo coefficinet from
    hessian and gradient files
    index1 and 2 indicate the starting point for the rows from which to extract the members (3x3 array)
    from the final matrix from the inverse hessian multiplied by the mixed position field derivative matrix
    """
    print r0
    print dxdf

    print dxdf[index1, 2]
    print dxdf[index2,2]
    dz21df = dxdf[index1, 2]- dxdf[index2,2]
    
    piezo = abs(dz21df)/(r0*1.88973)*1.0/(0.514220652)

    return piezo

#def fullPiezoTensor(dxdf, positions):
#    
#    ### we need to describe the box in which the system lives ###
#    X, Y, Z, xList, yList, zList = piezoBox(positions)
#    
#    xIndex1 = 3 * xList[0]
#    yIndex1 = 3 * yList[0]
#    zIndex1 = 3 * zList[0]
#    
#    xIndex2 = 3 * xList[1]
#    yIndex2 = 3 * yList[1]
#    zIndex2 = 3 * zList[1]
#    ### done
#
#    
#    ### box stuff
#    
#    dXDFX = dxdf[xIndex1:xIndex1+3,:] - dxdf[xIndex2:xIndex2+3,:]
#    dXDFY = dxdf[yIndex1:yIndex1+3,:] - dxdf[yIndex2:yIndex2+3,:]
#    dXDFZ = dxdf[zIndex1:zIndex1+3,:] - dxdf[zIndex2:zIndex2+3,:]
#    ###
#    piezoTenX = dXDFX/X
#    piezoTenY = dXDFY/Y
#    piezoTenZ = dXDFZ/Z
#    
#    print piezoTenX, piezoTenY, piezoTenZ
#    
#    piezoTensorFull = np.zeros((3,3,3))
#    
#    piezoTensorFull[:,0,:] = piezoTenX
#    piezoTensorFull[:,1,:] = piezoTenY
#    piezoTensorFull[:,2,:] = piezoTenZ
#    
#    print piezoTensorFull
#    
#    
#    #symmeterize
#    for k in range(3):
#        for i in range(3):
#            for j in range(i +1,3):
#                piezoTensorFull[i,j, k] = (piezoTensorFull[i,j,k]+piezoTensorFull[j,i,k])/2.0
#                piezoTensorFull[j,i, k] = piezoTensorFull[i,j, k]
#                
#    print piezoTensorFull
#    piezoTensorFull = piezoTensorFull/1.88973*10/(5.142)
#    print piezoTensorFull
#
#    #construct S
#    np.zeros((3,3))
#    thing01 = dXDF[index1:index1+3, :]
#    thing02 = dXDF[index2:index2+3, :]  
#    
##    thing = np.dot(X,-1.0 *np.dot(invSubH, subHrf))
##    thing1 = thing[index1:index1+3, :]
##    thing2 = thing[index2:index2+3, :]  
##    
##    piezoTensor = thing1-thing2
#    piezoTensor0 = thing01-thing02
#
#
#    
#    eigenval, eigenvec = np.linalg.eig(piezoTensor0)
#    
#    PtP = np.dot(np.transpose(piezoTensor0), piezoTensor0) 
##    print PtP
#    
#    eigvalptp, eigvecptp = np.linalg.eigh(PtP)
##    print eigvalptp, eigvecptp
#    x = r0[0]
#    y = r0[1]
#    z = r0[2]
#    
#    piezoTensor0[0,:] = piezoTensor0[0,:]/x
#    piezoTensor0[1,:] = piezoTensor0[1,:]/y
#    piezoTensor0[2,:] = piezoTensor0[2,:]/z
#    
#    
#    eigval1, eigvec1 = np.linalg.eig(piezoTensor0)
#    
##    print  piezoTensor0
##    print eigval1*10/(5.142*1.88973)
##    print eigvec1
#    
#    r00 = np.dot(r0,eigenvec[:,0])/np.linalg.norm(eigenvec[:,0])
#    r01 = np.dot(r0,eigenvec[:,1])/np.linalg.norm(eigenvec[:,1])
#    r02 = np.dot(r0,eigenvec[:,2])/np.linalg.norm(eigenvec[:,2])
#
#    piezo0 = -1.0 * eigenval[0]/(r00*1.88973)*10/(5.142)
#    piezo1 = -1.0 * eigenval[1]/(r01*1.88973)*10/(5.142)
#    piezo2 = -1.0 * eigenval[2]/(r02*1.88973)*10/(5.142)
#
#
#    return piezo0, piezo1, piezo2, eigenval, eigenvec 
    
def rotationTranslationVec(xyzArray, vector, center=np.array([0.0,0.0,0.0])):
    """
    a vector version of the rotation and translation function, where you 
    are given a vector to align along the z axis and a point to put at the origin.
    Useful if you are using centers which correspond to center of mass and other
    points which do not correspond to 
    """
    zVec = np.array([0, 0, 1.0])
    yVec = np.array([0, 1.0, 0])
    
    #we will shift the origin to coincide with the point given
    xyzArray = xyzArray-center
    
    #project vector onto xy plane    
    vecXy = np.array([vector[0],vector[1], 0.0])
    vecYz = np.array([0.0 ,np.linalg.norm(vecXy), vector[2]])
    
    if np.linalg.norm(vecXy)==0.0:
        return xyzArray

    vecYzNorm = vecYz * 1.0/(np.linalg.norm(vecYz))        
    vecXyNorm = vecXy* 1.0/(np.linalg.norm(vecXy))
    #vecYzNorm =  vecYz * 1.0/(np.linalg.norm(vecYz))

    theta1 = np.arccos(np.dot(vecXyNorm, yVec))
    theta2 = np.arccos(np.dot(vecYzNorm, zVec))

    cross1 = np.cross(vecXyNorm, yVec)
    cross2 = np.cross(vecYzNorm, zVec)

    if cross1[2] < 0:
        theta1 = 2*np.pi -theta1
    if cross2[0] < 0:
        theta2 = 2 *np.pi -theta2

    rotationMatrix =np.array([[np.cos(theta1), -np.sin(theta1), 0.0],
    [np.cos(theta2)*np.sin(theta1), np.cos(theta2)* np.cos(theta1), -np.sin(theta2)],
    [np.sin(theta2)*np.sin(theta1), np.sin(theta2)* np.cos(theta1), np.cos(theta2)]])

    for i in range(xyzArray.shape[0]):
        vecCurrent = xyzArray[i]
        vecNew = np.reshape(np.dot(rotationMatrix, np.reshape(vecCurrent, [3,1])), [3])
        xyzArray[i] = vecNew

    return xyzArray

    
def rotationTranslation(xyzArray, index1, index2):
    """
    a function for rotating and translating a molecule so that a given line
    segment between two atoms lies on the z axix
    """
    zVec = np.array([0, 0, 1.0])
    yVec = np.array([0, 1.0, 0])

    transVec = xyzArray[index1]

    xyzArray = xyzArray - transVec

    vec = xyzArray[index2]

    vecXy = np.array([vec[0],vec[1], 0.0])
    vecYz = np.array([0.0 ,np.linalg.norm(vecXy), vec[2]])
    #vecYz = np.array([0.0, np.linalg.norm(vecXy), vec[2]])
    
    if np.linalg.norm(vecXy)==0.0:
        return xyzArray

    vecYzNorm = vecYz * 1.0/(np.linalg.norm(vecYz))        
    vecXyNorm = vecXy* 1.0/(np.linalg.norm(vecXy))
    #vecYzNorm =  vecYz * 1.0/(np.linalg.norm(vecYz))

    theta1 = np.arccos(np.dot(vecXyNorm, yVec))
    theta2 = np.arccos(np.dot(vecYzNorm, zVec))

    cross1 = np.cross(vecXyNorm, yVec)
    cross2 = np.cross(vecYzNorm, zVec)

    if cross1[2] < 0:
        theta1 = 2*np.pi -theta1
    if cross2[0] < 0:
        theta2 = 2 *np.pi -theta2

    rotationMatrix =np.array([[np.cos(theta1), -np.sin(theta1), 0.0],
    [np.cos(theta2)*np.sin(theta1), np.cos(theta2)* np.cos(theta1), -np.sin(theta2)],
    [np.sin(theta2)*np.sin(theta1), np.sin(theta2)* np.cos(theta1), np.cos(theta2)]])

    for i in range(xyzArray.shape[0]):
        vecCurrent = xyzArray[i]
        vecNew = np.reshape(np.dot(rotationMatrix, np.reshape(vecCurrent, [3,1])), [3])
        xyzArray[i] = vecNew

    return xyzArray


def piezoBox(positionArray):
    """ 
    we are going to try to make a rectilinear box in cartesian coordinates for our system
    which we will then use to get the full piezotensor
    """
    
    size = np.shape(positionArray)[0]
    
    bounds = np.zeros((size, size, 3))
    
    for i in range(size):
        for j in range(i +1,size):
            for k in range(3):
                bound0 = positionArray[i,k]
                bound1 = positionArray[j,k]
                
                if bound1 >= bound0:
                    bound = bound1 - bound0
                    bounds[j,i,k] = bound
                    
                elif bound0 > bound1:
                    bound = bound0-bound1
                    bounds[i,j,k]= bound
    
    boundsX = bounds[:,:,0]
    boundsY = bounds[:,:,1]
    boundsZ = bounds[:,:,2]
    
    xmax = np.amax(boundsX)
    ymax = np.amax(boundsY)
    zmax = np.amax(boundsZ)
    
    for i in range(size):
        for j in range(size):
            if boundsX[i,j] == xmax:
                xList = [i,j]
            if boundsY[i,j] == ymax:
                yList = [i,j]
            if boundsZ[i,j] == zmax:
                zList = [i,j]
                
#    print bounds, boundsX, boundsY, boundsZ, xList, yList, zList
#    print xmax, ymax, zmax
    
    return xmax, ymax, zmax, xList, yList, zList
                
    

def directionSelection(normalModeArray, positionArray):
    """for the measurement of the piezocoefficient it is necesary to pick boundaries from
    which to make measurements. We here will choose two atoms.  One way to do this is to 
    pick the normal mode corresponding to the the lowest energy vibration.  We then can analyze
    the biggest percent change in the distance vector for every pair of atoms along the normal mode
    and choose these two atoms as our indexes"""
    
    strainList = []
    indexList = []
    size = np.shape(positionArray)[0]
    for i in range(size):
        for j in range(i + 1, size):
            dx1 = normalModeArray[3*i:3*i+3]
            dx2 = normalModeArray[3*j:3*j+3]
            x1 = positionArray[i,:]
            x2 = positionArray[j,:]
            
            dr = dx2-dx1
            r = x2-x1
            
            drLen = np.linalg.norm(dr)
            
            drOverR = np.linalg.norm(dr)/np.abs(np.dot(r,dr)/drLen)
            
            strainList.append(drOverR)
            indexList.append([i,j])

    zippy = zip(strainList,indexList)
    
    print zippy
    
    zippy = sorted(zippy, key=lambda x: x[0])
    
    print zippy

def getPiezoComplete(dxdf, rVec, index1, index2):
    r0 = np.linalg.norm(rVec)
    e = rVec/r0
    print r0
    print e
    dx1df = dxdf[index1:index1 + 3, :]
    dx2df = dxdf[index2:index2+3 , :]
    
    dudf = dx1df-dx2df
    
    dudfdr = dudf/(r0* 1.88973*0.51422)
    print dudfdr
    
    eival,eivec = np.linalg.eigh(dudfdr)
    print "eigenvalues"
    print eival
    A = np.dot(np.transpose(dudfdr), dudfdr)
    b = np.dot(e,dudfdr)

    eigval, eigvec = np.linalg.eigh(A)
    
    piezoOpt = np.sqrt(eigval)
    print piezoOpt
#    lambdas = iterativePiezoSolver(A,b)
    
#    print lambdas
    
#    dz21df = dxdf[index1, 2]- dxdf[index2,2]
#    
#    piezo = abs(dz21df)/(r0*1.88973)*1.0/(0.514220652)

    
    return b
    

    
    
def calculateD33Automatic(inputFile, df, index1, index2, cores = 4, timeout = 96):
    """
    a function for calculating piezo coefficients.
    this should handle intitial geometry optimizations
    and subsequent frequency calculation.
    param inputFile: the  initial qchem input file
    param df:  the magnitude of the field to be applied in V/nm
    param index1:  one of the indexes corresponding to the pair of atoms
        participating in the hydrogen bond.
    parma index2: the other index of the atom corresponding to the pair of
        atoms in the hydrogen bond.
    """
    stub, ext = os.path.splitext(inputFile)
    
    df = df/514.220652
    
    print df 
    
    dxdf, inFile = getDxdf(inputFile, df,index1, index2, cores, timeout)
    
    index1Exp = 3*index1 +2
    index2Exp = 3*index2 +2

    r0 = inFile.interatomic_distance([index1, index2])
    
    piezo = getPiezoD33(dxdf, r0, index1Exp, index2Exp)

    handle = open(stub + '.d33', 'w')
    handle.write(str(piezo))
    handle.close()


def calculateD33AutomaticComplete(inputFile, index1, index2, cores = 4, timeout = 96):
    
    stub, ext = os.path.splitext(inputFile)
    
    
    
    dxdf, inFile = getDxdf(inputFile, index1, index2, cores, timeout)
    
    index1Exp = 3*index1 
    index2Exp = 3*index2 

    rvec = inFile.interatomic_distance_vector([index1, index2])
    
    piezo = getPiezoComplete(dxdf, rvec, index1Exp, index2Exp)
#    piezo2 = getPiezoComplete(dxdf2, rvec, index1Exp, index2Exp)

    handle = open(stub + '.d33', 'w')
    handle.write(str(piezo) + '\n')
#    handle.write(str(piezo2))
    handle.close()
    
def getPiezoCOM(dxdf, atomList1, atomList2, inFile):
    """for calculating the piezoelectric matrix for two points coinciding with two center of masses"""

    com1=inFile.getSubCenterOfMass(atomList1)
    com2=inFile.getSubCenterOfMass(atomList2)    
    
    rVec= com2-com1
    rdist = np.linalg.norm(rVec)
    
    atomList1Mat=np.zeros((3,3))
    atomList2Mat=np.zeros((3,3))
    
    totMass1 = 0.0
    totMass2 = 0.0
    
    for index in atomList1:
        atomList1Mat += dxdf[3*index:3*index+3,:]*inFile.masses[index]
        totMass1     += inFile.masses[index]
        
    for index in atomList2:
        atomList2Mat += dxdf[3*index:3*index+3,:]*inFile.masses[index]
        totMass2     += inFile.masses[index]
        
    com1Mat = atomList1Mat/totMass1
    com2Mat = atomList2Mat/totMass2
    
    dudfdr = (com2Mat-com1Mat)/(rdist* 1.88973*0.51422)
    
    return dudfdr
    
    
def calculatePiezoMatrixCOM(inputFile, atomList1, atomList2, cores=4, timeout=96):
    stub, ext= os.path.splitext(inputFile)
    
    dxdf, inFile = getDxdfCOM(inputFile, atomList1, atomList2, cores=4, timeout=96)
    
    piezoMatCOM = getPiezoCOM(dxdf, atomList1, atomList2, inFile)
    
    handle = open(stub + '.Mat', 'w')
    handle.write(str(piezoMatCOM) + '\n')
#    handle.write(str(piezo2))
    handle.close()
    

def Analysis(inputFile, df0, index1, index2, cores = 4, timeout = 96, fieldNum=6):
    
    '''
    fieldNum: Number of field values which will be used to perform optimizations about zero field optimized structure; should be even
    df: in V/nm
    '''
    df = df0/514.220652
    
    #check fieldNum is even 
    if fieldNum%2 != 0:
        fieldNum = fieldNum +1

    #split the input filename
    stub, ext = os.path.splitext(inputFile)

    #establish other useful file names to be used for the calculation
    stubPiezo = stub + 'PiezoActual'
    stubPiezoFieldX = stub + 'PiezoActualFieldX'
    stubPiezoFieldY = stub + 'PiezoActualFieldY'
    stubPiezoFieldZ = stub + 'PiezoActualFieldZ'
    
    submitList = []
    outList = []
    outXList = []
    outYList = []
    outZList = []
    
    for i in range(1, fieldNum+1):
        submitList.append(stubPiezoFieldX + str(i))
        submitList.append(stubPiezoFieldY + str(i))
        submitList.append(stubPiezoFieldZ + str(i))
        
    for i in range(1,fieldNum+1):
        outList.append(stubPiezoFieldX +str(i) + '.out')
        outList.append(stubPiezoFieldY +str(i) + '.out')
        outList.append(stubPiezoFieldZ +str(i) + '.out')
        
    for i in range(1, fieldNum+1):
        outXList.append(stubPiezoFieldX +str(i) + '.out')
        outYList.append(stubPiezoFieldY +str(i) + '.out')
        outZList.append(stubPiezoFieldZ +str(i) + '.out')
        
    

    #use qchem parser class to format optimization file
    inFile = pyqchem.QChemInputParser(inputFile)

    inFile.rem['jobtype'] = 'opt'
    
    inFile.fields['x'] = 0.000
    inFile.fields['y'] = 0.000
    inFile.fields['z'] = 0.000
    
    inFile.save(stubPiezo + '.n')

    #run optimization
    runAndCheckLoneJob(stubPiezo, cores=cores, timing=timeout)


    outFile = pyqchem.QChemOutputParser(stubPiezo + '.out')
    outFile.optimizedGeometry(stubPiezo + '.xyz')

    # need to rotate and translate system so that two atoms of interest lie parallel to the z-axis
    xyz = pyqchem.xyzParser(stubPiezo + '.xyz')

    newPositions = rotationTranslation(xyz.positions, index1, index2)
    
    # set new positions to that of translated and rotated system
    inFile.setPositions(newPositions)
    
    r0 = inFile.interatomic_distance([index1, index2])
    
    for i in range(1, fieldNum + 1):
        inFile.fields['x'] = (fieldNum/2.0 -1.0/2.0) *-df + df*(i-1)
        inFile.fields['y'] = 0.000
        inFile.fields['z'] = 0.000
        inFile.save(stubPiezoFieldX + str(i)+'.n')
        inFile.fields.clear()
        
        inFile.fields['x'] = 0.000
        inFile.fields['y'] = (fieldNum/2.0 -1.0/2.0) *-df + df*(i-1)
        inFile.fields['z'] = 0.000
        inFile.save(stubPiezoFieldY + str(i)+'.n')
        inFile.fields.clear()
        
        inFile.fields['x'] = 0.000
        inFile.fields['y'] = 0.000
        inFile.fields['z'] = (fieldNum/2.0 -1.0/2.0) *-df + df*(i-1)
        inFile.save(stubPiezoFieldZ + str(i)+'.n')
        inFile.fields.clear()

    #run list of jobs
    runAndCheckJobGroup(submitList, cores=cores, timing=timeout)
            
    ####### Now we extract all of our information ######
    ###### We need 9 differenct plots dui/dxj ######
            
    for i, job in enumerate(outList):
        outFile = pyqchem.QChemOutputParser(job)
        outFile.optimizedGeometry( submitList[i] + '.xyz')
        
    vec0 = inFile.interatomic_distance_vector([index1,index2])
    
    ux0 = vec0[0]
    uy0 = vec0[1]
    uz0 = vec0[2]
    
    
    ### make files with plot info and keep infor in lists####
    fList   =[]
    xfxList =[]
    yfxList =[]
    zfxList =[]
    xfyList =[]
    yfyList =[]
    zfyList =[]
    xfzList =[]
    yfzList =[]
    zfzList =[]
    
    for i in range(1, fieldNum + 1):
        fList.append((fieldNum/2.0 -1.0/2.0) *-df0 + df0*(i-1))

    
    ##### field x direction #####
    
    handle = open('XFx','w')
    handle1 = open('YFx','w')
    handle2 = open('ZFx', 'w')    
    
    

    sx = ''
    sy = ''
    sz = ''
    
    for i in range(1, fieldNum+1):
        xyzFile = pyqchem.xyzParser(stubPiezoFieldX + str(i) + '.xyz')
        vec = xyzFile.interatomic_distance_vector([index1, index2])
        dux = vec[0] -ux0
        duy = vec[1] -uy0
        duz = vec[2] -uz0
        sx += '%10.8f %10.8f\n' % ((fieldNum/2.0 -1.0/2.0) *-df0 + df0*(i-1), dux)
        sy += '%10.8f %10.8f\n' % ((fieldNum/2.0 -1.0/2.0) *-df0 + df0*(i-1), duy)
        sz += '%10.8f %10.8f\n' % ((fieldNum/2.0 -1.0/2.0) *-df0 + df0*(i-1), duz)
        xfxList.append(dux)
        yfxList.append(duy)
        zfxList.append(duz)
        
    handle.write(sx)
    handle1.write(sy)
    handle2.write(sz)
    handle.close()
    handle1.close()
    handle2.close()
    
#### y field
    
    handle = open('XFy','w')
    handle1 = open('YFy','w')
    handle2 = open('ZFy', 'w')    
    
    

    sx = ''
    sy = ''
    sz = ''
    
    for i in range(1, fieldNum+1):
        xyzFile = pyqchem.xyzParser(stubPiezoFieldY + str(i) + '.xyz')
        vec = xyzFile.interatomic_distance_vector([index1, index2])
        dux = vec[0] -ux0
        duy = vec[1] -uy0
        duz = vec[2] -uz0
        sx += '%10.8f %10.8f\n' % ((fieldNum/2.0 -1.0/2.0) *-df0 + df0*(i-1), dux)
        sy += '%10.8f %10.8f\n' % ((fieldNum/2.0 -1.0/2.0) *-df0 + df0*(i-1), duy)
        sz += '%10.8f %10.8f\n' % ((fieldNum/2.0 -1.0/2.0) *-df0 + df0*(i-1), duz)
        xfyList.append(dux)
        yfyList.append(duy)
        zfyList.append(duz)
        
    handle.write(sx)
    handle1.write(sy)
    handle2.write(sz)
    handle.close()
    handle1.close()
    handle2.close()

### z field 
    handle = open('XFz','w')
    handle1 = open('YFz','w')
    handle2 = open('ZFz', 'w')    
    
    
    sx = ''
    sy = ''
    sz = ''
    
    for i in range(1, fieldNum+1):
        xyzFile = pyqchem.xyzParser(stubPiezoFieldZ + str(i) + '.xyz')
        vec = xyzFile.interatomic_distance_vector([index1, index2])
        dux = vec[0] -ux0
        duy = vec[1] -uy0
        duz = vec[2] -uz0
        sx += '%10.8f %10.8f\n' % ((fieldNum/2.0 -1.0/2.0) *-df0 + df0*(i-1), dux)
        sy += '%10.8f %10.8f\n' % ((fieldNum/2.0 -1.0/2.0) *-df0 + df0*(i-1), duy)
        sz += '%10.8f %10.8f\n' % ((fieldNum/2.0 -1.0/2.0) *-df0 + df0*(i-1), duz)
        xfzList.append(dux)
        yfzList.append(duy)
        zfzList.append(duz)
        
    handle.write(sx)
    handle1.write(sy)
    handle2.write(sz)
    handle.close()
    handle1.close()
    handle2.close()
        
#### Make geometry videos ######
        
    pyqchem.utils.vmdVideoFromQchemOutfiles('xFieldVideo.xyz', outXList)
    pyqchem.utils.vmdVideoFromQchemOutfiles('yFieldVideo.xyz', outYList)
    pyqchem.utils.vmdVideoFromQchemOutfiles('zFieldVideo.xyz', outZList)
        

    
    f, ((ax1,ax2,ax3),(ax4,ax5,ax6),(ax7, ax8,ax9)) = plt.subplots(3,3, sharex=True, sharey=True )
    p = np.polyfit(fList, xfxList, 1)
    z = np.poly1d(p)
    ax1.scatter(fList, xfxList)
    ax1.plot(fList, z(fList), 'r-')
    ax1.set_title('dx/dfx')
    ax1.text(0, 0, ((p[0]/r0) * 1000), fontsize=10)
    

    ax2.scatter(fList, xfyList)
    ax2.set_title('dx/dfy')
    p = np.polyfit(fList, xfyList, 1)
    z = np.poly1d(p)
    ax2.plot(fList, z(fList), 'r-')
    ax2.text(0, 0, ((p[0]/r0) * 1000), fontsize=10)
    
    ax3.scatter(fList, xfzList)
    ax3.set_title('dx/dfz')
    p = np.polyfit(fList, xfzList, 1)
    z = np.poly1d(p)
    ax3.plot(fList, z(fList), 'r-')
    ax3.text(0, 0, ((p[0]/r0) * 1000), fontsize=10)

    ax4.scatter(fList, yfxList)
    ax4.set_title('dy/dfx')
    p = np.polyfit(fList, yfxList, 1)
    z = np.poly1d(p)
    ax4.plot(fList, z(fList), 'r-')
    ax4.text(0, 0, ((p[0]/r0) * 1000), fontsize=10)
    
    ax5.scatter(fList, yfyList)
    ax5.set_title('dy/dfy')
    p = np.polyfit(fList, yfyList, 1)
    z = np.poly1d(p)
    ax5.plot(fList, z(fList), 'r-')
    ax5.text(0, 0, ((p[0]/r0) * 1000), fontsize=10)
    
    ax6.scatter(fList, yfzList)
    ax6.set_title('dy/dfz')
    p = np.polyfit(fList, yfzList, 1)
    z = np.poly1d(p)
    ax6.plot(fList, z(fList), 'r-')
    ax6.text(0, 0, ((p[0]/r0) * 1000), fontsize=10)
    
    ax7.scatter(fList, zfxList)
    ax7.set_title('dz/dfx')
    p = np.polyfit(fList, zfxList, 1)
    z = np.poly1d(p)
    ax7.plot(fList, z(fList), 'r-')
    ax7.text(0, 0, ((p[0]/r0) * 1000), fontsize=10)
    
    ax8.scatter(fList, zfyList)
    ax8.set_title('dz/dfy')
    p = np.polyfit(fList, zfyList, 1)
    z = np.poly1d(p)
    ax8.plot(fList, z(fList), 'r-')
    ax8.text(0, 0, ((p[0]/r0) * 1000), fontsize=10)
    
    ax9.scatter(fList, zfzList)
    ax9.set_title('dz/dfz')
    p = np.polyfit(fList, zfzList, 1)
    z = np.poly1d(p)
    ax9.plot(fList, z(fList), 'r-')
    ax9.text(0, 0, ((p[0]/r0) * 1000), fontsize=10)
    
    plt.show()
    
    return 0

def gradientAnalysis(inputFile, df0, index1, index2, cores = 4, timeout = 96, fieldNum=6):
        
    df = df0/514.220652
    
    #check fieldNum is even 
    if fieldNum%2 != 0:
        fieldNum = fieldNum +1

    #split the input filename
    stub, ext = os.path.splitext(inputFile)

    #establish other useful file names to be used for the calculation
    stubPiezo = stub + 'PiezoGrad'
    stubPiezoField = stub + 'PiezoField'
    stubPiezoFieldX = stub + 'PiezoGradlFieldX'
    stubPiezoFieldY = stub + 'PiezoGradFieldY'
    stubPiezoFieldZ = stub + 'PiezoGradFieldZ'
    
    submitList = []
    gradFXList = []
    gradFYList = []
    gradFZList = []
    outList = []
    
    for i in range(1, fieldNum+1):
        submitList.append(stubPiezoFieldX + str(i))
        submitList.append(stubPiezoFieldY + str(i))
        submitList.append(stubPiezoFieldZ + str(i))
        
    for i in range(1,fieldNum+1):
        outList.append(stubPiezoFieldX +str(i) + '.out')
        outList.append(stubPiezoFieldY +str(i) + '.out')
        outList.append(stubPiezoFieldZ +str(i) + '.out')
        
    for i in range(1, fieldNum+1):
        gradFXList.append(stubPiezoFieldX + str(i)+ '/GRAD')
        gradFYList.append(stubPiezoFieldY + str(i)+ '/GRAD')
        gradFZList.append(stubPiezoFieldZ + str(i)+ '/GRAD')
        

    #use qchem parser class to format optimization file
    inFile = pyqchem.QChemInputParser(inputFile)

    inFile.rem['jobtype'] = 'opt'
    
    inFile.fields['x'] = 0.000
    inFile.fields['y'] = 0.000
    inFile.fields['z'] = 0.000
    
    inFile.save(stubPiezo + '.n')

    #run optimization
    runAndCheckLoneJob(stubPiezo, cores=cores, timing=timeout)
    
    outFile = pyqchem.QChemOutputParser(stubPiezo + '.out')
    outFile.optimizedGeometry(stubPiezo + '.xyz')

    # need to rotate and translate system so that two atoms of interest lie parallel to the z-axis
    xyz = pyqchem.xyzParser(stubPiezo + '.xyz')

    newPositions = rotationTranslation(xyz.positions, index1, index2)
    
    # set new positions to that of translated and rotated system
    inFile.setPositions(newPositions)
    
    inFile.rem['jobtype']= 'force'
    
    inFile.save(stubPiezoField + '.n')
    
    runAndCheckLoneJob(stubPiezoField, cores=cores, timing=timeout, command='Gradient')
    
    for i in range(1, fieldNum/2+1):
        inFile.fields['x']    = df*i 
        inFile.fields['y']    = 0.000
        inFile.fields['z']    = 0.000
        inFile.save(stubPiezoFieldX + str(i)+'.n')
        inFile.fields.clear()
        
        inFile.fields['x']    = -df*i
        inFile.fields['y']    = 0.000
        inFile.fields['z']    = 0.000
        inFile.save(stubPiezoFieldX + str(i+fieldNum/2)+'.n')
        inFile.fields.clear()

        inFile.fields['x']   = 0.000     
        inFile.fields['y']   =  df*i 
        inFile.fields['z']   = 0.000
        inFile.save(stubPiezoFieldY + str(i)+'.n')
        inFile.fields.clear()
        
        inFile.fields['x']   = 0.000     
        inFile.fields['y']   = -df*i 
        inFile.fields['z']   = 0.000
        inFile.save(stubPiezoFieldY + str(i+fieldNum/2)+'.n')
        inFile.fields.clear()


        inFile.fields['x']  = 0.000
        inFile.fields['z']  =  df*i
        inFile.fields['y']  = 0.000
        inFile.save(stubPiezoFieldZ + str(i)+'.n')
        inFile.fields.clear()
        
        inFile.fields['x']  = 0.000
        inFile.fields['z']  = -df*i
        inFile.fields['y']  = 0.000
        inFile.save(stubPiezoFieldZ + str(i+fieldNum/2)+'.n')
        inFile.fields.clear()
        
    runAndCheckJobGroup(submitList, timing=timeout, command='Gradient')
    
    size = inFile.size *3
    
    coord=random.sample(set(range(size)), 1)
    
    gradValListFx = []
    gradValListFy = []
    gradValListFz = []
    fieldValListX  = []
    fieldValListY  = []
    fieldValListZ  = []
    
    fieldValListX.append(0.000)
    fieldValListY.append(0.000)
    fieldValListZ.append(0.000)
    
    handle = pyqchem.gradient.Gradient(stubPiezoField + '/GRAD')
    gradValListFx.append(float(handle.gradient[coord]))
    gradValListFy.append(float(handle.gradient[coord]))
    gradValListFz.append(float(handle.gradient[coord]))
    
            
    for i, job in enumerate(gradFXList):
         infile = pyqchem.qchem_parser.QChemInputParser(stubPiezoFieldX + str(i+1) +'.n')
         handle = pyqchem.gradient.Gradient(job)
         fieldValListX.append(infile.fields['x']*514.220652)
         gradValListFx.append(float(handle.gradient[coord]))
         
    for i, job in enumerate(gradFYList):
         infile = pyqchem.qchem_parser.QChemInputParser(stubPiezoFieldY + str(i+1) +'.n')
         handle = pyqchem.gradient.Gradient(job)
         fieldValListY.append(infile.fields['y']*514.220652)
         gradValListFy.append(float(handle.gradient[coord]))
         
    for i, job in enumerate(gradFZList):
         infile = pyqchem.qchem_parser.QChemInputParser(stubPiezoFieldZ + str(i+1) +'.n')
         handle = pyqchem.gradient.Gradient(job)
         fieldValListZ.append(infile.fields['z']*514.220652)
         gradValListFz.append(float(handle.gradient[coord]))
    
    
    f, ((ax1),(ax2),(ax3)) = plt.subplots(1,3, sharex=True, sharey=True )
    print gradValListFx
    print fieldValListX
    p = np.polyfit(fieldValListX, gradValListFx, 1)
    z = np.poly1d(p)
    ax1.scatter(fieldValListX,  gradValListFx)
    ax1.plot(fieldValListX, z(fieldValListX), 'r-')
    ax1.set_title('gradient coordinate #' + str(coord)+ ' vs. field in x direction')
    ax1.text(0, 0, (p[0]), fontsize=10)
        
    p = np.polyfit(fieldValListY, gradValListFy, 1)
    z = np.poly1d(p)
    ax2.scatter(fieldValListY,  gradValListFy)
    ax2.plot(fieldValListY, z(fieldValListY), 'r-')
    ax2.set_title('gradient coordinate #' + str(coord)+ ' vs. field in y direction')
    ax2.text(0, 0, (p[0]), fontsize=10)
    
    p = np.polyfit(fieldValListZ, gradValListFz, 1)
    z = np.poly1d(p)
    ax3.scatter(fieldValListZ,  gradValListFz)
    ax3.plot(fieldValListZ, z(fieldValListZ), 'r-')
    ax3.set_title('gradient coordinate #' + str(coord)+ ' vs. field in z direction')
    ax3.text(0, 0, (p[0]), fontsize=10)
    
    plt.show()
           
    return 0
    
    
    

def runAndCheckLoneJob(stub, cores = 4, timing = 96, command='Normal'):
    """inputFile: initial qchem input file
       Stub:  The root name from which the new input and output file will be created
       Note: assumes file already exists
    """
    if command=='Normal':
        command0= r'submit {stub} {cores:d} {timing:d}'
    elif command=='Gradient' or command=='Hessian':
        command0= r'submit_hessian {stub} {cores:d} {timing:d}'
    else:
        raise RuntimeError('Command type is not recgonised')
    #we need to make sure the job has not already been completed
    try:
        handle = open(stub + '.out', 'r' )
        if pyqchem.utils.checkJobFinished(stub + '.out')== True:
            pass
    except:
        subprocess.call(command0.format(stub=stub, cores=cores, timing=timing).split(),0)

    while True:
        try:
            handle = open(stub + '.out', 'r')
            handle.close()
            break
        except:
            time.sleep(100)
            continue

    counter = 0
    while pyqchem.utils.checkJobFinished(stub + '.out')== False:
        time.sleep(300) #sleep for 5 minutes
        counter = counter + 300
        if counter >= timing * 3600:
            raise RuntimeError('job '+ stub + ' did not finish in alotted time')
            
    return 
    
def runAndCheckJobGroup(stubList, cores=4, timing = 96, command='Normal'):
    
    if command=='Normal':
        command0= r'submit {stub} {cores:d} {timing:d}'
    elif command=='Gradient' or command=='Hessian':
        command0= r'submit_hessian {stub} {cores:d} {timing:d}'
    else:
        raise RuntimeError('Command type is not recgonised')
    
    
    for i, job in enumerate(stubList):
            #we need to make sure the job has not already been completed
        try:
            handle = open(job + '.out', 'r' )
            if pyqchem.utils.checkJobFinished(job+ '.out')== True:
                pass
        except:
            subprocess.call(command0.format(stub=stubList[i], cores=cores, timing=timing).split(),0)
            
            # check to see if the outfiles exist yet
    for job in stubList:  
        while True:
            try:
                handle = open(job + '.out', 'r' )
                handle.close()
                break
            except:
                time.sleep(100)
                continue
    
    counter2 = 0      
    for job in stubList: 
        while pyqchem.utils.checkJobFinished(job+ '.out')==False:         
            if counter2 >= timing*3600:
                raise RuntimeError('calculation for ' + job + ' has exceeded time limit')
                break  
            time.sleep(300)
            counter2 = counter2+300
            
    return
    
#def formSansTransRotMatrix(centerOfMass, positions, momentInertiaTensor )

                    
#    subprocess.call(('submit',stubPiezo, cores, timeout), 0)
        


#def example_1():
#    print work
#
#    root, dirs, files = os.walk(work).next()
#
#    print root
#    print dirs
#    print files
#
#    for f in files:
#        print os.path.join(root, f)
#
#    for d in dirs:
#        print os.path.join(root, d)
#
#class Keith(object):
#    _lastname = 'Werling'
#
#    def __init__(self):
#        pass
#
#    @property
#    def lastname(self):
#        return self._lastname
#
#    @lastname.setter
#    def lastname(self, value):
#        print 'really!?'
#        self._lastname = value
#
#def my_decor(func):
#    def blarg():
#        print 'doing stuff'
#        func()
#    return blarg()
#
#@my_decor
#def test():
#    print 'done'
#
#
#if __name__ == '__main__':
##    work = os.getcwd()
##
##    found = []
##    for root, dirs, files in os.walk(work):
##        for f in files:
##            stub, ext = os.path.splitext(f)
##            if ext == '.sample':
##                f = os.path.join(root, f)
##                found.append(f)
##
##    print 'I found %d files!' % len(found)
##    for f in found:
##        print f
#
#    keith = Keith()
#    print keith.lastname
#    keith.lastname = 'Dumb'
#    print keith.lastname
