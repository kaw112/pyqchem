import numpy as np
import pyqchem
import sys


class Gradient:
    """
    A class to contain important
    information from a gradient output file from qchem
    """
    def __init__(self, file_name):
        self.reset()
        self.load(file_name)
        self.buildGradient()

    def load(self, file_name):
        """
        load the file and set important
        parameters
        """

        self.file = file_name
        
        handle = open(self.file, 'r')
        line = handle.readline()
        while line:
            line = line.strip()
            if line:
                self.lines.append(line)
            line = handle.readline()
        for i in range(2):
            self.lines
        
        tokens = self.lines[1].strip().split()
        self.energy = float(tokens[0])
        
        for line in self.lines[3:]:
            tokens = line.strip().split()
            if line =='$end':
                break
            for token in tokens:
                self.gradientValues.append(float(token))
            
        self.dimension = len(self.gradientValues)

    def reset(self):
        """
        resets class parameters
        """
        
        self.file = None
        self.lines = []
        self.dimension = 0
        self.gradient = [] # will be a column array
        self.energy = 0
        self.gradientValues = []
        
    def buildGradient(self):
        """
        guilds gradient from gradientValues
        """
        self.gradient = np.zeros((self.dimension, 1))
        for i, value in enumerate(self.gradientValues):
            self.gradient[i] = value
            
            
            
            