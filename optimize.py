# -*- coding: utf-8 -*-
import numpy as np
import pyqchem
import os
import time

def rotationsForDan(positions):
    pos = positions[0:30,:]
    
    gcom = np.zeros(3)
    
    for i in range(30):
        gcom += pos[i,:]
        
    gcom /= 30.0
    
    relPos = np.zeros((30,3))
    
    for i in range(30):
        relPos[i,:]= pos[i,:] - gcom

    rotTens = np.zeros((3,3))

    #first the diagonal
    for k in range(30):
        rotTens[0,0] += (relPos[k,1]**2 + relPos[k,2]**2)
        rotTens[1,1] += (relPos[k,0]**2 + relPos[k,2]**2)
        rotTens[2,2] += (relPos[k,0]**2 + relPos[k,1]**2)

    #now ze off-diagonals
    for k in range(30):
        for i in range(3):
            for j in range(i+1,3):
                rotTens[i,j]+= -relPos[k,i]*relPos[k,j]
                rotTens[j,i]= rotTens[i,j]

    ###rotation Matrix###
    eigval, eigvec = np.linalg.eigh(rotTens)
    
    R = np.zeros((3, 105))
    
    for i in range(30):
        for j in range(np.shape(eigvec)[1]):
            velocity = np.cross(eigvec[:,j], relPos[i,:])
            R[j,i*3:i*3+3] = velocity

        ###normalize the vectors###

    for i in range(np.shape(eigvec)[1]):
        R[i,:] = R[i,:]/np.linalg.norm(R[i,:])

    return R.transpose()

def runFrankOptimization():
    pass
class QchemOptimizer:
    """ used to tackle optimizations """
    
    def __init__(self, inputFile, method= 'bfgs', rotTrans = True, linesearch= None, maxCycles = 900, gradTol = 1.5e-6, dispTol = 2e-5, energyTol = 1e-7, ppn = 4, spTiming = 96, removeMotionFuncList = [] ):
        self.reset()
        self.stub, self.ext = os.path.splitext(inputFile)
        self.stubEnergy = self.stub + 'Energy'
        self.inputFile = inputFile
        self.inputFileEnergy = self.stubEnergy + self.ext
        self.stepType = method
        self.linesearchType = linesearch
        self.gradTol = gradTol
        self.dispTol = dispTol
        self.energyTol = energyTol
        self.maxCycles = maxCycles
        self.removeRotTrans = rotTrans
        self.removeList = removeMotionFuncList
        self.ppn = ppn
        self.spTiming = spTiming
        self.read()
        
    def read(self):
        self.system = pyqchem.qchem_parser.QChemInputParser(self.inputFile)   
    def reset(self):
        
        ######## stub string stuff ##############
        #string inputfile stub
        self.stub = None
        
        # string inputfile stub with energy tag
        self.stubEnergy = None
        
        # string for input file name
        self.inputFile = None
        
        # string for energy input file name
        self.inputFileEnergy =None
        
        ######### qchem file obj stuff ############
        # qchem input parser object.  Used to constantly change input infor to run new calculations. always store current
        self.system = None   
        # list of file objects in which to run gradient or energy calculations
        self.gradCalcList = []
        
        # list of output file objects for gradient calcualtions
        self.gradCalcOutList = []
        
        
        
        #### system attribute lists (ie. energy, gradient, etc)##########
        
        # contains geometries from step to step (does not include line search extras)
        self.geometryList= []
        
        self.gradientList=[]
        
        self.energyList = []
  
        # list of absolute value of maximum displacement
        self.absDispList=[]      
        
        # list for changes in energy
        self.energyChangeList=[]
        
        # list of of absolute value of gradient max 
        self.absGradList = []
        
        ###### step and linesearch algorithm type strings#############        
        self.linesearchType = None
        
        self.stepType = None
        

        ##### tolerances for convergence criteria ############

        # gradient(absolute value of max component), energy (absolute value of change in energy), and displacement(absolute value of max component) tolerances
        self.gradTol = None
        
        self.energyTol = None
        
        self.dispTol = None   

        # number of maximum cycles
        self.maxCycles = 0

        self.cycleCounter = 0
        
        self.stepSizeTrustRadius =0.1
        
        
        ##### Lists pertaining to linesearch #####
        self.energyLinesearchList = []
        
        self.geometryLinesearchList = []
        
        # type of line search
        self.lineSearchType = None
        
        # inverse curvature approximation for bfgs like linesearch
        self.bfgsLinesearchInvCurvatureList = []
        

        #### step direction lists (type specific)######################

        self.bfgsInvHessianList = []
        
        self.rotTransLessMatList = []
        
        
        #### stuff concerning projecting out certain movements#######
        self.removeRotTrans = True
        
        """ removeList contains added functions to calculate motions at 
        a point to add to the space of rotations and translations for removal """
        self.removeList = []
        
        ### list of modes after calculating them ####
        self.otherModes = []
        
        self.ppn = 0
        
        self.spTiming = 0
        
    def main(self):
        
        #first add first geometry to list
        self.geometryList.append(self.system.positions.flatten())
        
        # run calculation of energy and gradient and store the objects in respective list       
        gradSystem, gradOutSystem, energy, gradient = self.gradientCalculation(self.stubEnergy +'.in', self.system.positions)
        self.gradCalcList.append(gradSystem)   
        self.gradCalcOutList.append(gradOutSystem)
        self.energyList.append(energy)
        self.gradientList.append(gradient*1.889727)
              
        #run gradient calculation, find line search direction, minimize along linesearch, check convergence, repeat 
        for i in range(self.maxCycles):
            #determination of step direction from latest point (bfgs, etc)
            step = self.stepDirection()
            print "step is \n"
            print step
            #linesearch with current step direction (find the next point)
            gradSystem, gradOutSystem, energy, gradient = self.lineSearch(step)
            self.gradCalcList.append(gradSystem)   
            self.gradCalcOutList.append(gradOutSystem)
            self.energyList.append(energy)
            self.gradientList.append(gradient*1.889727)
            self.geometryList.append(gradSystem.positions.flatten())
            self.appendAnimationFile()
            #### print some stuff for debugging ####

            self.cycleCounter = self.cycleCounter +1
            if self.convergenceCheck()==True:
                self.debug()
                break
            elif self.convergenceCheck() ==False:
                self.debug()
                continue
           
                
        if self.cycleCounter==self.maxCycles and self.convergenceCheck()==False:
            raise RuntimeError('Optimization Exceeded maximum number of cycles')
        elif self.convergenceCheck()==True:
            self.writeConvergedXYZFile()
            #self.writeAnimationFile()
            
            
            
            
    ############## gradient and energy calcualtion procedure ######################3    
    def gradientCalculation(self, fileName, positions):
        
        ### for the initial energy and gradient calculation
        gradSystem = self.gradientCalculationStart(fileName,positions)
        #self.gradCalcList.append(gradSystem)
        gradOutSystem = self.createOutputObject(gradSystem)
        #self.gradCalcOutList.append(gradOutsystem) 
#        energy = self.extractEnergy(gradOutSystem)
#        self.energyList.append(energy)
        while True:
            try:
                grad = pyqchem.gradient.Gradient(gradSystem.stub + '/GRAD')
                break
            except:
                time.sleep(30)
                continue
            
        energy = grad.energy
        gradient = grad.gradient
        return gradSystem, gradOutSystem, energy, gradient
        
    def gradientCalculationStart(self, fileName, positions):
        #calculates gradient (and or energy)
        gradSystem = self.createCalculationObject('force', fileName, positions)
 #       gradSystem.runJobInBackground()
 #       gradSystem.command = 'submit_grad'
        gradSystem.submitJob('submitH2PHessian', self.ppn, self.spTiming )
        return gradSystem
        
#    def gradientCalculationFinish(self, fileObj)
#        outFileObj = self.createOutputObject(fielObj)
#        return outFileObj
        
        
    def createCalculationObject(self, jobTypeString, fileToSave, positions):
        self.system.rem['jobtype']=jobTypeString
        self.system.rem['sym_ignore']='true'
        self.system.setPositions(positions)
        self.system.save(fileToSave)
        
        fileObj = pyqchem.qchem_parser.QChemInputParser(fileToSave)
        return fileObj
        
    def createOutputObject(self, inFileObj):
        inFileObj.checkJobStarted()
        inFileObj.checkJobFinished()
        outFileObj= pyqchem.qchem_parser.QChemOutputParser(inFileObj.stub + '.out')
        return outFileObj
     ######################################################################            sel############   
#    def energyCalculationSubmit(self):
#        self.system.rem['jobtype']='force'
#        self.system.rem['sym_ignore']='true'
#        self.system.save(self.inputFileEnergy)
#        
#        self.systemEnergy = pyqchem.qchem_parser.QChemInputParser(self.inputFileEnergy)
#        self.systemEnergy.submitJob()
        
#    def extractEnergy(self, outSystem):
#        energy = outSystem.getEnergy(self.energyStringToSearch)
#        return energy

    def stepDirection(self):
        if self.stepType == 'bfgs':
            step = self.stepBFGS()            
        if self.stepType == 'steepestDescent':
            step = self.stepDescend()
            
        return step
                
    def stepBFGS(self):
            
        # form invhessian
        if self.cycleCounter%10 == 0:
            tentativeInvHess = np.eye(self.system.size *3)
            if self.removeRotTrans == True:
                # calculate other modes
                self.calcOtherModes()
                # form transformation matrix
                transformMat = self.calculateTransformationMatrixRotTransLess(tentativeInvHess)
                self.rotTransLessMatList.append(transformMat)
                #calculate inverse hessian
                self.bfgsInvHessianList.append(np.linalg.inv(np.dot(transformMat.transpose(),np.dot(tentativeInvHess, transformMat))))
                #ps. I know all of this is ridiculous and that the inverse should just be a smaller identity matrix.  just curious i guess
            elif self.removeRotTrans == False: 
                self.bfgsInvHessianList.append(tentativeInvHess)
        else:
            if self.removeRotTrans == True:
                # calculate other modes
                self.calcOtherModes()
                #calculate new transformation matridisplacementx
                transformMat = self.calculateTransformationMatrixRotTransLess(np.dot(self.rotTransLessMatList[-1],np.dot(np.linalg.inv(self.bfgsInvHessianList[-1]),self.rotTransLessMatList[-1].transpose())))
                self.rotTransLessMatList.append(transformMat)
                #calculate inverse hessian
                newInvHessian = self.formInvHessianBFGSRotTrans()
                self.bfgsInvHessianList.append(newInvHessian)
            elif self.removeRotTrans == False:
                newInvHessian = self.formInvHessianBFGS()
                self.bfgsInvHessianList.append(newInvHessian)
                
        #take step then form new Hessian
        if self.removeRotTrans ==True:
            # transformation matrix
#            Vmat = self.rotTransLessMatList[0]
            Vmat = self.rotTransLessMatList[-1]
            Hinv = self.bfgsInvHessianList[-1]
            # something like V Hvv^-1 V^T g
            stepDirection = -np.dot(Vmat,(np.dot(Hinv, np.dot(Vmat.transpose(), self.gradientList[-1]))))
        elif self.removeRotTrans == False:
            stepDirection = -np.dot(self.bfgsInvHessianList[-1], self.gradientList[-1])
            
        return stepDirection.flatten()
        # run
        
    def formInvHessianBFGS(self):
        x = self.geometryList[-1]-self.geometryList[-2]
        g = self.gradientList[-1]-self.gradientList[-2]
        
        
        
        h =  self.bfgsInvHessianList[-1]

        xg = np.dot(x,g)
        hg = np.dot(h,g)
        ghg = np.dot(g,hg)
        u = x/xg-hg/ghg
        
        newInvHessian = h + np.outer(x,x)/xg-np.outer(hg,hg)/ghg+ghg*(np.outer(u,u))
        return newInvHessian
        
    def formInvHessianBFGSRotTrans(self):
        #form old hessian from previous inverse    
        h =  self.bfgsInvHessianList[-1]
        hNonInv = np.linalg.inv(h)
        
        #transform said hessian to cartesian space
        vOld=self.rotTransLessMatList[-2]
#        vOld = self.rotTransLessMatList[0]
        HessCart = np.dot(vOld, np.dot(hNonInv,vOld.transpose()))
        
        # calculate new basis to transform to
        vNew = self.rotTransLessMatList[-1]
#        vNew = self.rotTransLessMatList[0]
        #transform to new basis
        hNonInvNew = np.dot(vNew.transpose(),np.dot(HessCart,vNew))
        
        #form inverse in the new space
        hNew = np.linalg.inv(hNonInvNew)
        
        x = self.geometryList[-1]-self.geometryList[-2]
        g = self.gradientList[-1]-self.gradientList[-2]
        vx = np.dot(vNew.transpose(),x).flatten()
        vg = np.dot(vNew.transpose(),g).flatten()
        vxvg = np.dot(vx,vg)
        hg = np.dot(hNew,vg)
        ghg = np.dot(vg,hg)
        print 'ghg is \n'
        print ghg
        print 'h is \n'
        print hNew
        u = vx/vxvg-hg/ghg
        h1 = np.outer(vx,vx)/vxvg
        h2 = np.outer(hg,hg)/ghg
        h3 = ghg*(np.outer(u,u))
        
        print 'h1 is \n'
        print h1
        print 'h2 is \n'
        print h2
        print 'h3 is \n'
        print h3
        newInvHessian = hNew + h1-h2+h3
        return newInvHessian
        
    def calculateTransformationMatrixRotTransLess(self, hessian):
        #takes actual hessian, not inverse
        newModes = self.orthogonalizeModes()
        hess =pyqchem.hessian.HessianManual(hessian)
        hess.buildRotTransLessVecSpace(newModes)
        #hess.buildRotTransLessVecSpace(self.system.rotTransMat)
        return hess.vibBasis
        
    def updatePositions(self, positions):
        self.system.setPositions(np.reshape(positions, (self.system.size,3)))
    
    def reshapePositions(self, positions):
        positionsNew = np.reshape(positions, (self.system.size, 3))
        return positionsNew
    def stepDescend(self):
        # steepest descent routine
        return 0

    def lineSearch(self, step): 
        print "starting linesearch \n"
        if self.linesearchType == None:
#            if np.linalg.norm(step)< np.sqrt(self.system.size *3) *self.stepSizeTrustRadius:
#                positions = self.geometryList[-1]+step
            maxStep = np.amax(np.absolute(step))
            if maxStep <self.stepSizeTrustRadius:
                positions = self.geometryList[-1]+step
            else:
                scaleFactor = self.stepSizeTrustRadius/maxStep
                step = scaleFactor * step
                positions = self.geometryList[-1]+step
#            else:
#                scaleFactor = np.sqrt(self.system.size*3)* self.stepSizeTrustRadius/np.linalg.norm(step)
#                step = scaleFactor * step
#                positions = self.geometryList[-1]+step
                
            for i in range(3):
                gradSystem, gradOutSystem, energy, gradient = self.gradientCalculation(self.stubEnergy + str(self.cycleCounter) + 'LineSearch' + str(i)+ '.in', np.reshape(positions,(self.system.size,3)))
                if energy > self.energyList[-1]:
                    positions = self.geometryList[-1] + (3.00-i)/4.00*step
                elif energy < self.energyList[-1]:
                    break
                
            print "finishing linesearch \n"
            return gradSystem, gradOutSystem, energy, gradient
            
        elif self.linesearchType == 'lineBfgs':
            self.lineSearchBfgs(step)
        elif self.linesearchType == 'lineDescend':
            pass
    def writeConvergedXYZFile(self):
        ### write converged xyz file for finished optimization###
    
        fileobj = self.gradCalcList[-1]
        fileobj.write_xyz(self.stub + 'finalGeometry.xyz')
    
    def appendAnimationFile(self):
        handle = open(self.stub + 'animation.xyz', 'a')
        positions = self.geometryList[-1]
        coords = np.reshape(positions, (self.system.size, 3))
        handle.write(str(self.system.size))
        handle.write('\n\n')
        s=''
        for i in range(self.system.size):
            x = coords[i,0]
            y = coords[i,1]
            z = coords[i,2]
            s += '%-3s %10.8f %10.8f %10.8f\n' % (self.system.atoms[i], x, y, z)
        handle.write(s)
        
        handle.close()
        
    def writeAnimationFile(self):
        handle = open(self.stub +'animation.xyz', 'w')
        for positions in self.geometryList:
            coords = np.reshape(positions, (self.system.size, 3))
            handle.write(str(self.system.size))
            handle.write('\n\n')
            s=''
            for i in range(self.system.size):
                x = coords[i,0]
                y = coords[i,1]
                z = coords[i,2]
                s += '%-3s %10.8f %10.8f %10.8f\n' % (self.system.atoms[i], x, y, z)
            handle.write(s)
            
        handle.close()
        
    def lineSearchBfgs(self, step):
        pass
        
    def convergenceCheck(self):
        
        #find maximum value of displacement vector
        disp = self.geometryList[-1]-self.geometryList[-2]
        absDisp = np.absolute(disp)
        dispMax = np.amax(absDisp)
        
        self.absDispList.append(dispMax)
        
        # find maximum value of gradient 
        
        grad = self.gradientList[-1]
        if self.removeRotTrans==True:
#            grad = np.dot(self.rotTransLessMatList[0], np.dot(self.rotTransLessMatList[0].transpose(), grad))
            grad = np.dot(self.rotTransLessMatList[-1], np.dot(self.rotTransLessMatList[-1].transpose(), grad))
        absGrad = np.absolute(grad)
        gradMax = np.amax(absGrad)
        
        self.absGradList.append(gradMax)
        
        energyChange = np.abs(self.energyList[-1]-self.energyList[-2])
        self.energyChangeList.append(energyChange)
        
        toleranceList = []
        if energyChange < self.energyTol:
            toleranceList.append(1)
        elif energyChange > self.energyTol:
            toleranceList.append(0)
        if dispMax < self.dispTol:
            toleranceList.append(1)
        elif dispMax > self.dispTol:
            toleranceList.append(0)
        if gradMax < self.gradTol:
            toleranceList.append(1)
        elif gradMax > self.gradTol:
            toleranceList.append(0)
            
        total = np.sum(toleranceList)
        if total ==3:
            return True
        elif total < 3:
            return False
            
    def calcOtherModes(self):
        
        ### calculate modes for other motions desired to be removed ####
        positions = np.reshape(self.geometryList[-1], (self.system.size, 3))
        
        modeList = np.zeros((self.system.size, len(self.removeList)))
        for i, func in enumerate(self.removeList):
            if i == 0:
                modeList = func(positions)
            elif i > 0:
                modeList = np.hstack((modeList,func(positions)))
        
        self.otherModes.append(modeList)
    
    def orthogonalizeModes(self):
        
        otherModes = self.otherModes[-1]
        rotTransModes = self.system.rotTransMat 
        
        totalModes = np.hstack((rotTransModes, otherModes))
        
        newModes = pyqchem.utils.gramSchmidt(totalModes)
       
        return newModes
    
    def debug(self):
        print 'Cycle ' + str(self.cycleCounter-1) + '\n'
        print 'Geometry \n'
        print self.geometryList[-1]
        print '\n'
        print 'Gradient \n'
        print self.gradientList[-1]
        print '\n'
        print 'Inverse Hessian \n'
        print self.bfgsInvHessianList[-1]
        print '\n'
        print 'Gradient abs max \n'
        print self.absGradList[-1]
        print 'Displacement abs max \n'
        print self.absDispList[-1]
        print 'abs Energy change \n'
        print self.energyChangeList[-1]