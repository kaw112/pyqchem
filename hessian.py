import numpy as np
import pyqchem
import sys


class HessianManual:
    def __init__(self, hessian):
        self.reset()
        self.load(hessian)
        
    def load(self, hessian):
        self.hessian = hessian        
        self.eigVal, self.eigVec = np.linalg.eigh(self.hessian)
        self.dimension = np.shape(self.hessian)[0]
        
    def reset(self):
        self.hessian = []
        self.eigVec = []
        self.eigVal = []        
        self.rotTrans = []
        self.vibBasis = []
        self.dimension = 0
    def buildRotTransLessVecSpace(self, rotTransMatrix):
        ###use qr decomposition after projecting out rotations and translations###
        #first project out rotations and translations
        self.rotTrans = rotTransMatrix
        
        newVecs = np.zeros((self.dimension, self.dimension))        
        for i in range(self.dimension):
            vec = self.eigVec[:,i]
            for j in range(np.shape(self.rotTrans)[1]):
                vec = vec-np.dot(vec,self.rotTrans[:,j])*self.rotTrans[:,j]    
            
            newVecs[:,i]= vec
            
        deleteList = []
        for i in range(self.dimension):
            if np.amax(np.abs(newVecs[:,i]))<1e-5:
                deleteList.append(i)
                
        newVecs =np.delete(newVecs, deleteList, 1)
        
        #normalize
        for i in range(np.shape(newVecs)[1]):
            newVecs[:,i]= newVecs[:,i]/np.linalg.norm(newVecs[:,i])
            
            
        for i in range(np.shape(newVecs)[1]):
            vec = newVecs[:,i]
            for j in range(0,i):
                vec = vec - np.dot(vec, newVecs[:,j])*newVecs[:,j]
            if np.amax(np.abs(vec))<1e-5:
                newVecs[:,i]=np.zeros(self.dimension)
            else:
                newVecs[:,i]=vec/np.linalg.norm(vec)
        
        deleteList = []
        for i in range(np.shape(newVecs)[1]):
            if np.amax(np.abs(newVecs[:,i]))<1e-6:
                deleteList.append(i)
                
        self.vibBasis = np.delete(newVecs, deleteList, 1)        
        
#        print newVecs   
#        q,r = np.linalg.qr(newVecs)
#        print r
#        print self.dimension
#        print q
#        deleteList = []
#        for i in range(self.dimension):
##            print "\n vector " + str(i) + " has length: \n"
##            print np.linalg.norm(r[i,:])
#            if np.linalg.norm(r[i,:])<1.0e-03:
#                deleteList.append(i)
#
#        self.vibBasis = np.delete(q, deleteList, 1)

#        for i in range(np.shape(self.vibBasis)[1]):
#            for j in range(np.shape(self.rotTrans)[1]):
#                print np.dot(self.rotTrans[:,j],self.vibBasis[:,i])
#        print np.dot(self.vibBasis.transpose(), self.vibBasis)
        print "shape of vib basis \n"
        print np.shape(self.vibBasis)
        
        
class Hessian:
    """
    A class to contain important
    information from a hessian output file from qchem
    """
    def __init__(self, file_name):
        self.reset()
        self.load(file_name)
        self.buildHessian()
        self.buildInvHessian()
        self.buildEigen()
        
#    @classmethod
#    def fromfileName(cls, fileName):
#        data  = fileName
#        return cls(data)
#
#    @classmethod
#    def fromhessian(cls, hessian):
#        data = hessian
    def load(self, file_name):
        """
        load the file and set important
        parameters
        """

        self.file = file_name

        handle = open(self.file, 'r')
        line = handle.readline()
        while line:
            line = line.strip()
            if line:
                self.lines.append(line)
            line = handle.readline()

        tokens = self.lines[1].strip().split()
        self.dimension = int(tokens[1])

        for line in self.lines[2:]:
            tokens = line.strip().split()
            if line =='$end':
                break
            for token in tokens:
                self.hessianValues.append(float(token))

    def reset(self):
        """
        resets class parameters
        """

        self.file = None
        self.lines = []
        self.dimension = 0
        self.hessian = []
        self.invHessian = []
        self.hessianValues = []
        self.eigenvalues = []
        self.eigenvectors = []
        self.massWeightedHess = []

    def buildHessian(self):
        self.hessian = np.zeros((self.dimension,self.dimension))
        counter1 = 0
        counter2 = 0
        for value in self.hessianValues:
                self.hessian[counter1,counter2] = value
                self.hessian[counter2, counter1] = value
                if counter2 < counter1:
                    counter2 = counter2 +1
                elif counter2 == counter1:
                    counter1 = counter1 +1
                    counter2 = 0

    def buildInvHessian(self):
        """
        title is misleading, this requires no real
        work for me
        """
        self.invHessian = np.linalg.inv(self.hessian)
    def buildEigen(self):
        self.eigenvalues, self.eigenvectors = np.linalg.eigh(self.hessian)

    def removeTransRot(self, masses, positions, rotationalTensor, momentInertiaTensor):
        """
        removes translations and rotations from hessian as best it can
        param masses: list of masses corresponding to the atoms in molecule
        param positions: array of syz positions (columns) for the atoms of the
                        system
        """
        #we first wish to construct the mass weighted hessian
        # this is most easily acomplishe by M*H*M
        # where m is the diagonal matrix containing Mii = 1/sqrt(mi)
        # mi is repaeated 3 times, one for each degree of freedom for the atom
        numAtoms = self.dimension/3        
        
        newMasses = []
        
        for mass in masses:
            for i in range(3):
                newMasses.append(1.0/np.sqrt(mass))
                
        M = np.diag(newMasses)
        
        cOM = self.centerOfMass(masses, positions)
        
        print cOM
        
        relPositions = np.zeros(np.shape(positions))
        
        for i in range(numAtoms):
            relPositions[i,:] = positions[i,:]-cOM
            

        self.massWeightedHess = np.dot(np.dot(M,self.hessian), M)

       # eigenvalues and vectors for moment of Inertia tensor
        
        iEigenval, iEigenvec = np.linalg.eigh(rotationalTensor)
        inEigVal, inEigVec = np.linalg.eigh(momentInertiaTensor)
        #now we need to calculate the vectors corresponding to translation and rotations
        
        #we start with translations
        T = np.zeros((3, self.dimension))
        for i in range(numAtoms):
            for j in range(3):
                T[j,i*3 + j] = 1.0
        #each row of array T is a corresponding x,y, or z component of the translation vector

        #now I attempt to construct a rotations matrix
        R = np.zeros((3, self.dimension))
        Q = np.zeros((3, self.dimension))
        
        #we  essentially need to use the eigenvectors of the inertia matrix
        #to calculate the displacements caused by rotations
        
        # the governing formula for angular momentum is I*w = sum(ri x mi w x ri)
        # w x ri is the velocity of the particles
        # we will use the eigenvectors wi to calculate the speeds which can serve
        # as displacements.  Then mass weight them
        
        for i in range(numAtoms):
            for j in range(3):
                velocity = np.cross(iEigenvec[:,j], relPositions[i,:])
                R[j,i*3:i*3+3] = velocity
        
        for i in range(numAtoms):
            for j in range(3):
                vel = np.cross(inEigVec[:,j], relPositions[i,:])/masses[i]
                Q[j,i*3:i*3+3] = vel
                
        # Next we should normalize all the rotation and translation vectors
        # and stack them neatly into a D matrix 
        for i in range(3):
            T[i,:] = T[i,:]/np.linalg.norm(T[i,:])
            R[i,:] = R[i,:]/np.linalg.norm(R[i,:])
            Q[i,:] = Q[i,:]/np.linalg.norm(Q[i,:])
            
#        D = np.vstack((T,R))
        D = np.vstack((T,Q))

        for i in range(6):
            for j in range(i,6):
                print np.dot(D[i,:],D[j,:])
        #These vectors are in principle orthonormal but numerical precision is somewhat a problem
        # We should do a gram-schmidt just to make more accurate
        for i in range(6):
            for j in range(i):
                D[i,:] = D[i,:]-np.dot(D[i,:], D[j,:])*D[j,:]
            if np.linalg.norm(D[i,:]) < 1.0e-06:
                D[i,:] = np.zeros(self.dimension)
            else:
                D[i,:] = D[i,:]/np.linalg.norm(D[i,:])

   
        # first we need the eigenvectors of the mass weighted hessian
        hEigval, hEigvec = np.linalg.eigh(self.hessian)
        
        # now we can perform a gram-schmidt orthogonalization on the eigenvectors to get 
        # the 3N-6 eigenvectors
        #first remove translations and rotations
        for i in range(self.dimension):
            for j in range(6):
                hEigvec[:,i] = hEigvec[:,i]-np.dot(hEigvec[:,i],D[j,:])*D[j,:]
            if np.linalg.norm(hEigvec[:,i]) < 1.0e-06:
                hEigvec[:,i] = np.zeros(self.dimension)
            else: 
                hEigvec[:,i] = hEigvec[:,i]/np.linalg.norm(hEigvec[:,i])
        
        
        for i in range(self.dimension):
            for j in range(i):
                hEigvec[:,i] = hEigvec[:,i]-np.dot(hEigvec[:,i],hEigvec[:,j])* hEigvec[:,j]
            if np.linalg.norm(hEigvec[:,i]) < 1.0e-06:
                hEigvec[:,i] = np.zeros(self.dimension)
            else: 
                hEigvec[:,i] = hEigvec[:,i]/np.linalg.norm(hEigvec[:,i])
            
        # now we will keep only relevant vectors and form the new matrix X
        xList = []
        for i in range(self.dimension):
            if np.linalg.norm(hEigvec[:,i]) > 1.0e-12:
                xList.append(hEigvec[:,i])
                
        X = np.zeros((self.dimension, len(xList)))
        
        for i in range(len(xList)):
            X[:,i] = xList[i]
        
        print X
        print np.shape(X)
        
        return X
        
    def centerOfMass(self, masses, positions):
        
        totMass = np.sum(masses)
        massPositions = np.zeros((len(masses), 3))
    
        for i, mass in enumerate(masses):
            massPositions[i, :]= mass*positions[i,:]
        
        cOM= np.sum(massPositions, axis = 0)/totMass
    
        return cOM

class NormalModeCart:
    """A class which stores the cartesian normal modes from a qchem 
    calculation as columns in an array"""
    def __init__(self, binaryFile):
        self.reset()
        self.build(binaryFile)

    def reset(self):
        self.fileName = None
        self.binaryReader = None
        self.dimension = 0
        self.doubleList = []
        self.cartNormalModes = []
        
    def build(self, binaryFile):
        
        self.fileName = binaryFile
        self.binaryReader = pyqchem.utils.BinaryReader(self.fileName)
        
        while True:
            try: 
                self.doubleList.append(self.binaryReader.read('double'))
            except:
                break
            
        self.dimension = int(np.sqrt(len(self.doubleList)))
        
        self.cartNormalModes = np.zeros((self.dimension, self.dimension))
        
        counter = 0
        for i in range(self.dimension):
            for j in range(self.dimension):
                self.cartNormalModes[j,i] = self.doubleList[counter]
                counter += 1
        
        
    
    
class NormalModeMassWeighted:
    """A class which stores the mass weighted normal modes from a qchem 
    calculation as columns in an array"""
    def __init__(self, binaryFile):
        self.reset()
        self.build(binaryFile)

    def reset(self):
        self.fileName = None
        self.binaryReader = None
        self.dimension = 0
        self.doubleList = []
        self.mwNormalModes = []
        
    def build(self, binaryFile):
        
        self.fileName = binaryFile
        self.binaryReader = pyqchem.utils.BinaryReader(self.fileName)
        
        while True:
            try: 
                self.doubleList.append(self.binaryReader.read('double'))
            except:
                break
            
        self.dimension = int(np.sqrt(len(self.doubleList)))
        
        self.mwNormalModes = np.zeros((self.dimension, self.dimension))
        
        counter = 0
        for i in range(self.dimension):
            for j in range(self.dimension):
                self.mwNormalModes[j,i] = self.doubleList[counter]
                counter += 1
    