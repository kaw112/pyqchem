import pyqchem

class Pop:
    """
    """
    def __init__(self, job, type_string = ""):
        self.type_string = type_string
        self.charges = []
        return

    def _get_charges(self, job):
        # first, find the section
        section_string_left = "Ground-State "
        section_string_right = " Net Atomic Charges"
        if self.type_string == "Hirshfeld":
            header = self.type_string + " Atomic Charges"
        else:
            header = section_string_left + self.type_string + section_string_right
        idx_section = pyqchem.utils.get_string_index(job.lines, header)
        if (idx_section == -1): return

        # now, find each line:
        #    Atom                 Charge (a.u.)
        # ----------------------------------------
        #     1 C                    -0.173355
        #     2 C                    -0.324298
        #     3 C                    -0.270962
        #     4 H                     0.220456
        #     5 Cl                   -0.058830
        # ...

        idx = idx_section + 3
        for line in job.lines[idx:idx+job.size]:
            self.charges.append(float(line.split()[2]))

class Mulliken(Pop):
    """
    """
    def __init__(self, job, type_string = "Mulliken"):
        Pop.__init__(self, job, type_string)
        # Ground-State Mulliken Net Atomic Charges
        self._get_charges(job)
        return

class ChElPG(Pop):
    """
    """
    def __init__(self, job, type_string = "ChElPG"):
        Pop.__init__(self, job, type_string)
        # Ground-State ChElPG Net Atomic Charges
        self._get_charges(job)
        return

class Hirshfeld(Pop):
    """
    """
    def __init__(self, job, type_string = "Hirshfeld"):
        Pop.__init__(self, job, type_string)
        # Hirshfeld Atomic Charges
        self._get_charges(job)
        return
