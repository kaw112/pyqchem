# -*- coding: utf-8 -*-
"""
Created on Thu Aug 15 14:56:58 2019

@author: michal
"""
import os
import pyqchem as pyq
import math
def testPseudos():
    a = os.listdir(os.getcwd())
    rmsDev = []
    for fileName in a :
        inputObject = pyq.EspressoParser(fileName)
        posInit = inputObject.positions
        outObj = pyq.EspressoOutputParser(inputObject.stub + '.out')
        outObj.getFinalPositions()
        posOpt = outObj.finalPositions
        a = 0
        for i in range(inputObject.size):
            for j in range(3):
                a += (posInit[i, j] - posOpt[i, j])**2
        rmsDev1 = math.sqrt(a/len(inputObject.atoms))
        rmsDev.append(rmsDev1)
    f = open("rmsDev.txt","w+")
    for i in rmsDev:
        f.write(i)
        f.close()